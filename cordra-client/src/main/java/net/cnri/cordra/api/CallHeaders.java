package net.cnri.cordra.api;

public class CallHeaders {
    public String mediaType;
    public String filename;

    public CallHeaders() { }

    public CallHeaders(String mediaType, String filename) {
        this.mediaType = mediaType;
        this.filename = filename;
    }
}