package net.cnri.cordra.api;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import com.google.gson.JsonElement;

import net.cnri.cordra.util.Util;

public interface DefaultingFromCallResponseCordraClient extends CallResponseHandlerEnabledCordraClient {

    @Override
    @SuppressWarnings("resource")
    default InputStream getPayload(String id, String payloadName, Options options) throws CordraException {
        return getPayloadAsResponse(id, payloadName, options).body;
    }

    @Override
    default CallResponse getPayloadAsResponse(String id, String payloadName, Options options) throws CordraException {
        return getPartialPayloadAsResponse(id, payloadName, null, null, options);
    }

    @Override
    default void getPayload(String id, String payloadName, CallResponseHandler handler, Options options) throws CordraException {
        try (CallResponse response = getPayloadAsResponse(id, payloadName, options)) {
            handler.setMediaType(response.headers.mediaType);
            handler.setFilename(response.headers.filename);
            Util.copyStream(response.body, handler.getOutputStream());
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    @SuppressWarnings("resource")
    default InputStream getPartialPayload(String id, String payloadName, Long start, Long end, Options options) throws CordraException {
        return getPartialPayloadAsResponse(id, payloadName, start, end, options).body;
    }

    @Override
    CallResponse getPartialPayloadAsResponse(String id, String payloadName, Long start, Long end, Options options) throws CordraException;

    @Override
    default void getPartialPayload(String id, String payloadName, Long start, Long end, CallResponseHandler handler, Options options) throws CordraException {
        try (CallResponse response = getPartialPayloadAsResponse(id, payloadName, start, end, options)) {
            handler.setMediaType(response.headers.mediaType);
            handler.setFilename(response.headers.filename);
            Util.copyStream(response.body, handler.getOutputStream());
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    default CallResponse callAsResponse(String objectId, String methodName, JsonElement params, Options options) throws CordraException {
        ByteArrayInputStream input = params == null ? null : new ByteArrayInputStream(params.toString().getBytes(StandardCharsets.UTF_8));
        return callAsResponse(objectId, methodName, input, options);
    }

    @Override
    default void call(String objectId, String methodName, JsonElement params, CallResponseHandler handler, Options options) throws CordraException {
        try (CallResponse response = callAsResponse(objectId, methodName, params, options)) {
            handler.setMediaType(response.headers.mediaType);
            handler.setFilename(response.headers.filename);
            Util.copyStream(response.body, handler.getOutputStream());
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    default void call(String objectId, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException {
        try (CallResponse response = callAsResponse(objectId, methodName, input, options)) {
            handler.setMediaType(response.headers.mediaType);
            handler.setFilename(response.headers.filename);
            Util.copyStream(response.body, handler.getOutputStream());
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    default CallResponse callForTypeAsResponse(String type, String methodName, JsonElement params, Options options) throws CordraException {
        ByteArrayInputStream input = params == null ? null : new ByteArrayInputStream(params.toString().getBytes(StandardCharsets.UTF_8));
        return callAsResponse(type, methodName, input, options);
    }

    @Override
    default void callForType(String type, String methodName, JsonElement params, CallResponseHandler handler, Options options) throws CordraException {
        try (CallResponse response = callForTypeAsResponse(type, methodName, params, options)) {
            handler.setMediaType(response.headers.mediaType);
            handler.setFilename(response.headers.filename);
            Util.copyStream(response.body, handler.getOutputStream());
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    default void callForType(String type, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException {
        try (CallResponse response = callForTypeAsResponse(type, methodName, input, options)) {
            handler.setMediaType(response.headers.mediaType);
            handler.setFilename(response.headers.filename);
            Util.copyStream(response.body, handler.getOutputStream());
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }
}
