package net.cnri.cordra.collections;

import java.util.Iterator;
import java.util.List;

import net.cnri.cordra.api.FacetResult;
import net.cnri.cordra.api.SearchResults;

public class SearchResultsFromIterator<T> implements SearchResults<T> {

    private final int size;
    private final List<FacetResult> facets;
    private final Iterator<T> iter;

    public SearchResultsFromIterator(int size, Iterator<T> iter) {
        this(size, null, iter);
    }

    public SearchResultsFromIterator(int size, List<FacetResult> facets, Iterator<T> iter) {
        this.size = size;
        this.facets = facets;
        this.iter = iter;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public List<FacetResult> getFacets() {
        return facets;
    }

    @Override
    public Iterator<T> iterator() {
        return iter;
    }

    @Override
    public void close() {
        // no-op
    }
}
