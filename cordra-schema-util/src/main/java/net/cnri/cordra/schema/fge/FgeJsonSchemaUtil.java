package net.cnri.cordra.schema.fge;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.cfg.ValidationConfigurationBuilder;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.load.SchemaLoader;
import com.github.fge.jsonschema.core.load.configuration.LoadingConfiguration;
import com.github.fge.jsonschema.core.load.configuration.LoadingConfigurationBuilder;
import com.github.fge.jsonschema.core.load.uri.URITranslatorConfiguration;
import com.github.fge.jsonschema.core.load.uri.URITranslatorConfigurationBuilder;
import com.github.fge.jsonschema.core.messages.JsonSchemaCoreMessageBundle;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.msgsimple.bundle.MessageBundle;
import com.github.fge.msgsimple.load.MessageBundles;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheBuilderSpec;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import net.cnri.cordra.InvalidException;
import net.cnri.cordra.schema.SchemaNodeAndUri;
import net.cnri.cordra.schema.SchemaResolver;
import net.cnri.cordra.schema.SchemaUtil;
import net.cnri.cordra.util.JacksonUtil;

public class FgeJsonSchemaUtil {
    private static final MessageBundle BUNDLE = MessageBundles.getBundle(JsonSchemaCoreMessageBundle.class);

    public static Map<String, JsonNode> extract(ProcessingReport report, JsonNode schema, SchemaResolver resolver) {
        resolver = new SchemaResolverFromInside(schema, resolver);
        Map<String, JsonNode> schemas = new HashMap<>();
        for (ProcessingMessage msg : report) {
            if (msg.getLogLevel() == LogLevel.INFO && "net.cnri.message".equals(msg.getMessage())) {
                JsonNode node = msg.asJson();
                String keyword = node.get("keyword").asText(null);
                if (keyword == null) continue;
                if ("format".equals(keyword)) {
                    keyword = node.get("attribute").asText(null);
                    if (keyword == null) continue;
                }
                String pointer = node.get("instance").get("pointer").asText(null);
                if (pointer == null) continue;
                String schemaPointer = node.get("schema").get("pointer").asText(null);
                if (schemaPointer == null) continue;
                String loadingUri = node.get("schema").get("loadingURI").asText(null);

                JsonNode fieldNameNode = node.get("fieldName");
                if (fieldNameNode != null) {
                    pointer += fieldNameNode.asText("");
                }
                JsonNode fieldPointerNode = node.get("fieldPointer");
                if (fieldPointerNode != null) {
                    schemaPointer = fieldPointerNode.asText("");
                }

                JsonNode actualSchema = loadingUri == null ? schema : SchemaUtil.loadUsingResolver(URI.create(loadingUri), resolver);
                JsonNode subSchema = JacksonUtil.getJsonAtPointer(schemaPointer, actualSchema);
                schemas.put(pointer, subSchema);
            }
        }
        return schemas;
    }

    private static class SchemaResolverFromInside implements SchemaResolver {
        private final JsonNode schema;
        private final SchemaResolver resolver;

        public SchemaResolverFromInside(JsonNode schema, SchemaResolver resolver) {
            this.schema = schema;
            this.resolver = resolver;
        }

        @Override
        public SchemaNodeAndUri resolve(String uri) {
            if (uri == null || uri.isEmpty()) return new SchemaNodeAndUri(schema, null);
            else return resolver.resolve(uri);
        }

    }

    // This allows us to override the json-schema-validator behavior,
    // regardless of the use of the $schema keyword on the schemas
    @SuppressWarnings("rawtypes")
    public
    static void clearLibraries(ValidationConfigurationBuilder vcb) {
        try {
            Field f = vcb.getClass().getDeclaredField("libraries");
            f.setAccessible(true);
            ((Map)f.get(vcb)).clear();
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    public static LoadingConfigurationBuilder buildRefAwareLoadingConfigurationBuilder() {
        LoadingConfigurationBuilder lcb = LoadingConfiguration.newBuilder();
        // This doesn't actually disable caching in a meaningful way; so instead we recreate the factory anew as needed in GenerationalFgeSchemaFactory
        // lcb.setEnableCache(false);
        URITranslatorConfigurationBuilder utcb = URITranslatorConfiguration.newBuilder();
        utcb.setNamespace("file:/"); // will force relative refs to be absolute
        lcb.setURITranslatorConfiguration(utcb.freeze());
        return lcb;
    }

    public static ProcessingException uriNotFoundException(URI uri) {
        return new ProcessingException(new ProcessingMessage()
            .setMessage(BUNDLE.getMessage("uriManager.uriIOError"))
            .putArgument("uri", uri)
            .put("exceptionMessage", "$ref URI not found"));
    }

    public static void setSchemaResolver(JsonSchemaFactory factory, SchemaResolver resolver) {
        try {
            Field loaderField = JsonSchemaFactory.class.getDeclaredField("loader");
            loaderField.setAccessible(true);
            SchemaLoader loader = (SchemaLoader) loaderField.get(factory);
            Field cacheField = SchemaLoader.class.getDeclaredField("cache");
            cacheField.setAccessible(true);
            LoadingCache<URI, JsonNode> newCache = CacheBuilder.from(CacheBuilderSpec.disableCaching())
                .build(new CacheLoader<URI, JsonNode>() {
                    @Override
                    public JsonNode load(URI uri) throws ProcessingException {
                        JsonNode res = SchemaUtil.loadUsingResolver(uri, resolver);
                        if (res == null) throw uriNotFoundException(uri);
                        return res;
                    }
                });
            cacheField.set(loader, newCache);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    public static JsonSchema getJsonSchema(JsonSchemaFactory factory, JsonNode node, String baseUri) throws InvalidException {
        try {
            JsonNode adjustedSchemaNode = SchemaUtil.schemaWithBaseUri(node, baseUri);
            return factory.getJsonSchema(adjustedSchemaNode);
        } catch (ProcessingException e) {
            throw new InvalidException("Invalid schema", e);
        }
    }
}
