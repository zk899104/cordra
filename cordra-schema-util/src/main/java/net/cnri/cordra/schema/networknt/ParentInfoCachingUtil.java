package net.cnri.cordra.schema.networknt;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.networknt.schema.JsonValidator;

import net.cnri.cordra.util.JsonUtil;

public class ParentInfoCachingUtil {

    public static ThreadLocal<JsonNode> parentJsonNodeThreadLocal = new ThreadLocal<>();
    public static ThreadLocal<String> parentAtThreadLocal = new ThreadLocal<>();
    public static ThreadLocal<String> parentJsonPointerThreadLocal = new ThreadLocal<>();

    public static String determineLastSegment(String at) {
        if (at == null) throw new NullPointerException();
        if (JsonValidator.AT_ROOT.equals(at)) return null;
        String parentAt = parentAtThreadLocal.get();
        String childAtExtension = at.substring(parentAt.length());
        if (childAtExtension.startsWith(".")) {
            return childAtExtension.substring(1);
        } else if (childAtExtension.startsWith("[") && childAtExtension.endsWith("]")) {
            return childAtExtension.substring(1, childAtExtension.length() - 1);
        } else {
            throw new IllegalArgumentException(parentAt + " " + at);
        }
    }

    public static String determineJsonPointerFromAt(String at) {
        String parentJsonPointer = parentJsonPointerThreadLocal.get();
        String lastSegment = determineLastSegment(at);
        String jsonPointer = determineJsonPointer(parentJsonPointer, lastSegment);
        return jsonPointer;
    }

    public static String determineJsonPointer(String parentJsonPointer, String lastSegment) {
        String jsonPointer;
        if (parentJsonPointer == null && lastSegment == null) {
            jsonPointer = "";
        } else if (parentJsonPointer != null && lastSegment != null) {
            jsonPointer = parentJsonPointer + "/" + JsonUtil.encodeSegment(lastSegment);
        } else {
            throw new IllegalStateException("Unexpected parentJsonPointer " + parentJsonPointer + " with lastSegment " + lastSegment);
        }
        return jsonPointer;
    }

    public static void setNode(JsonNode newNode, String lastSegment) {
        JsonNode parentNode = parentJsonNodeThreadLocal.get();
        if (parentNode == null || lastSegment == null) throw new IllegalStateException("Unable to setNode at root");
        if (parentNode.isObject()) {
            JsonNode oldNode = ((ObjectNode)parentNode).replace(lastSegment, newNode);
            if (oldNode == null) {
                throw new IllegalArgumentException("Replaced non-existing property " + lastSegment);
            }
        } else if (parentNode.isArray()) {
            JsonNode oldNode = ((ArrayNode)parentNode).set(Integer.parseInt(lastSegment), newNode);
            if (oldNode == null) {
                throw new IllegalArgumentException("Replaced non-existing item " + lastSegment);
            }
        } else {
            throw new IllegalStateException("Parent is not object or array");
        }
    }
}
