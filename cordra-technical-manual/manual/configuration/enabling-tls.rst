.. _enabling-tls:

Enabling TLS
============

In addition to :doc:`responding over HTTPS <https>`, Cordra can be configured to use TLS/SSL when
communicating with underlying systems.

See also :ref:`CordraClient TLS configuration <client_tls_configuration>` for how to configure
the Java CordraClient for server TLS authentication when connecting to Cordra.

ZooKeeper
---------

.. note::
    TLS connections are only supported in ZooKeeper 3.5.5 or higher.

Connecting to ZooKeeper using TLS requires certain system properties to be set.
These properties can also be set in the ``WEB-INF/web.xml`` file using a ``context-param``
called ``properties`` (in Java properties format) or ``properties.json`` (in JSON format).
If using Java properties format note that whitespace at the end of a line, but not beginning of a line, is significant.

Example web.xml::

    <context-param>
        <param-name>zookeeperConnectionString</param-name>
        <param-value>10.5.0.101:2181,10.5.0.102:2181,10.5.0.103:2181/cordra</param-value>
    </context-param>
    <context-param>
        <param-name>configName</param-name>
        <param-value>/config.json</param-value>
    </context-param>
    <context-param>
        <param-name>properties</param-name>
        <param-value>
    zookeeper.clientCnxnSocket=org.apache.zookeeper.ClientCnxnSocketNetty
    zookeeper.client.secure=true
    zookeeper.ssl.keyStore.location=/root/zookeeper/ssl/testKeyStore.jks
    zookeeper.ssl.keyStore.password=testpass
    zookeeper.ssl.trustStore.location=/root/zookeeper/ssl/testTrustStore.jks
    zookeeper.ssl.trustStore.password=testpass
    zookeeper.ssl.hostnameVerification=true
        </param-value>
    </context-param>

For more details on ZooKeeper client configuration, see the
`ZooKeeper Administrator's Guide <https://zookeeper.apache.org/doc/r3.5.7/zookeeperAdmin.html>`__
and the
`ZooKeeper SSL User Guide <https://cwiki.apache.org/confluence/display/ZOOKEEPER/ZooKeeper+SSL+User+Guide>`__.

Kafka
-----

Generally, the Kafka clients created by Cordra will use the same TLS configuration. This can be set using the
``kafkaCommonConfig`` property in ``config.json``. Any configuration in this property will be applied to all
internal Kafka clients, unless that setting is overridden by a specific client configuration.

Sample config.json::

      {
          "kafkaCommonConfig": {
              "security.protocol": "SSL",
              "ssl.key.password": "secret_key",
              "ssl.keystore.location": "/path/to/keystore",
              "ssl.keystore.password": "keystore_secret",
              "ssl.truststore.location": "/path/to/truststore",
              "ssl.truststore.password": "truststore_secret",
              "ssl.endpoint.identification.algorithm": "https"
          }
      }

For more details on Kafka client configuration, see the
`Kafka documentation <https://kafka.apache.org/documentation/#security_configclients>`__.

General TLS
-----------

Connecting to some applications may require modifying ``javax.net.ssl`` system properties.
For example, if you are using mutual TLS authentication or using self-signed certificates,
with MongoDB, Solr, or Elasticsearch,
you may need to set the properties ``javax.net.ssl.keyStore``, ``javax.net.ssl.keyStorePassword``,
``javax.net.ssl.trustStore``, and ``javax.net.ssl.trustStorePassword``.

Cordra supports configuring system properties using the ``properties`` property in ``config.json``.
Example config.json with these TLS settings::

    {
        "properties": {
            "javax.net.ssl.keyStore": "/path/to/keystore",
            "javax.net.ssl.keyStorePassword": "keystore_secret",
            "javax.net.ssl.trustStore": "/path/to/truststore",
            "javax.net.ssl.trustStorePassword": "truststore_secret"
        }
    }

