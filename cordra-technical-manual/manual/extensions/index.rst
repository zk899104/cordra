Extensions and Applications
===========================

.. toctree::
   :maxdepth: 1
   :glob:

   extending-with-java
   document-repository
   medical-records-application
   person-registry
   user-registration
   object-linking
   oai-pmh
   recommendations
   sending-emails
   external-authentication-provider
   collab-prototype
