package net.cnri.cordra;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.AllHandlesUpdater.UpdateStatus;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.*;
import net.cnri.cordra.auth.RequestAuthenticationInfo;
import net.cnri.cordra.collections.SearchResultsFromStream;
import net.cnri.cordra.doip.CordraClientDoipProcessor;
import net.cnri.cordra.doip.DoipServerConfigWithEnabledFlag;
import net.cnri.cordra.handle.LightWeightHandleServer;
import net.cnri.cordra.indexer.*;
import net.cnri.cordra.javascript.*;
import net.cnri.cordra.model.*;
import net.cnri.cordra.replication.kafka.CordraObjectWithPayloadsAsStrings;
import net.cnri.cordra.replication.kafka.ReplicationMessage;
import net.cnri.cordra.replication.kafka.ReplicationProducer;
import net.cnri.cordra.schema.LazySchema;
import net.cnri.cordra.schema.SchemaNodeAndUri;
import net.cnri.cordra.schema.fge.GenerationalFgeSchemaFactory;
import net.cnri.cordra.storage.CordraStorage;
import net.cnri.cordra.sync.*;
import net.cnri.cordra.util.JacksonUtil;
import net.cnri.cordra.util.JsonUtil;
import net.cnri.cordra.util.SearchUtil;
import net.cnri.cordra.web.batch.BatchUploadProcessor;
import net.cnri.microservices.Alerter;
import net.cnri.microservices.MultithreadedKafkaConsumer;
import net.cnri.microservices.StripedExecutorService;
import net.cnri.servletcontainer.sessions.HttpSessionManager;
import net.cnri.util.StreamUtil;
import net.cnri.util.ThrottledExecutorService;
import net.cnri.util.javascript.JavaScriptEnvironment;
import net.cnri.util.javascript.JavaScriptRunner;
import net.dona.doip.server.DoipProcessor;
import net.dona.doip.server.DoipServer;
import net.dona.doip.server.DoipServerConfig;
import net.handle.hdllib.*;

import net.handle.hdllib.trust.JsonWebSignature;
import net.handle.hdllib.trust.JsonWebSignatureFactory;
import net.handle.hdllib.trust.TrustException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.ScriptException;
import javax.servlet.http.HttpSession;

import java.io.*;
import java.net.InetAddress;
import java.security.*;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CordraService {
    private static Logger logger = LoggerFactory.getLogger(CordraService.class);

    public static final String DESIGN_OBJECT_ID = "design";
    public static final String DESIGN_OBJECT_TYPE = "CordraDesign";
    public static final String HASH_ATTRIBUTE = "admin_password_hash";
    public static final String SALT_ATTRIBUTE = "admin_password_salt";
    public static final String ITERATIONS_ATTRIBUTE = "admin_password_iterations";
    public static final String HASH_ALGORITHM_ATTRIBUTE = "admin_password_hash_algorithm";

    public static final String REPOSITORY_SERVICE_PREFIX_HANDLE = "0.NA/20.5000";

    public static final String TXN_ID = "txnId";

    static final Gson gson = GsonUtility.getPrettyGson();

    final String cordraServiceId;
    final String cordraClusterId;
    final boolean isReadOnly;
    final CordraStorage storage;
    final CordraIndexer indexer;
    final Reindexer reindexer;
    final ReplicationProducer replicationProducer;
    final StripedExecutorService stripedTaskRunner;
    final MultithreadedKafkaConsumer replicationConsumer;
    PrivateKey privateKey;
    PublicKey publicKey;
    PrivateKey handlePrivateKey;
    PublicKey handlePublicKey;
    final CordraConfig cordraConfig;
    final DoipSetupProvider doipSetupProvider;

    final CordraObjectSchemaValidator validator;
    final VersionManager versionManager;
    private final AllHandlesUpdater handlesUpdater;
    final Alerter alerter;

    // State based on administrative config
    private final HttpSessionManager sessionManager;
    final KeyPairAuthJtiChecker keyPairAuthJtiChecker;
    final AclEnforcer aclEnforcer;
    final InternalAuthorizer authorizer;
    final HandleMinter handleMinter;
    HandleClient handleClient;
    AuthenticationInfo authInfo;
    LightWeightHandleServer lightWeightHandleServer;
    DoipServer doipServer;
    volatile CordraClientDoipProcessor doipProcessor;
    DoipServerConfigWithEnabledFlag doipServerConfig;
    String doipServiceId;

    // Synchronization
    private final SyncObjects syncObjects;
    private final TransactionManager transactionManager;
    private final LeadershipManager leadershipManager;
    private final SingleThreadReadWriteCheckableLocker designLocker;
    private final SignalWatcher signalWatcher;
    private final CheckableLocker schemaNameLocker;
    private final CheckableLocker usernameLocker;
    final NameLocker objectLocker;
    private final TransactionReprocessingQueue transactionReprocessingQueue;
    private final Object doipServerRestartSync = new Object(); // local sync only

    // Cached state
    private final GenerationalFgeSchemaFactory jsonSchemaFactory = new GenerationalFgeSchemaFactory(this::supplyJsonSchemaFactory);
    volatile DesignPlusSchemas design;
    volatile Map<String, LazySchema<JsonSchema>> schemas;
    volatile Map<String, CordraObject> schemaCordraObjects;
    private final CordraRequireLookup cordraRequireLookup;
    private final JavaScriptEnvironment javaScriptEnvironment;
    private final JavaScriptLifeCycleHooks javaScriptHooks;

    private final AtomicLong authObjectChangeCount = new AtomicLong();
    private final AtomicLong authObjectChangeIndexed = new AtomicLong();
    private final AuthCache authCache;
    private final ExecutorService preCacheExecutorService = new ThrottledExecutorService(120_000, 10_000);
    private final ExecutorService backgroundSchemaWarmUpExec = Executors.newSingleThreadExecutor();
    private final AtomicLong javaScriptWarmUpGeneration = new AtomicLong();

    private final Collection<Callable<Void>> shutdownHooks = new ConcurrentLinkedQueue<>();

    private volatile boolean closed = false;

    public CordraService(String cordraServiceId, String cordraClusterId, CordraStorage storage, CordraIndexer indexer,
                         ReplicationProducer replicationProducer,
                         HttpSessionManager sessionManager, StripedExecutorService stripedTaskRunner,
                         MultithreadedKafkaConsumer replicationConsumer,
                         PrivateKey handlePrivateKey, PublicKey handlePublicKey,
                         PrivateKey privateKey, PublicKey publicKey, boolean isReadOnly,
                         SyncObjects syncObjects, CordraConfig cordraConfig,
                         DoipSetupProvider doipSetupProvider) {
        this.cordraConfig = cordraConfig;
        this.cordraServiceId = cordraServiceId;
        this.cordraClusterId = cordraClusterId;
        this.isReadOnly = isReadOnly;
        this.syncObjects = syncObjects;
        this.storage = storage;
        this.indexer = indexer;
        this.replicationProducer = replicationProducer;
        this.stripedTaskRunner = stripedTaskRunner;
        this.replicationConsumer = replicationConsumer;
        this.sessionManager = sessionManager;
        this.authInfo = null;
        this.handlePrivateKey = handlePrivateKey;
        this.handlePublicKey = handlePublicKey;
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.doipSetupProvider = doipSetupProvider;
        this.validator = new CordraObjectSchemaValidator(this);
        this.authCache = new MemoryAuthCache();
        this.aclEnforcer = new AclEnforcer(this, storage, indexer, authCache);
        this.authorizer = new InternalAuthorizer(this);
        this.handlesUpdater = new AllHandlesUpdater(syncObjects.getAllHandlesUpdaterSync());

        CordraHooksSupportProvider.set(new CordraHooksSupportImpl());
        this.cordraRequireLookup = new CordraRequireLookup(this);
        this.javaScriptEnvironment = new JavaScriptEnvironment(cordraRequireLookup);
        this.javaScriptHooks = new JavaScriptLifeCycleHooks(javaScriptEnvironment, cordraConfig.traceRequests, cordraRequireLookup, this::getDesign);

        this.keyPairAuthJtiChecker = syncObjects.getKeyPairAuthJtiChecker();
        this.transactionManager = syncObjects.getTransactionManager();
        this.leadershipManager = syncObjects.getLeadershipManager();
        this.designLocker = syncObjects.getDesignLocker();
        this.handleMinter = new HandleMinter(null); // prefix will be set later
        this.versionManager = new VersionManager(storage, indexer, this);
        this.alerter = syncObjects.getAlerter();
        this.schemaNameLocker = syncObjects.getSchemaNameLocker();
        this.usernameLocker = syncObjects.getUsernameLocker();
        this.objectLocker = new ContextRecordingNameLocker(syncObjects.getObjectLocker());
        this.signalWatcher = syncObjects.getSignalWatcher();
        this.reindexer = new Reindexer(storage, indexer, transactionManager, cordraConfig, this, cordraServiceId, objectLocker, alerter);

        TransactionReprocessingQueue delegateTransactionReprocessingQueue = syncObjects.getTransactionReprocessingQueue();
        if (delegateTransactionReprocessingQueue == null) {
            this.transactionReprocessingQueue = null;
        } else {
            this.transactionReprocessingQueue = new DelegatingErrorCatchingTransactionReprocessingQueue(delegateTransactionReprocessingQueue);
        }
//        if (replicationConsumer == null) {
//            this.replicationApplier = null;
//        } else {
//            this.replicationApplier = new StripedThreadPoolExecutorService(numReplicationThreads, numReplicationThreads, 500, (thread, exception) -> {
//                this.alerter.alert("Exception in replicationApplier " + exception);
//                logger.error("Exception in replicationApplier", exception);
//            });
//        }
    }

    private class CordraHooksSupportImpl implements CordraHooksSupport {
        @Override
        public CordraClient getCordraClient() {
            return InternalCordraClientFactory.get();
        }

        @Override
        public void addShutdownHook(Callable<Void> shutdownHook) {
            shutdownHooks.add(shutdownHook);
        }

        @Override
        public String signWithCordraKey(String payload, boolean useJsonSerialization) throws CordraException {
            if (privateKey == null) {
                throw new InternalErrorCordraException("No key available for signing");
            }
            if (payload == null) {
                throw new InternalErrorCordraException("No payload provided for signing");
            }
            try {
                JsonWebSignature jws = JsonWebSignatureFactory.getInstance().create(payload, privateKey);
                if (useJsonSerialization) {
                    return jws.serializeToJson();
                } else {
                    return jws.serialize();
                }
            } catch (TrustException e) {
                throw new InternalErrorCordraException("Error signing", e);
            }
        }

        @Override
        public boolean verifyWithCordraKey(String jwt) throws CordraException {
            if (publicKey == null) {
                throw new InternalErrorCordraException("No key available for verifying");
            }
            try {
                JsonWebSignatureFactory signatureFactory = JsonWebSignatureFactory.getInstance();
                JsonWebSignature jws = signatureFactory.deserialize(jwt);
                boolean isValid = jws.validates(publicKey);
                return isValid;
            } catch (TrustException e) {
                throw new InternalErrorCordraException("Error verifying", e);
            }
        }

    }

    public void init() throws CordraException {
        if (doipSetupProvider != null) {
            try {
                doipSetupProvider.initializeKeysAndCertChain(publicKey, privateKey);
            } catch (Exception e) {
                throw new InternalErrorCordraException(e);
            }
        }
        preCache();
        indexer.setDesignSupplier(this::getDesign);
        indexer.setObjectTransformer(javaScriptHooks::objectForIndexing);
        transactionManager.start(cordraServiceId);
        signalWatcher.start(cordraServiceId, this::receiveSignal);
        if (transactionReprocessingQueue != null) {
            transactionReprocessingQueue.start(this::processPendingTransaction, transactionManager);
        }
        CordraServiceForLifeCycleHooks cordraServiceForLifeCycleHooks = new CordraServiceForLifeCycleHooks();
        cordraServiceForLifeCycleHooks.init(this);
        javaScriptEnvironment.getScriptEngineAndCompilationCache().put("_cordraReturningStrings", cordraServiceForLifeCycleHooks);
        CordraUtilForLifeCycleHooks cordraUtilForLifeCycleHooks = new CordraUtilForLifeCycleHooks();
        cordraUtilForLifeCycleHooks.init(this);
        javaScriptEnvironment.getScriptEngineAndCompilationCache().put("_cordraUtil", cordraUtilForLifeCycleHooks);
    }

    public void startReplication() {
        if (replicationConsumer != null) {
            replicationConsumer.start(this::applyReplicationMessage, this::stripePicker);
        }
    }

    public void receiveSignal(SignalWatcher.Signal signal) {
        if (signal == SignalWatcher.Signal.DESIGN) {
            try {
                loadStatefulData();
            } catch (CordraException e) {
                alerter.alert("Error refreshing state data: " + e);
                logger.error("Error refreshing state data", e);
            }
        } else if (signal == SignalWatcher.Signal.AUTH_CHANGE) {
            authObjectChangeCount.incrementAndGet();
            authCache.clearAllGroupsForUserValues();
            authCache.clearAllUserIdForUsernameValues();
            preCache();
        } else if (signal == SignalWatcher.Signal.JAVASCRIPT_CLEAR_CACHE) {
            cordraRequireLookup.clearAllObjectIdsForModuleValues();
            javaScriptEnvironment.clearCache();
            backgroundSchemaWarmUpExec.submit(() -> warmUpJavaScript(jsonSchemaFactory.getGeneration(), javaScriptWarmUpGeneration.incrementAndGet()));
        }
    }

    private void preCache() {
        preCacheExecutorService.execute(this::preCacheNow);
    }

    public void preCacheNow() {
        //if (true) return;
        try {
            long authObjectChangeCountAtStart = authObjectChangeCount.get();
            ensureIndexUpToDateWhenAuthChange();
            Map<String, List<String>> userToGroupsMap = new HashMap<>();
            String q = "username:[* TO *] users:[* TO *] -isVersion:true -objatt_isVersion:true";
            try (SearchResults<CordraObject> results = searchRepo(q)) {
                for (CordraObject co : results) {
                    if (authObjectChangeCount.get() > authObjectChangeCountAtStart) return;
                    JsonElement username = co.metadata.internalMetadata.get("username");
                    if (username != null) {
                        authCache.setUserIdForUsername(username.getAsString(), co.id);
                    }
                    JsonElement groupMember = co.metadata.internalMetadata.get("users");
                    if (groupMember != null) {
                        for (String member : groupMember.getAsString().split("\n")) {
                            if (member.isEmpty()) continue;
                            userToGroupsMap
                            .computeIfAbsent(member, unused -> new ArrayList<>())
                            .add(co.id);
                        }
                    }
                }
            }
            Map<String, Set<String>> recursiveUserToGroupsMap = calculateRecursiveUserToGroupsMap(userToGroupsMap);
            for (Map.Entry<String, Set<String>> entry : recursiveUserToGroupsMap.entrySet()) {
                if (authObjectChangeCount.get() > authObjectChangeCountAtStart) return;
                authCache.setGroupsForUser(entry.getKey(), entry.getValue());
            }
        } catch (Exception e) {
            // in case of errors, it is okay to abort the pre-cache
            return;
        }
    }

    Map<String, Set<String>> calculateRecursiveUserToGroupsMap(Map<String, List<String>> userToGroupsMap) {
        Map<String, Set<String>> recursiveUserToGroupsMap = new HashMap<>();
        for (String memberId : userToGroupsMap.keySet()) {
            Set<String> accumulation = getGroupsForUserRecursive(memberId, userToGroupsMap, recursiveUserToGroupsMap, new HashSet<String>());
            recursiveUserToGroupsMap.put(memberId, accumulation);
        }
        return recursiveUserToGroupsMap;
    }

    Set<String> getGroupsForUserRecursive(String memberId, Map<String, List<String>> userToGroupsMap, Map<String, Set<String>> recursiveUserToGroupsMap, Set<String> seen) {
        Set<String> accumulation = new HashSet<>();
        Set<String> recursiveResult = recursiveUserToGroupsMap.get(memberId);
        if (recursiveResult != null) {
            accumulation.addAll(recursiveResult);
            return accumulation;
        }
        List<String> groupsForUser = userToGroupsMap.get(memberId);
        if (groupsForUser == null || groupsForUser.isEmpty()) {
            recursiveUserToGroupsMap.put(memberId, Collections.emptySet());
        } else {
            for (String groupId : groupsForUser) {
                if (!seen.contains(groupId)) {
                    seen.add(groupId);
                    Set<String> parentGroups = getGroupsForUserRecursive(groupId, userToGroupsMap, recursiveUserToGroupsMap, seen);
                    //recursiveUserToGroupsMap.put(groupId, parentGroups);
                    accumulation.add(groupId);
                    accumulation.addAll(parentGroups);
                }
            }
        }
        return accumulation;
    }

    public void processPendingTransactions() {
        try {
            logger.info("Cordra " + cordraServiceId + " processing pending txns");
            processPendingTransactionsThrowing();
            logger.info("Cordra " + cordraServiceId + " finished processing pending txns");
        } catch (Exception e) {
            alerter.alert("Error processing pending transactions: " + e);
            logger.error("Error processing pending transactions", e);
        }
    }

    private void processPendingTransactionsThrowing() throws CordraException {
        // allowing read-only instances to process transactions for replication
        //        if (isReadOnly) throw new ReadOnlyCordraException();
        logger.info("Cordra " + cordraServiceId + " processing pending payload indexing");
        processPendingPayloadIndexing();

        List<String> oldMembersWithPendingTxns = new ArrayList<>(transactionManager.getCordraServiceIdsWithOpenTransactions());
        oldMembersWithPendingTxns.removeAll(leadershipManager.getGroupMembers());
        for (String oldMember : oldMembersWithPendingTxns) {
            logger.info("Cordra " + cordraServiceId + " processing pending txns for " + oldMember);
            processPendingTransactionsForOneOldMember(oldMember);
        }
    }

    private void processPendingPayloadIndexing() throws CordraException, IndexerException {
        String query = "+(" + CordraIndexer.PAYLOAD_INDEX_STATE + ":" + CordraIndexer.INDEX_IN_PROCESS
            +" objatt_" + CordraIndexer.PAYLOAD_INDEX_STATE + ":" + CordraIndexer.INDEX_IN_PROCESS + ")";
        // exclude live members
        for (String groupMember : leadershipManager.getGroupMembers()) {
            query += " -" + CordraIndexer.PAYLOAD_INDEX_CORDRA_SERVICE_ID + ":" + groupMember
                + " -objatt_" + CordraIndexer.PAYLOAD_INDEX_CORDRA_SERVICE_ID + ":" + groupMember;
        }
        ensureIndexUpToDate();
        try (SearchResults<String> objectsWithPayloadsInProcess = indexer.searchHandles(query)) {
            for (String handle : objectsWithPayloadsInProcess) {
                logger.info("objectsWithPayloadsInProcess " + handle);
                objectLocker.lock(handle);
                try {
                    CordraObject co = storage.get(handle);
                    if (co != null) {
                        boolean indexPayloads = shouldIndexPayloads(co.type);
                        indexObject(co, indexPayloads);
                    } else {
                        indexer.deleteObject(handle);
                    }
                } finally {
                    objectLocker.release(handle);
                }
            }
        }
    }

    private void processPendingTransactionsForOneOldMember(String oldMember) throws CordraException, IndexerException {
        Iterator<Entry<Long, CordraTransaction>> iter = transactionManager.iterateTransactions(oldMember);
        try {
            boolean failure = false;
            while (iter.hasNext()) {
                Entry<Long, CordraTransaction> entry = iter.next();
                CordraTransaction txn = entry.getValue();
                if (failure) {
                    transactionReprocessingQueue.insert(txn, oldMember);
                    transactionManager.closeTransaction(txn.txnId, oldMember);
                } else {
                    try {
                        logger.info("Processing pending txn from ZK " + txn.objectId + " " + txn.txnId);
                        processPendingTransaction(txn);
                        transactionManager.closeTransaction(txn.txnId, oldMember);
                    } catch (Exception e) {
                        failure = true;
                        try {
                            transactionReprocessingQueue.insert(txn, oldMember);
                            transactionManager.closeTransaction(txn.txnId, oldMember);
                        } catch (Exception ex) {
                            logger.error("Exception processing old member txn; followed by reprocessing error", e);
                            throw ex;
                        }
                        logger.error("Exception processing old member txn", e);
                    }
                }
            }
            transactionManager.cleanup(oldMember);
        } finally {
            if (iter instanceof Closeable) try { ((Closeable)iter).close(); } catch (IOException e) { }
        }
    }

    private void processPendingTransaction(CordraTransaction txn) throws CordraException, IndexerException {
        objectLocker.lock(txn.objectId);
        try {
            CordraObject co = storage.get(txn.objectId);
            logger.info("Indexing pending txn " + txn.objectId);
            if (co != null) {
                boolean indexPayloads = shouldIndexPayloads(co.type);
                indexObject(co, indexPayloads);
                if (txn.isNeedToReplicate) sendUpdateReplicationMessage(co);
            } else {
                indexer.deleteObject(txn.objectId);
                if (txn.isNeedToReplicate) sendDeleteReplicationMessage(txn.objectId);
            }
        } finally {
            objectLocker.release(txn.objectId);
        }
    }

    // called only from factory initialization method
    public boolean initializeDesignCordraObjectIfNeeded() throws CordraException {
        designLocker.writeLock().acquire();
        boolean designObjectMigrated = false;
        //        CordraTransaction txn = null;
        try {
            //            txn = makeUpdateTransactionFor(DESIGN_OBJECT_ID);
            CordraObject designObject = getDesignCordraObject();
            if (designObject == null) {
                logger.info("Creating new design object.");
                designObject = createNewDesignCordraObject();
            }
            if (null == designObject.content || designObject.content.isJsonNull()) {
                migrateToDesignObjectFormat3(designObject);
                designObjectMigrated = true;
            }
            //            signalWatcher.sendSignal(SignalWatcher.Signal.DESIGN);
            //            sendUpdateReplicationMessage(designObject);
            //            transactionManager.closeTransaction(txn.txnId, cordraServiceId);
        } catch (ReadOnlyCordraException e) {
            throw new AssertionError();
            //        } catch (Exception e) {
            //            if (txn != null) {
            //                transactionReprocessingQueue.insert(txn, cordraServiceId);
            //                transactionManager.closeTransaction(txn.txnId, cordraServiceId);
            //            }
            //            throw e;
        } finally {
            designLocker.writeLock().release();
        }
        return  designObjectMigrated;
    }

    // called only from factory initialization method, via initializeDesignCordraObjectIfNeeded
    private CordraObject createNewDesignCordraObject() throws CordraException, ReadOnlyCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        CordraObject designObject = new CordraObject();
        designObject.id = DESIGN_OBJECT_ID;
        designObject.type = DESIGN_OBJECT_TYPE;
        Design localDesign = new Design();
        localDesign.uiConfig = gson.fromJson(getDefaultUiConfig(), UiConfig.class);
        localDesign.authConfig = AuthConfigFactory.getDefaultAuthConfig();
        localDesign.handleMintingConfig = HandleMintingConfig.getDefaultConfig();
        localDesign.handleServerConfig = HandleServerConfig.getDefaultNewCordraConfig();
        localDesign.doip = DoipServerConfigWithEnabledFlag.getDefaultNewCordraConfig();
        designObject.setContent(localDesign);
        designObject.metadata = new CordraObject.Metadata();
        designObject.metadata.internalMetadata = new JsonObject();
        designObject.metadata.internalMetadata.addProperty("version", "3");
        persistDesignToCordraObject(designObject, true, localDesign, Collections.emptyMap());
        return designObject;
    }

    private void migrateToDesignObjectFormat3(CordraObject designObject) throws CordraException, ReadOnlyCordraException {
        JsonElement uiConfigElement = designObject.metadata.internalMetadata.get("uiConfig");
        String uiConfigJson = uiConfigElement != null ? uiConfigElement.getAsString() : getDefaultUiConfig();
        UiConfig uiConfig = gson.fromJson(uiConfigJson, UiConfig.class);
        JsonElement authConfigJsonElement = designObject.metadata.internalMetadata.get("authConfig");
        AuthConfig authConfig;
        if (authConfigJsonElement == null) {
            authConfig = AuthConfigFactory.getDefaultAuthConfig();
        } else {
            authConfig = gson.fromJson(authConfigJsonElement.getAsString(), AuthConfig.class);
        }
        HandleMintingConfig handleMintingConfig;
        JsonElement handleMintingConfigElement = designObject.metadata.internalMetadata.get("handleMintingConfig");
        if (handleMintingConfigElement == null) {
            handleMintingConfig = HandleMintingConfig.getDefaultConfig();
        } else {
            handleMintingConfig = gson.fromJson(handleMintingConfigElement.getAsString(), HandleMintingConfig.class);
        }
        Map<String, String> schemaIds;
        JsonElement schemaIdsElement = designObject.metadata.internalMetadata.get("schemas");
        if (schemaIdsElement == null) {
            schemaIds = new HashMap<>();
        } else {
            schemaIds = gson.fromJson(schemaIdsElement.getAsString(), new TypeToken<Map<String, String>>() {}.getType());
        }
        designObject.type = DESIGN_OBJECT_TYPE;
        designObject.metadata.internalMetadata.addProperty("version", "3");
        designObject.metadata.internalMetadata.remove("meta");
        designObject.metadata.internalMetadata.remove("remoteRepositories");
        Design localDesign = new Design();
        localDesign.uiConfig = uiConfig;
        localDesign.authConfig = authConfig;
        localDesign.handleMintingConfig = handleMintingConfig;
        persistDesignToCordraObject(designObject, false, localDesign, schemaIds);
    }

    public void loadStatefulData() throws CordraException {
        designLocker.readLock().acquire();
        try {
            CordraObject designObject = getDesignCordraObject();
            logger.info("Loading stateful data from design object");
            Design designLite;
            if (designObject == null) {
                designLite = new Design();
            } else {
                designLite = GsonUtility.getGson().fromJson(designObject.content, Design.class);
            }
            if (designLite.uiConfig == null) designLite.uiConfig = gson.fromJson(getDefaultUiConfig(), UiConfig.class);
            if (designLite.authConfig == null) designLite.authConfig = AuthConfigFactory.getDefaultAuthConfig();
            if (designLite.handleMintingConfig == null) designLite.handleMintingConfig = HandleMintingConfig.getDefaultConfig();
            if (designLite.doip == null) {
                designLite.doip = cordraConfig.doip;
                // config.json doip did not have enabled flag
                if (designLite.doip != null) designLite.doip.enabled = true;
            }
            fixPrefixOnHandleMintingConfig(designLite.handleMintingConfig);
            boolean isHandleServerConfigChanged = isConfigChanged(design, designLite, des -> des.handleServerConfig);
            boolean isDoipServerConfigChanged = isConfigChanged(design, designLite, des -> des.doip) || isConfigChanged(design, designLite, des -> des.handleMintingConfig.prefix);
            Map<String, String> schemaIds = getSchemaIdsFromDesignObject(designObject);
            this.design = new DesignPlusSchemas(null, designLite, schemaIds);

            List<CordraObject> knownSchemaObjects = objectListFromHandleList(design.schemaIds.keySet());
            rebuildSchemasFromListOfObjects(knownSchemaObjects);

            String oldDesignJavaScript = cordraRequireLookup.getDesignJavaScript();
            if ((design.javascript == null && oldDesignJavaScript != null) || (design.javascript != null && !design.javascript.equals(oldDesignJavaScript))) {
                cordraRequireLookup.setDesignJavaScript(design.javascript);
                javaScriptEnvironment.clearCache();
                backgroundSchemaWarmUpExec.submit(() -> warmUpJavaScript(jsonSchemaFactory.getGeneration(), javaScriptWarmUpGeneration.incrementAndGet()));
            }

            aclEnforcer.setDesignAuthConfig(this.design.authConfig);
            this.handleMinter.setPrefix(design.handleMintingConfig.prefix);
            if (design.handleMintingConfig.handleAdminIdentity != null && privateKey != null) {
                ValueReference valRef = ValueReference.fromString(design.handleMintingConfig.handleAdminIdentity);
                authInfo = new PublicKeyAuthenticationInfo(valRef.handle, valRef.index, privateKey);
            }

            String oldHandleJavaScript = cordraRequireLookup.getHandleJavaScript();
            if ((design.handleMintingConfig.javascript == null && oldHandleJavaScript != null)
                    || (design.handleMintingConfig.javascript != null && !design.handleMintingConfig.javascript.equals(oldHandleJavaScript))) {
                cordraRequireLookup.setHandleJavaScript(design.handleMintingConfig.javascript);
                javaScriptEnvironment.clearCache();
                backgroundSchemaWarmUpExec.submit(() -> warmUpJavaScript(jsonSchemaFactory.getGeneration(), javaScriptWarmUpGeneration.incrementAndGet()));
            }

            if (!isReadOnly && authInfo != null) {
                if (!design.handleMintingConfig.isMintHandles()) {
                    this.handleClient = null;
                } else {
                    this.handleClient = new HandleClient(authInfo, design.handleMintingConfig, javaScriptHooks, doipServiceId);
                }
            } else {
                this.handleClient = null;
            }
            if (isHandleServerConfigChanged) {
                if (lightWeightHandleServer != null) {
                    lightWeightHandleServer.shutdown();
                    lightWeightHandleServer = null;
                }
                if (design.handleServerConfig != null) {
                    try {
                        if (design.handleServerConfig.enabled == Boolean.TRUE) {
                            if (handlePrivateKey == null && handlePublicKey == null) {
                                handlePrivateKey = privateKey;
                                handlePublicKey = publicKey;
                                //createAndSaveHandleKeys();
                            }
                            if (handlePrivateKey == null || handlePublicKey == null) {
                                throw new Exception("No keypair for internal handle server");
                            }
                            lightWeightHandleServer = new LightWeightHandleServer(handlePrivateKey, handlePublicKey, design.handleServerConfig, this::getHandleValues, getListenAddress());
                            lightWeightHandleServer.startInterfaces();
                        }
                    } catch (Exception e) {
                        logger.error("Could not start internal handle server", e);
                        System.out.println("Could not start internal handle server (see error.log for details)");
                    }
                }
            }
            if (isDoipServerConfigChanged) {
                restartDoipServiceAfterConfigChange();
            }
        } finally {
            boolean stayedLocked = designLocker.readLock().isLocked();
            designLocker.readLock().release();
            if (!stayedLocked) loadStatefulData();
        }
    }

    public String getListenAddress() {
        String res = cordraConfig.listenAddress;
        if (res != null) return res;
        return InetAddress.getLoopbackAddress().getHostAddress();
    }

    private void restartDoipServiceAfterConfigChange() {
        new Thread(this::restartDoipServiceAfterConfigChangeSync).start();
    }

    public JsonObject getDoipProcessorConfig() {
        return doipServerConfig.processorConfig;
    }

    public DoipProcessor getDoipProcessor() { return doipProcessor; }

    private void restartDoipServiceAfterConfigChangeSync() {
        synchronized (doipServerRestartSync) {
            if (doipServer != null) {
                doipServer.shutdown();
                doipServer = null;
            }
            doipServerConfig = fixUpDoipServerConfig(design.handleMintingConfig.prefix, design.doip);
            doipServiceId = doipServerConfig.processorConfig.get("serviceId").getAsString();
            doipServerConfig.enabled = true;
            if (handleClient != null) handleClient.setDoipServiceId(doipServiceId);
            CordraClientDoipProcessor newDoipProcessor = new CordraClientDoipProcessor(InternalCordraClientFactory.get());
            newDoipProcessor.init(doipServerConfig.processorConfig);
            doipProcessor = newDoipProcessor;
            if (design.doip != null) {
                try {
                    if (design.doip.enabled == Boolean.TRUE && design.doip.port > 0) {
                        doipServer = new DoipServer(doipServerConfig, doipProcessor);
                        System.out.println("Initializing DOIP interface on port " + doipServerConfig.port);
                        doipServer.init();
                    }
                } catch (Exception e) {
                    logger.error("Could not start DOIP server", e);
                    System.out.println("Could not start DOIP server (see error.log for details)");
                }
            }
        }
    }

    private DoipServerConfigWithEnabledFlag fixUpDoipServerConfig(String prefix, DoipServerConfigWithEnabledFlag doip) {
        DoipServerConfigWithEnabledFlag doipConfig = new DoipServerConfigWithEnabledFlag();
        if (doip != null) {
            doipConfig.enabled = doip.enabled;
            doipConfig.listenAddress = doip.listenAddress;
            doipConfig.port = doip.port;
            doipConfig.tlsConfig = new DoipServerConfig.TlsConfig();
            doipConfig.numThreads = doip.numThreads;
            doipConfig.backlog = doip.backlog;
            doipConfig.maxIdleTimeMillis = doip.maxIdleTimeMillis;
            if (doip.tlsConfig != null) {
                doipConfig.tlsConfig.id = doip.tlsConfig.id;
                doipConfig.tlsConfig.publicKey = doip.tlsConfig.publicKey;
                doipConfig.tlsConfig.privateKey = doip.tlsConfig.privateKey;
                doipConfig.tlsConfig.certificateChain = doip.tlsConfig.certificateChain;
            }
            if (doipConfig.listenAddress == null && doipSetupProvider != null) {
                doipConfig.listenAddress = doipSetupProvider.getListenAddress();
            }
            if (doipConfig.listenAddress == null) {
                doipConfig.listenAddress = "localhost";
            }
        }
        if (doip != null && doip.processorConfig != null) {
            doipConfig.processorConfig = doip.processorConfig.deepCopy();
        }
        if (doipConfig.processorConfig == null) {
            doipConfig.processorConfig = new JsonObject();
        }
        String id = null;
        if (doipConfig.processorConfig.has("serviceId")) {
            id = doipConfig.processorConfig.get("serviceId").getAsString();
        }
        if (id == null && doipConfig.tlsConfig != null) {
            id = doipConfig.tlsConfig.id;
        }
        if (id == null) id = prefix + "/service";
        if (doipConfig.tlsConfig != null) {
            if (doipConfig.tlsConfig.id == null) doipConfig.tlsConfig.id = id;
            if  (doipSetupProvider != null) {
                if ((doipConfig.tlsConfig.publicKey == null && doipConfig.tlsConfig.certificateChain == null) || doipConfig.tlsConfig.privateKey == null) {
                    doipConfig.tlsConfig.publicKey = doipSetupProvider.getPublicKey();
                    doipConfig.tlsConfig.privateKey = doipSetupProvider.getPrivateKey();
                    doipConfig.tlsConfig.certificateChain = doipSetupProvider.getCertChain();
                }
            }
            if (doipConfig.tlsConfig.publicKey == null && doipConfig.tlsConfig.certificateChain != null && doipConfig.tlsConfig.certificateChain.length > 0) {
                doipConfig.tlsConfig.publicKey = doipConfig.tlsConfig.certificateChain[0].getPublicKey();
            }
            if (!doipConfig.processorConfig.has("publicKey") && doipConfig.tlsConfig.publicKey != null) {
                doipConfig.processorConfig.add("publicKey", GsonUtility.getGson().toJsonTree(doipConfig.tlsConfig.publicKey));
            }
        }
        if (!doipConfig.processorConfig.has("serviceId")) {
            doipConfig.processorConfig.addProperty("serviceId", id);
        }
        if (!doipConfig.processorConfig.has("address") && doipConfig.listenAddress != null) {
            doipConfig.processorConfig.addProperty("address", doipConfig.listenAddress);
        }
        if (!doipConfig.processorConfig.has("port") && doipConfig.port > 0) {
            doipConfig.processorConfig.addProperty("port", doipConfig.port);
        }
        return doipConfig;
    }

    private boolean isConfigChanged(Design oldDesign, Design newDesign, Function<Design, Object> accessor) {
        Object oldConfig;
        Object newConfig;
        if (oldDesign == null) {
            oldConfig = null;
        } else {
            oldConfig = accessor.apply(oldDesign);
        }
        if (newDesign == null) {
            newConfig = null;
        } else {
            newConfig = accessor.apply(newDesign);
        }
        if (oldConfig == null) {
            if (newConfig == null) return false;
            else return true;
        }
        else if (newConfig == null) return true;
        else return !(oldConfig.equals(newConfig));
    }

    private Map<String, String> getSchemaIdsFromDesignObject(CordraObject designObject) {
        JsonElement schemaIds = designObject.metadata.internalMetadata.get("schemaIds");
        if (schemaIds == null) {
            return new HashMap<>();
        } else {
            return gson.fromJson(schemaIds.getAsString(), new TypeToken<Map<String,String>>() { }.getType());
        }
    }

    // called only from factory initialization method
    public void reindexEverythingIfIndexIsEmpty(boolean isBrandNewDesignObject) throws CordraException {
        // allow read-only instance to reindex
        //        if (isReadOnly) throw new ReadOnlyCordraException();
        boolean wasReindexInProcess = transactionManager.isReindexInProcess();
        if (wasReindexInProcess) {
            reindexer.reindexEverything(isBrandNewDesignObject);
            return;
        }
        QueryParams params = new QueryParams(0, 2, null);
        try (SearchResults<String> results = indexer.searchHandles("*:*", params)) {
            if (results.size() == 0) {
                reindexer.reindexEverything(isBrandNewDesignObject);
                return;
            }
            if (results.size() == 1) {
                String foundHandle = results.stream().findFirst().orElse(null);
                if (DESIGN_OBJECT_ID.equals(foundHandle)) {
                    reindexer.reindexEverything(isBrandNewDesignObject);
                    return;
                }
            }
        }
    }

    public void reindexBatchIds(List<String> batch, boolean lockObjects, AuthenticationResult authResult) throws CordraException {
        authorizer.authorizeReindexBatch(authResult);
        reindexer.indexBatch(batch, lockObjects);
    }

    public CordraStorage getStorage() {
        return storage;
    }

    // called only from factory initialization method
    public void updateKnownSchemasBySearch() throws CordraException, ReadOnlyCordraException {
        ensureIndexUpToDate();
        designLocker.writeLock().acquire();
        try {
            CordraObject designObject = this.getDesignCordraObject();
            Map<String, String> schemaIds = getSchemaIdsFromDesignObject(designObject);
            Set<String> newSchemaHandles = new HashSet<>();
            try (SearchResults<String> schemasResults = indexer.searchHandles("type:Schema -" + VersionManager.IS_VERSION + ":true")) {
                for (String handle : schemasResults) {
                    newSchemaHandles.add(handle);
                }
            } catch (UncheckedCordraException e) {
                e.throwCause();
            }
            if (newSchemaHandles.equals(schemaIds.keySet())) {
                return;
            }
            // ensure previously cached schemas still included even if search fails
            for (String id : schemaIds.keySet()) {
                CordraObject existingSchema = getCordraObjectOrNull(id);
                if (existingSchema != null && "Schema".equals(existingSchema.type) && !Boolean.TRUE.equals(existingSchema.metadata.isVersion)) {
                    newSchemaHandles.add(id);
                }
            }
            if (newSchemaHandles.equals(schemaIds.keySet())) {
                return;
            }
            logger.info("Schemas found by search differ from those cached in design; rebuilding");
            List<CordraObject> knownSchemaObjects = objectListFromHandleList(newSchemaHandles);
            rebuildSchemasFromListOfObjects(knownSchemaObjects);
            persistDesignToCordraObject(designObject);
        } finally {
            designLocker.writeLock().release();
        }
    }

    // called only from factory initialization method, via initializeDesignCordraObjectIfNeeded
    private static String getDefaultUiConfig() {
        InputStream resource = CordraService.class.getResourceAsStream("uiconfig.json");
        try {
            return StreamUtil.readFully(new InputStreamReader(resource, "UTF-8"));
        } catch (Exception e) {
            throw new AssertionError(e);
        } finally {
            try { resource.close(); } catch (Exception e) { }
        }
    }

    // called only from factory initialization method
    public void createDefaultSchemas() throws CordraException, ReadOnlyCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        designLocker.writeLock().acquire();
        try {
            List<CordraObject> newSchemas = new ArrayList<>();
            CordraObject user = createSchemaObject("User", DefaultSchemasFactory.getDefaultUserSchema(), DefaultSchemasFactory.getDefaultUserJavaScript(), null);
            newSchemas.add(user);
            CordraObject group = createSchemaObject("Group", DefaultSchemasFactory.getDefaultGroupSchema(), null, null);
            newSchemas.add(group);
            CordraObject doc = createSchemaObject("Document", DefaultSchemasFactory.getDefaultDocumentSchema(), null, null);
            newSchemas.add(doc);
            rebuildSchemasFromListOfObjects(newSchemas);
            persistDesignToCordraObject(getDesignCordraObject());
        } finally {
            designLocker.writeLock().release();
        }
    }

    // called only from factory initialization method
    public void updateUserSchemaIfNecessary() throws CordraException, ReadOnlyCordraException, InvalidException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        String schemaId = idFromTypeNoSearch("User");
        if (schemaId == null) return;
        CordraObject co = getCordraObject(schemaId);
        if (!co.content.isJsonObject()) return;
        if (!co.content.getAsJsonObject().has("javascript")) return;
        if (!co.content.getAsJsonObject().get("javascript").isJsonPrimitive()) return;
        String javascript = co.content.getAsJsonObject().get("javascript").getAsString();
        if (!DefaultSchemasFactory.getLegacyDefaultUserJavaScript().trim().equals(javascript.trim())) return;
        co.content.getAsJsonObject().addProperty("javascript", DefaultSchemasFactory.getLegacyUpdateDefaultUserJavaScript());
        writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(schemaId, null, co.getContentAsString(), null, null, null, AuthenticationResult.admin(), null, false);
    }

    // called only from factory initialization method, via createDefaultSchemas
    private CordraObject createSchemaObject(String schemaName, String schema, String javascript, String baseUri) throws CordraException, ReadOnlyCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        while (true) {
            String handle = handleMinter.mintByTimestamp();
            CordraTransaction txn = makeUpdateTransactionFor(handle);
            try {
                CordraObject co = new CordraObject();
                co.id = handle;
                initializeSchemaObject(co, schemaName, schema, javascript, txn.txnId, baseUri);
                try {
                    storage.create(co);
                } catch (ConflictCordraException e) {
                    transactionManager.closeTransaction(txn.txnId, cordraServiceId);
                    continue;
                }
                boolean indexPayloads = shouldIndexPayloads("Schema");
                indexObject(co, indexPayloads);
                sendUpdateReplicationMessage(co);
                transactionManager.closeTransaction(txn.txnId, cordraServiceId);
                return co;
            } catch (Exception e) {
                try {
                    transactionReprocessingQueue.insert(txn, cordraServiceId);
                    transactionManager.closeTransaction(txn.txnId, cordraServiceId);
                } catch (Exception ex) {
                    logger.error("Error in createSchemaObject; followed by reprocessing error", e);
                    throw ex;
                }
                throw e;
            }
        }
    }

    // used for testing; otherwise
    // called only from factory initialization method, via createDefaultSchemas
    public static void initializeSchemaObject(CordraObject co, String schemaName, String schema, String javascript, long txnId, String baseUri) {
        SchemaInstance schemaInstance = new SchemaInstance();
        schemaInstance.identifier = co.id;
        schemaInstance.name = schemaName;
        schemaInstance.schema = JsonParser.parseString(schema);
        schemaInstance.javascript = javascript;
        if (baseUri != null) {
            schemaInstance.baseUri = baseUri;
        }
        co.type = "Schema";
        co.setContent(schemaInstance);
        co.metadata = new CordraObject.Metadata();
        co.metadata.internalMetadata = new JsonObject();
        co.metadata.createdBy = "admin";
        co.metadata.modifiedBy = "admin";
        co.metadata.internalMetadata.addProperty("schemaName", schemaName);
        co.metadata.txnId = txnId;
        long now = System.currentTimeMillis();
        co.metadata.createdOn = now;
        co.metadata.modifiedOn = now;
    }

    // called only from factory initialization method AND loadStatefulData
    private List<CordraObject> objectListFromHandleList(Collection<String> knownSchemaHandles) throws CordraException {
        List<CordraObject> knownSchemaObjects = new ArrayList<>();
        for (String schemaHandle : knownSchemaHandles) {
            try {
                knownSchemaObjects.add(getCordraObject(schemaHandle));
            } catch (NotFoundCordraException e) {
                logger.warn("Could not find cordra object for listed schema " + schemaHandle);
            }
        }
        return knownSchemaObjects;
    }

    // called only from factory initialization method AND loadStatefulData
    // always called inside designLocker lock
    // Note: changes to this method should mirror changes made to addToKnownSchemas
    private void rebuildSchemasFromListOfObjects(List<CordraObject> objects) throws CordraException {
        //schemaJavaScriptModules.reloadAll(objects);
        ConcurrentMap<String, String> schemaJavaScripts = new ConcurrentHashMap<>();
        Map<String, String> schemaIds = new ConcurrentHashMap<>();
        Map<String, String> baseUriToNameMap = new ConcurrentHashMap<>();
        Map<String, String> nameToBaseUriMap = new ConcurrentHashMap<>();
        Map<String, LazySchema<JsonSchema>> newSchemas = new ConcurrentHashMap<>();
        Map<String, CordraObject> newSchemaCordraObjects = new ConcurrentHashMap<>();
        ConcurrentMap<String, String> schemaStrings = new ConcurrentHashMap<>();
        baseUriToNameMap.put(net.cnri.cordra.schema.SchemaUtil.getDefaultBaseUriFromSchemaName("Schema"), "Schema");
        baseUriToNameMap.put(net.cnri.cordra.schema.SchemaUtil.getDefaultBaseUriFromSchemaName("CordraDesign"), "CordraDesign");
        nameToBaseUriMap.put("Schema", net.cnri.cordra.schema.SchemaUtil.getDefaultBaseUriFromSchemaName("Schema"));
        nameToBaseUriMap.put("CordraDesign", net.cnri.cordra.schema.SchemaUtil.getDefaultBaseUriFromSchemaName("CordraDesign"));
        schemaStrings.put("Schema", JacksonUtil.printJson(SchemaSchemaFactory.getNode()));
        schemaStrings.put("CordraDesign", JacksonUtil.printJson(CordraDesignSchemaFactory.getNode()));
        newSchemas.put("Schema", new LazySchema<>(SchemaSchemaFactory.getNode(), null, SchemaSchemaFactory.getSchema()));
        newSchemas.put("CordraDesign", new LazySchema<>(CordraDesignSchemaFactory.getNode(), null, CordraDesignSchemaFactory.getSchema()));
        if (design.builtInTypes != null) {
            if (design.builtInTypes.CordraDesign != null && design.builtInTypes.CordraDesign.javascript != null && !design.builtInTypes.CordraDesign.javascript.isEmpty()) {
                schemaJavaScripts.put("CordraDesign", design.builtInTypes.CordraDesign.javascript);
            }
            if (design.builtInTypes.Schema != null && design.builtInTypes.Schema.javascript != null && !design.builtInTypes.Schema.javascript.isEmpty()) {
                schemaJavaScripts.put("Schema", design.builtInTypes.Schema.javascript);
            }
        }

        for (CordraObject schemaObject : objects) {
            String handle = schemaObject.id;
            JsonNode node = JacksonUtil.gsonToJackson(schemaObject.content);
            JsonNode schemaNode = JacksonUtil.getJsonAtPointer("/schema", node);
            String schemaString = JacksonUtil.printJson(schemaNode);
            String type = JacksonUtil.getJsonAtPointer("/name", node).asText();
            String baseUri = net.cnri.cordra.schema.SchemaUtil.getBaseUriFromSchemaCordraObject(schemaObject);
            String js = JacksonUtil.getJsonAtPointer("/javascript", node).asText();
            if (js != null && js.isEmpty()) js = null;
            baseUriToNameMap.put(baseUri, type);
            nameToBaseUriMap.put(type, baseUri);
            if (js != null && !js.isEmpty()) {
                schemaJavaScripts.put(type, js);
            }
            schemaStrings.put(type, schemaString);
            newSchemas.put(type, new LazySchema<>(schemaNode, baseUri, jsonSchemaFactory));
            newSchemaCordraObjects.put(type, schemaObject);
            schemaIds.put(handle, type);
        }
        checkForDuplicateSchemaNames(schemaIds);
        boolean needClear = false;
        Map<String, String> oldJavaScripts = cordraRequireLookup.getAllSchemaJavaScripts();
        if (!oldJavaScripts.equals(schemaJavaScripts)) {
            this.cordraRequireLookup.replaceAllSchemaJavaScript(schemaJavaScripts);
            needClear = true;
        }
        Map<String, String> oldSchemas = cordraRequireLookup.getAllSchemas();
        if (!oldSchemas.equals(schemaStrings)) {
            this.cordraRequireLookup.replaceAllSchemas(schemaStrings);
            needClear = true;
        }
        this.schemas = newSchemas;
        long gen = this.jsonSchemaFactory.incrementAndGetGeneration();
        backgroundSchemaWarmUpExec.submit(() -> parseAllSchemas(gen));
        if (needClear) {
            javaScriptEnvironment.clearCache();
            backgroundSchemaWarmUpExec.submit(() -> warmUpJavaScript(gen, javaScriptWarmUpGeneration.incrementAndGet()));
        }
        this.schemaCordraObjects = newSchemaCordraObjects;
        design.schemas = getSchemaNodes();
        design.schemaIds = schemaIds;
        design.baseUriToNameMap = baseUriToNameMap;
        design.nameToBaseUriMap = nameToBaseUriMap;
    }

    void checkForDuplicateSchemaNames(Map<String, String> schemaIds) {
        Map<String, List<String>> nameToIdMap = new HashMap<>();
        for (Map.Entry<String, String> entry : schemaIds.entrySet()) {
            String schemaId = entry.getKey();
            String name = entry.getValue();
            List<String> idsForName = nameToIdMap.get(name);
            if (idsForName == null) {
                idsForName = new ArrayList<>();
                nameToIdMap.put(name, idsForName);
            }
            idsForName.add(schemaId);
        }
        for (Map.Entry<String, List<String>> nameEntry : nameToIdMap.entrySet()) {
            String name = nameEntry.getKey();
            List<String> idsForName = nameEntry.getValue();
            if (idsForName.size() > 1) {
                String message = "More than one schema has the same name '"+ name + "': " + gson.toJson(idsForName);
                System.out.println("Error: " + message);
                logger.error(message);
            }
        }
    }

    private void parseAllSchemas(long generation) {
        try {
            if (cordraConfig.traceRequests) {
                logger.trace("Parsing all schemas (generation: " + generation + ")");
            }
            for (Map.Entry<String, LazySchema<JsonSchema>> entry : schemas.entrySet()) {
                if (backgroundSchemaWarmUpExec.isShutdown()) return;
                if (jsonSchemaFactory.getGeneration() != generation) return;
                try {
                    entry.getValue().getSchema();
                } catch (InvalidException e) {
                    logger.warn("Exception pre-parsing schema " + entry.getKey(), e);
                }
            }
            if (cordraConfig.traceRequests) {
                logger.trace("Done parsing all schemas (generation: " + generation + ")");
            }
        } catch (Throwable t) {
            logger.error("Exception pre-parsing all schemas", t);
        }
    }

    private void warmUpJavaScript(long schemaGen, long javaScriptGen) {
        try {
            if (backgroundSchemaWarmUpExec.isShutdown()) return;
            if (jsonSchemaFactory.getGeneration() != schemaGen) return;
            if (javaScriptWarmUpGeneration.get() != javaScriptGen) return;
            if (cordraConfig.traceRequests) {
                logger.trace("Warming up JavaScript (schemaGen: " + schemaGen + ", javaScriptGen: " + javaScriptGen + ")");
            }
            javaScriptHooks.warmUpModule(CordraRequireLookup.DESIGN_MODULE_ID);
            for (String schemaName : schemas.keySet()) {
                if (backgroundSchemaWarmUpExec.isShutdown()) return;
                if (jsonSchemaFactory.getGeneration() != schemaGen) return;
                if (javaScriptWarmUpGeneration.get() != javaScriptGen) return;
                javaScriptHooks.warmUpModule(CordraRequireLookup.moduleIdForSchemaType(schemaName));
            }
            if (cordraConfig.traceRequests) {
                logger.trace("Done warming up JavaScript (schemaGen: " + schemaGen + ", javaScriptGen: " + javaScriptGen + ")");
            }
        } catch (Throwable t) {
            logger.error("Exception warming up JavaScript", t);
        }
    }

    // admin API
    public void updateHandleMintingConfig(HandleMintingConfig handleMintingConfig) throws CordraException, ReadOnlyCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        fixPrefixOnHandleMintingConfig(handleMintingConfig);
        String baseUri = handleMintingConfig.baseUri;
        designLocker.writeLock().acquire();
        //CordraTransaction txn = null;
        try {
            String oldHandleJavaScript = cordraRequireLookup.getHandleJavaScript();
            if ((handleMintingConfig.javascript == null && oldHandleJavaScript != null)
                    || (handleMintingConfig.javascript != null && !handleMintingConfig.javascript.equals(oldHandleJavaScript))) {
                cordraRequireLookup.setHandleJavaScript(handleMintingConfig.javascript);
                javaScriptEnvironment.clearCache();
                backgroundSchemaWarmUpExec.submit(() -> warmUpJavaScript(jsonSchemaFactory.getGeneration(), javaScriptWarmUpGeneration.incrementAndGet()));
            }
            if (this.design.handleMintingConfig.prefix != handleMintingConfig.prefix) {
                restartDoipServiceAfterConfigChange();
            }
            if (!handleMintingConfig.isMintHandles()) {
                handleClient = null;
            } else {
                baseUri = ensureSlash(baseUri);
                handleMintingConfig.baseUri = baseUri;
                if (handleMintingConfig.handleAdminIdentity != null && privateKey != null) {
                    ValueReference valRef = ValueReference.fromString(handleMintingConfig.handleAdminIdentity);
                    authInfo = new PublicKeyAuthenticationInfo(valRef.handle, valRef.index, privateKey);
                }
                if (authInfo != null) {
                    handleClient = new HandleClient(authInfo, handleMintingConfig, javaScriptHooks, doipServiceId);
                }
            }
            this.handleMinter.setPrefix(handleMintingConfig.prefix);
            //txn = makeUpdateTransactionFor(DESIGN_OBJECT_ID);
            CordraObject designObject = getDesignCordraObject();
            design.handleMintingConfig = handleMintingConfig;
            persistDesignToCordraObject(designObject);
            loadStatefulData();
            //signalWatcher.sendSignal(SignalWatcher.Signal.DESIGN);
            //sendUpdateReplicationMessage(designObject);
            //transactionManager.closeTransaction(txn.txnId, cordraServiceId);
            //        } catch (Exception e) {
            //            if (txn != null) {
            //                transactionReprocessingQueue.insert(txn, cordraServiceId);
            //                transactionManager.closeTransaction(txn.txnId, cordraServiceId);
            //            }
            //            throw e;
        } finally {
            designLocker.writeLock().release();
        }
    }

    private static void fixPrefixOnHandleMintingConfig(HandleMintingConfig handleMintingConfig) {
        if (handleMintingConfig.prefix == null || handleMintingConfig.prefix.isEmpty()) {
            handleMintingConfig.prefix = "test";
        }
        if (handleMintingConfig.prefix.toUpperCase(Locale.ENGLISH).startsWith("0.NA/")) {
            handleMintingConfig.prefix = handleMintingConfig.prefix.substring(5);
        }
        if (handleMintingConfig.prefix.endsWith("/")) {
            handleMintingConfig.prefix = handleMintingConfig.prefix.substring(0, handleMintingConfig.prefix.length() - 1);
        }
    }

    // admin API
    public void updateDesign(Design designUpdate) throws CordraException, ReadOnlyCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        designLocker.writeLock().acquire();
        try {
            CordraObject designObject = getDesignCordraObject();
            Design combinedDesign = new Design();
            combinedDesign.merge(design);
            combinedDesign.merge(designUpdate);
            persistDesignToCordraObject(designObject, false, combinedDesign, design.schemaIds);
            loadStatefulData();
        } finally {
            designLocker.writeLock().release();
        }
    }

    public List<String> getIds() {
        if (design.ids != null) return design.ids;
        if (design.handleMintingConfig != null) {
            String baseUri = design.handleMintingConfig.baseUri;
            if (baseUri != null) {
                return Arrays.asList(ensureSlash(baseUri), ensureNoSlash(baseUri));
            }
        }
        return Collections.emptyList();
    }

    private static String ensureSlash(String s) {
        if (s == null) return null;
        if (s.endsWith("/")) return s;
        else return s + "/";
    }

    private static String ensureNoSlash(String s) {
        if (s == null) return null;
        if (s.endsWith("/")) return s.substring(0, s.length() - 1);
        else return s;
    }

    // admin API
    public void updateAuthConfig(AuthConfig authConfig) throws CordraException, ReadOnlyCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        designLocker.writeLock().acquire();
        try {
            design.authConfig = authConfig;
            aclEnforcer.setDesignAuthConfig(authConfig);
            CordraObject designObject = getDesignCordraObject();
            persistDesignToCordraObject(designObject);
        } finally {
            designLocker.writeLock().release();
        }
    }

    private void persistDesignToCordraObject(CordraObject designObject) throws CordraException, ReadOnlyCordraException {
        persistDesignToCordraObject(designObject, false, design, design.schemaIds);
    }

    private void persistDesignToCordraObject(CordraObject designObject, boolean isCreate, Design designLite, Map<String, String> schemaIds) throws CordraException, ReadOnlyCordraException {
        String designJson = gson.toJson(designLite);
        if (designLite instanceof DesignPlusSchemas) {
            // remove schemas
            designLite = gson.fromJson(designJson, Design.class);
            designJson = gson.toJson(designLite);
        }
        if (designObject.metadata == null) designObject.metadata = new CordraObject.Metadata();
        if (designObject.metadata.internalMetadata == null) designObject.metadata.internalMetadata = new JsonObject();
        designObject.metadata.internalMetadata.addProperty("schemaIds", gson.toJson(schemaIds, new TypeToken<Map<String, String>>() {}.getType()));
        updateCordraObject(designObject, isCreate, "CordraDesign", designJson, null, null, null, null, "admin", null, false, false);
        signalWatcher.sendSignal(SignalWatcher.Signal.DESIGN);
    }

    // admin API
    public void setAdminPassword(String password) throws Exception {
        if (isReadOnly) throw new ReadOnlyCordraException();
        if (password == null || password.isEmpty()) {
            throw new BadRequestCordraException("Empty admin password is not allowed");
        }
        HashAndSalt hashAndSalt = new HashAndSalt(password, HashAndSalt.HASH_ITERATION_COUNT_10K, HashAndSalt.PBKDF2WithHmacSHA1);
        String hash = hashAndSalt.getHashString();
        String salt = hashAndSalt.getSaltString();
        String iterationsString = hashAndSalt.getIterations().toString();
        String alg = hashAndSalt.getAlgorithm();
        designLocker.writeLock().acquire();
        try {
            CordraObject designObject = getDesignCordraObject();
            designObject.metadata.internalMetadata.addProperty(HASH_ATTRIBUTE, hash);
            designObject.metadata.internalMetadata.addProperty(SALT_ATTRIBUTE, salt);
            designObject.metadata.internalMetadata.addProperty(ITERATIONS_ATTRIBUTE, iterationsString);
            designObject.metadata.internalMetadata.addProperty(HASH_ALGORITHM_ATTRIBUTE, alg);
            persistDesignToCordraObject(designObject);
        } finally {
            designLocker.writeLock().release();
        }
    }

    public List<HandleValue> getHandleValues(String handle) throws CordraException {
        CordraObject co = getCordraObjectOrNull(handle);
        if (co == null) {
            if (doipServerConfig != null && doipServerConfig.enabled && handle.equals(doipServiceId)) {
                return HandleClient.createDoipServiceHandleValues(doipServerConfig);
            }
            throw new NotFoundCordraException("Missing object: " + handle);
        }
        co = copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        JsonNode dataNode = JacksonUtil.gsonToJackson(co.content);
        List<HandleValue> result = null;
        try {
            Map<String, Object> context = new HashMap<>();
            context.put("objectId", co.id);
            context.put("effectiveAcl", getAclEnforcer().getEffectiveAcl(co));
            result = HandleClient.createHandleValues(co, co.type, dataNode, design.handleMintingConfig, javaScriptHooks, null, doipServiceId, context);
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
        return result;
    }

    // admin API
    public UpdateStatus getHandleUpdateStatus(AuthenticationResult authResult) throws CordraException {
        authorizer.authorizeGetHandleUpdateStatus(authResult);
        return this.handlesUpdater.getStatus();
    }

    // admin API
    public void updateAllHandleRecords(AuthenticationResult authResult) throws ReadOnlyCordraException, CordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        authorizer.authorizeUpdateAllHandleRecords(authResult);
        handlesUpdater.updateAllHandles(handleClient, this);
    }

    public boolean checkAdminPassword(String password) throws CordraException {
        CordraObject designObject = getDesignCordraObject();
        if (designObject == null) return false;
        JsonElement hash = designObject.metadata.internalMetadata.get(HASH_ATTRIBUTE);
        JsonElement salt = designObject.metadata.internalMetadata.get(SALT_ATTRIBUTE);
        if (hash == null || salt == null) {
            return false;
        }
        String hashString = hash.getAsString();
        String saltString = salt.getAsString();
        JsonElement iterationsElement = designObject.metadata.internalMetadata.get(ITERATIONS_ATTRIBUTE);
        int iterations;
        if (iterationsElement != null) {
            iterations = iterationsElement.getAsInt();
        } else {
            iterations = HashAndSalt.LEGACY_HASH_ITERATION_COUNT_2048;
        }
        JsonElement algorithm = designObject.metadata.internalMetadata.get(HASH_ALGORITHM_ATTRIBUTE);
        String algString = algorithm != null ? algorithm.getAsString() : null;
        HashAndSalt hashAndSalt = new HashAndSalt(hashString, saltString, iterations, algString);
        return hashAndSalt.verifySecret(password);
    }

    public boolean isAdminPasswordSet() throws CordraException {
        CordraObject designObject = getDesignCordraObject();
        if (designObject == null) return false;
        JsonElement hash = designObject.metadata.internalMetadata.get(HASH_ATTRIBUTE);
        JsonElement salt = designObject.metadata.internalMetadata.get(SALT_ATTRIBUTE);
        if (hash == null || salt == null) {
            return false;
        } else {
            return true;
        }
    }

    public AclEnforcer getAclEnforcer() {
        return aclEnforcer;
    }

    public InternalAuthorizer getAuthorizer() {
        return authorizer;
    }

    public boolean isKnownType(String type) {
        return type != null && schemas.containsKey(type);
    }

    public LazySchema<JsonSchema> getSchema(String type) {
        if (type == null) return null;
        return schemas.get(type);
    }

    private JsonSchemaFactory supplyJsonSchemaFactory() {
        return JsonSchemaFactoryFactory.newJsonSchemaFactory(this::resolveSchema);
    }

    public SchemaNodeAndUri resolveSchema(String uriString) {
        if (uriString == null) return null;
        String type = design.baseUriToNameMap.get(uriString);
        if (type == null) {
            type = net.cnri.cordra.schema.SchemaUtil.getNameFromCordraSchemasUri(uriString);
        }
        if (type == null) return null;
        LazySchema<JsonSchema> schemaAndNode = schemas.get(type);
        if (schemaAndNode == null) return null;
        return new SchemaNodeAndUri(schemaAndNode.getSchemaNode(), schemaAndNode.getBaseUri());
    }

    public String idFromTypeNoSearch(String type) {
        if (DESIGN_OBJECT_TYPE.equals(type)) return DESIGN_OBJECT_ID;
        if ("Schema".equals(type)) return DESIGN_OBJECT_ID;
        for (Map.Entry<String, String> schemaIdAndName : design.schemaIds.entrySet()) {
            String schemaId = schemaIdAndName.getKey();
            String schemaName = schemaIdAndName.getValue();
            if (schemaName.equals(type)) {
                return schemaId;
            }
        }
        return null;
    }

    public String idFromType(String type) throws CordraException {
        ensureIndexUpToDate();
        try (SearchResults<CordraObject> results = indexer.search("+type:Schema +schemaName:\"" + SearchUtil.escape(type) + "\" -isVersion:true -objatt_isVersion:true");) {
            for (CordraObject co : results) {
                String name = co.content.getAsJsonObject().get("name").getAsString();
                if (type.equals(name)) {
                    return co.id;
                }
            }
        } catch (UncheckedCordraException e) {
            throw e.getCause();
        }
        return null;
    }

    private CordraObject getDesignCordraObject() throws CordraException {
        return storage.get(DESIGN_OBJECT_ID);
    }

    public DesignPlusSchemas getDesign() {
        return design;
    }

    public boolean isDisableBackOffRequestParking() {
        Boolean disableBackOffRequestParking = getDesign().disableBackOffRequestParking;
        if (disableBackOffRequestParking == null) {
            return false;
        } else {
            return disableBackOffRequestParking;
        }
    }

    public DesignPlusSchemas getDesignAsUser(AuthenticationResult authResult) throws CordraException, ScriptException, InterruptedException {
        Design visibleDesign;
        try {
            CordraObject designObject = getContentPlusMetaWithPostProcessing(DESIGN_OBJECT_ID, authResult);
            visibleDesign = gson.fromJson(designObject.content, Design.class);
        } catch (UnauthorizedCordraException | NotFoundCordraException e) {
            visibleDesign = new Design();
        } catch (InternalErrorCordraException | ScriptException | InterruptedException e) {
            logger.warn("Unexpected error in getDesignAsUser, bypassing", e);
            visibleDesign = new Design();
            // This allows Cordra UI to continue to work even if there is bad design JavaScript
        }
        SearchRequest queryAndParams = null;
        try {
            queryAndParams = customizeAndRestrictQuery("type:Schema", QueryParams.DEFAULT, false, authResult, true);
        } catch (InternalErrorCordraException | ScriptException | InterruptedException e) {
            logger.warn("Unexpected error in getDesignAsUser, bypassing", e);
            // This allows Cordra UI to continue to work even if there is bad design JavaScript
        }
        Map<String, String> schemaIds = new LinkedHashMap<>();
        @SuppressWarnings("hiding")
        Map<String, JsonNode> schemas = new LinkedHashMap<>();
        schemas.put("CordraDesign", CordraDesignSchemaFactory.getNode());
        schemas.put("Schema", SchemaSchemaFactory.getNode());
        if (queryAndParams != null) {
            try (SearchResults<CordraObject> results = search(queryAndParams.query, queryAndParams.getParams())) {
                for (CordraObject co : results) {
                    try {
                        co = postProcessForSearch(authResult, co);
                    } catch (InternalErrorCordraException e) {
                        throw e;
                    } catch (CordraException e) {
                        // onObjectResolution enrichment may have forbidden access.
                        continue;
                    }
                    if (co.content == null || co.id == null) continue;
                    if (!co.content.isJsonObject()) continue;
                    String name = co.content.getAsJsonObject().get("name").getAsString();
                    JsonElement schema = co.content.getAsJsonObject().get("schema");
                    JsonNode schemaNode = JacksonUtil.gsonToJackson(schema);
                    schemaIds.put(co.id, name);
                    schemas.put(name, schemaNode);
                }
            }
        }
        DesignPlusSchemas res = new DesignPlusSchemas(schemas, visibleDesign, schemaIds);
        res.baseUriToNameMap = design.baseUriToNameMap.entrySet().stream()
            .filter(entry -> schemas.containsKey(entry.getValue()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        res.nameToBaseUriMap = design.nameToBaseUriMap.entrySet().stream()
            .filter(entry -> schemas.containsKey(entry.getKey()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return res;
    }

    public InputStream readPayload(String objectId, String payloadName) throws CordraException {
        InputStream res = storage.getPayload(objectId, payloadName);
        if (res == null) throw new NotFoundCordraException("Missing payload: " + objectId + " payload " + payloadName);
        return res;
    }

    public static Payload getCordraObjectPayloadByName(CordraObject co, String payloadName) {
        if (co.payloads == null) return null;
        for (Payload payload : co.payloads) {
            if (payload.name.equals(payloadName)) {
                return payload;
            }
        }
        return null;
    }

    public JsonElement getObjectFilterByJsonPointers(String objectId, AuthenticationResult authResult, Collection<String> filter, boolean isFull) throws CordraException {
        CordraObject co = getCordraObjectWithNoInternalMetadata(objectId);
        authorizer.authorizeRead(authResult, co);
        try {
            co = postProcess(authResult, co);
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
        JsonElement result;
        if (isFull) {
            JsonElement jsonElement = gson.toJsonTree(co, CordraObject.class);
            result = JsonUtil.pruneToMatchPointers(jsonElement, filter);
        } else {
            result = JsonUtil.pruneToMatchPointers(co.content, filter);
        }
        return result;
    }

    public JsonElement getAtJsonPointer(String objectId, AuthenticationResult authResult, String jsonPointer) throws CordraException {
        CordraObject co = getCordraObjectWithNoInternalMetadata(objectId);
        authorizer.authorizeRead(authResult, co);
        try {
            co = postProcess(authResult, co);
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
        if (co.content == null) throw new InternalErrorCordraException("Missing JSON attribute on " + objectId);
        JsonElement jsonElement = gson.toJsonTree(co, CordraObject.class);
        if (!JsonUtil.isValidJsonPointer(jsonPointer)) return null;
        JsonElement jsonSubElement = JsonUtil.getJsonAtPointer(jsonElement, jsonPointer);
        return jsonSubElement;
    }

    public FileMetadataResponse getPayloadMetadata(String objectId, String payloadName, AuthenticationResult authResult) throws CordraException, ScriptException, InterruptedException {
        CordraObject co = getCordraObjectWithNoInternalMetadata(objectId);
        authorizer.authorizeRead(authResult, co);
        co = postProcessForPayload(authResult, co, payloadName, null, null);
        Payload payload = getCordraObjectPayloadByName(co, payloadName);
        if (payload == null) return null;
        payload.setInputStream(null);
        return new FileMetadataResponse(payload.filename, payload.mediaType);
    }

    public CordraObject getCordraObjectForPayload(String objectId, String payloadName, Long start, Long end, AuthenticationResult authResult) throws CordraException, ScriptException, InterruptedException {
        CordraObject co = getCordraObjectWithNoInternalMetadata(objectId);
        authorizer.authorizeReadPayloads(authResult, co);
        co = postProcessForPayload(authResult, co, payloadName, start, end);
        return co;
    }

    public InputStream getPayloadByNameOrNull(String objectId, String payloadName, Long start, Long end) throws CordraException {
        CordraObject co = getCordraObjectOrNull(objectId);
        if (co == null) return null;
        Payload payload = getCordraObjectPayloadByName(co, payloadName);
        if (payload == null) return null;
        return getPayload(co, payload, new Range(start, end));
    }

    public InputStream getPayload(CordraObject co, Payload payload, Range range) throws CordraException {
        String objectId = co.id;
        String payloadName = payload.name;
        long size = payload.size;
        InputStream stream;
        if (size <= 0) {
            stream = storage.getPayload(objectId, payloadName);
        } else {
            range = range.withSize(size);
            if (range.isPartial()) {
                stream = storage.getPartialPayload(objectId, payloadName, range.getStart(), range.getEnd());
            } else {
                stream = storage.getPayload(objectId, payloadName);
            }
        }
        return stream;
    }

    public void streamPayload(CordraObject co, Payload payload, Range range, AuthenticationResult authResult, DirectIo directIo) throws IOException, CordraException, ScriptException, InterruptedException {
        Map<String, Object> context = createContextForPayload(authResult, co, payload.name, range.getStart(), range.getEnd());
        if (javaScriptHooks.streamPayload(co, context, directIo)) {
            return;
        }
        try (InputStream input = getPayload(co, payload, range)) {
            if (input == null) {
                throw new InternalErrorCordraException("Unexpected null payload stream");
            }
            IOUtils.copy(input, directIo.getOutputAsOutputStream());
        }
    }

    public String getObjectJson(String objectId) throws CordraException {
        CordraObject co = getCordraObject(objectId);
        String jsonData = co.getContentAsString();
        if (jsonData == null) throw new InternalErrorCordraException("Missing JSON attribute on " + objectId);
        return jsonData;
    }

    public String getFullObjectJson(String objectId) throws CordraException {
        CordraObject co = getCordraObjectWithNoInternalMetadata(objectId);
        return gson.toJson(co);
    }

    public CordraObject getCordraObject(String objectId) throws CordraException {
        CordraObject co = storage.get(objectId);
        if (co == null) throw new NotFoundCordraException("Missing object: " + objectId);
        return co;
    }

    public CordraObject getCordraObjectOrNull(String objectId) throws CordraException {
        return storage.get(objectId);
    }

    public CordraObject getCordraObjectOrNullWithInternalMetadataAndPostProcess(String objectId, AuthenticationResult authResult) throws CordraException, ScriptException, InterruptedException {
        CordraObject co = storage.get(objectId);
        if (co == null) return null;
        CordraObject coResult = postProcess(authResult, co);
        restoreInternalMetadata(co, coResult);
        return coResult;
    }

    public SearchResults<CordraObject> getObjects(Collection<String> ids) throws CordraException {
        return storage.get(ids);
    }

    public boolean doesCordraObjectExist(String objectId) throws CordraException {
        return storage.get(objectId) != null;
    }

    public static CordraObject copyOfCordraObjectRemovingInternalMetadata(CordraObject co) {
        CordraObject res = new CordraObject();
        res.id = co.id;
        res.type = co.type;
        res.content = co.content;
        res.acl = co.acl;
        res.payloads = co.payloads;
        res.userMetadata = co.userMetadata;
        res.responseContext = co.responseContext;
        if (co.metadata == null) return res;
        res.metadata = new CordraObject.Metadata();
        res.metadata.createdBy = co.metadata.createdBy;
        res.metadata.createdOn = co.metadata.createdOn;
        res.metadata.isVersion = co.metadata.isVersion;
        res.metadata.modifiedBy = co.metadata.modifiedBy;
        res.metadata.modifiedOn = co.metadata.modifiedOn;
        res.metadata.publishedBy = co.metadata.publishedBy;
        res.metadata.publishedOn = co.metadata.publishedOn;
        res.metadata.hashes = co.metadata.hashes;
        res.metadata.remoteRepository = co.metadata.remoteRepository; // presumably disused; remove from cordra-client.jar if so
        res.metadata.txnId = co.metadata.txnId;
        res.metadata.versionOf = co.metadata.versionOf;
        return res;
    }

    public static CordraObject copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(CordraObject co) {
        if (co == null || co.metadata == null || co.metadata.internalMetadata == null) return co;
        return copyOfCordraObjectRemovingInternalMetadata(co);
    }

    public static void restoreInternalMetadata(CordraObject co, CordraObject coResult) {
        if (co == null || co.metadata == null || co.metadata.internalMetadata == null) return;
        if (coResult == null) return;
        if (coResult.metadata == null) {
            coResult.metadata = new CordraObject.Metadata();
        }
        if (coResult.metadata.internalMetadata == null) {
            coResult.metadata.internalMetadata = new JsonObject();
        }
        for (Map.Entry<String, JsonElement> entry : co.metadata.internalMetadata.entrySet()) {
            if (!coResult.metadata.internalMetadata.has(entry.getKey())) {
                coResult.metadata.internalMetadata.add(entry.getKey(), entry.getValue());
            }
        }
    }

    // may be original
    public CordraObject getCordraObjectWithNoInternalMetadata(String objectId) throws CordraException {
        CordraObject co = getCordraObject(objectId);
        return copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
    }

    //return value will always be copy of cordra object
    public CordraObject getContentPlusMetaWithPostProcessing(String objectId, AuthenticationResult authResult) throws CordraException, ScriptException, InterruptedException {
        CordraObject co = getCordraObject(objectId);
        authorizer.authorizeRead(authResult, co);
        co = copyOfCordraObjectRemovingInternalMetadata(co);
        return postProcess(authResult, co);
    }

    public CordraObject postProcess(AuthenticationResult authResult, CordraObject co) throws CordraException, ScriptException, InterruptedException {
        Map<String, Object> context = createContext(authResult, co);
        return this.javaScriptHooks.onObjectResolution(co, context);
    }

    public CordraObject postProcessForSearch(AuthenticationResult authResult, CordraObject co) throws CordraException, ScriptException, InterruptedException {
        Map<String, Object> context = createContext(authResult, co);
        context.put("isSearch", true);
        return this.javaScriptHooks.onObjectResolution(co, context);
    }

    private CordraObject postProcessForPayload(AuthenticationResult authResult, CordraObject co, String payloadName, Long start, Long end) throws CordraException, ScriptException, InterruptedException {
        Map<String, Object> context = createContextForPayload(authResult, co, payloadName, start, end);
        return this.javaScriptHooks.onObjectResolution(co, context);
    }

    private Map<String, Object> createContextForPayload(AuthenticationResult authResult, CordraObject co, String payloadName, Long start, Long end) {
        Map<String, Object> context = createContext(authResult, co);
        context.put("payload", payloadName);
        if (start != null) context.put("start", start);
        if (end != null) context.put("end", end);
        return context;
    }

    private Map<String, Object> createContext(AuthenticationResult authResult, CordraObject co) {
        Map<String, Object> context = new HashMap<>();
        context.put("objectId", co.id);
        context.put("userId", authResult.userId);
        context.put("effectiveAcl", getAclEnforcer().getEffectiveAcl(co));
        context.put("groups", authResult.groupIds);
        return context;
    }

    public CordraObject.AccessControlList getAclFor(String objectId, AuthenticationResult authResult) throws CordraException, ScriptException, InterruptedException {
        CordraObject co = getContentPlusMetaWithPostProcessing(objectId, authResult);
        return co.acl;
    }

    private CordraTransaction makeUpdateTransactionFor(String objectId, boolean isNeedToReplicate) throws CordraException {
        long txnId = transactionManager.getAndIncrementNextTransactionId();
        Long now = System.currentTimeMillis();
        CordraTransaction txn = new CordraTransaction(txnId, now, objectId, CordraTransaction.OP.UPDATE, isNeedToReplicate);
        transactionManager.openTransaction(txnId, cordraServiceId, txn);
        return txn;
    }

    private CordraTransaction makeDeleteTransactionFor(String objectId, boolean isNeedToReplicate) throws CordraException {
        long txnId = transactionManager.getAndIncrementNextTransactionId();
        Long now = System.currentTimeMillis();
        CordraTransaction txn = new CordraTransaction(txnId, now, objectId, CordraTransaction.OP.DELETE, isNeedToReplicate);
        transactionManager.openTransaction(txnId, cordraServiceId, txn);
        return txn;
    }

    private CordraTransaction makeUpdateTransactionFor(String objectId) throws CordraException {
        return makeUpdateTransactionFor(objectId, true);
    }

    public void delete(String objectId, AuthenticationResult authResult) throws CordraException, ReadOnlyCordraException {
        delete(objectId, authResult, true);
    }

    public void delete(String objectId, AuthenticationResult authResult, boolean isNeedToAuthorizeAndRunHooksAndReplicate) throws CordraException, ReadOnlyCordraException {
        if (isReadOnly && isNeedToAuthorizeAndRunHooksAndReplicate) throw new ReadOnlyCordraException();
        objectLocker.lock(objectId);
        CordraTransaction txn = null;
        CordraObject co = null;
        Map<String, Object> context = null;
        try {
            co = storage.get(objectId);
            String type = co == null ? null : co.type;
            if (isNeedToAuthorizeAndRunHooksAndReplicate) {
                if (co == null) {
                    throw new NotFoundCordraException("Missing object: " + objectId);
                } else if (DESIGN_OBJECT_ID.equals(objectId)) {
                    throw new InternalErrorCordraException("Object not valid for deletion: " + objectId);
                } else {
                    authorizer.authorizeWrite(authResult, co);
                    try {
                        context = createContext(authResult, co);
                        javaScriptHooks.beforeDelete(co, context);
                    } catch (CordraException e) {
                        throw e;
                    } catch (Exception e) {
                        throw new InternalErrorCordraException(e);
                    }
                }
            }
            txn = this.makeDeleteTransactionFor(objectId, isNeedToAuthorizeAndRunHooksAndReplicate);
            if ("Schema".equals(type)) {
                designLocker.writeLock().acquire();
                try {
                    storage.delete(objectId);
                    deleteFromKnownSchemas(objectId);
                } finally {
                    designLocker.writeLock().release();
                }
            } else if (co != null) {
                storage.delete(objectId);
            }
            if (handleClient != null) {
                try {
                    handleClient.deleteHandle(objectId);
                } catch (HandleException e) {
                    logger.warn("Failure to delete handle " + objectId + ", out of sync", e);
                    // throw new InternalErrorCordraException(e);
                }
            }
            indexer.deleteObject(objectId);
            if (isNeedToAuthorizeAndRunHooksAndReplicate) sendDeleteReplicationMessage(objectId);
            if (isUserOrGroup(co)) {
                authObjectChangeCount.incrementAndGet();
                authCache.clearAllGroupsForUserValues();
                authCache.clearAllUserIdForUsernameValues();
                signalWatcher.sendSignal(SignalWatcher.Signal.AUTH_CHANGE);
                if (co != null) {
                    invalidateSessionsForUser(co.id);
                }
                preCache();
            }
            transactionManager.closeTransaction(txn.txnId, cordraServiceId);
        } catch (Exception e) {
            if (txn != null) {
                try {
                    transactionReprocessingQueue.insert(txn, cordraServiceId);
                    transactionManager.closeTransaction(txn.txnId, cordraServiceId);
                } catch (Exception ex) {
                    logger.error("Error in delete; followed by reprocessing error", e);
                    throw ex;
                }
            }
            throw e;
        } finally {
            objectLocker.release(objectId);
        }
        if (isNeedToAuthorizeAndRunHooksAndReplicate) {
            try {
                javaScriptHooks.afterDelete(co, context);
            } catch (CordraException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalErrorCordraException(e);
            }
        }
    }

    public void deleteJsonPointer(String objectId, String jsonPointer, AuthenticationResult authResult) throws CordraException, InvalidException, ReadOnlyCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        //        if (jsonPointer == null || jsonPointer.isEmpty()) {
        //            delete(objectId);
        //            return;
        //        }
        objectLocker.lock(objectId);
        try {
            CordraObject co = getCordraObject(objectId);
            JsonNode existingJsonNode = JacksonUtil.gsonToJackson(co.content);
            JacksonUtil.deletePointer(existingJsonNode, jsonPointer);
            String modifiedJson = existingJsonNode.toString();
            writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(objectId, null, modifiedJson, null, null, null, authResult, null, false);
        } finally {
            objectLocker.release(objectId);
        }
    }

    public CordraObject modifyObjectAtJsonPointer(String objectId, String jsonPointer, String replacementJsonData, AuthenticationResult authResult, boolean isDryRun) throws CordraException, InvalidException, ReadOnlyCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        objectLocker.lock(objectId);
        try {
            CordraObject co = getCordraObject(objectId);
            JsonNode existingJsonNode = JacksonUtil.gsonToJackson(co.content);
            JsonNode replacementJsonNode = JacksonUtil.parseJson(replacementJsonData);
            JacksonUtil.replaceJsonAtPointer(existingJsonNode, jsonPointer, replacementJsonNode);
            String modifiedJson = existingJsonNode.toString();
            co = writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(objectId, null, modifiedJson, null, null, null, authResult, Collections.emptyList(), isDryRun);
            co = copyOfCordraObjectRemovingInternalMetadata(co);
            return co;
        } finally {
            objectLocker.release(objectId);
        }
    }

    public void deletePayload(String objectId, String payloadName, AuthenticationResult authResult) throws CordraException, InvalidException, ReadOnlyCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        objectLocker.lock(objectId);
        try {
            CordraObject co = getCordraObject(objectId);
            String type = co.type;
            if (type == null) {
                throw new NotFoundCordraException("Missing object: " + objectId);
            }
            String existingJsonData = co.getContentAsString();
            List<String> payloadsToDelete = Collections.singletonList(payloadName);
            writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(objectId, null, existingJsonData, null, null, null, authResult, payloadsToDelete, false);
        } finally {
            objectLocker.release(objectId);
        }
    }

    public CordraObject writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(String objectId, String type, String jsonData, CordraObject.AccessControlList acl, JsonObject userMetadata, List<Payload> newPayloads, AuthenticationResult authResult, Collection<String> payloadsToDelete, boolean isDryRun) throws CordraException, InvalidException, ReadOnlyCordraException {
        return writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(objectId, type, jsonData, acl, userMetadata, newPayloads, authResult, payloadsToDelete, isDryRun, false, Collections.emptyMap());
    }

    public CordraObject writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdateWithoutAuthorizationOrBeforeSchemaValidation(String objectId, String type, String jsonData, CordraObject.AccessControlList acl, JsonObject userMetadata, List<Payload> newPayloads, AuthenticationResult authResult, Collection<String> payloadsToDelete, boolean isDryRun, Map<String, Object> extraContext) throws CordraException, InvalidException, ReadOnlyCordraException {
        return writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(objectId, type, jsonData, acl, userMetadata, newPayloads, authResult, payloadsToDelete, isDryRun, true, extraContext);
    }

    private boolean isAttemptToModifyImmutableVersion(CordraObject co) {
        if (co.metadata.isVersion != null && co.metadata.isVersion && design.enableVersionEdits != null && design.enableVersionEdits) {
            return true;
        } else {
            return false;
        }
    }

    private CordraObject writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(String objectId, String type, String jsonData, CordraObject.AccessControlList acl, JsonObject userMetadata, List<Payload> newPayloads, AuthenticationResult authResult, Collection<String> payloadsToDelete, boolean isDryRun, boolean bypassAuthorizationAndBeforeSchemaValidation, Map<String, Object> extraContext) throws CordraException, InvalidException, ReadOnlyCordraException {
        if (isReadOnly && !isDryRun) throw new ReadOnlyCordraException();
        objectLocker.lock(objectId);
        CordraObject co;
        JsonElement originalObjectJson;
        JsonElement bsvResultJson;
        Map<String, Object> context = new HashMap<>();
        try {
            co = getCordraObject(objectId);
            if (!bypassAuthorizationAndBeforeSchemaValidation) {
                authorizer.authorizeWrite(authResult, co);
            }
            if (isAttemptToModifyImmutableVersion(co)) throw new BadRequestCordraException("Objects versions cannot be edited");
            if (type == null) {
                type = co.type;
            }
            if (type == null) {
                throw new NotFoundCordraException("Missing object: " + objectId);
            }
            if (jsonData == null) {
                jsonData = co.getContentAsString();
            }
            originalObjectJson = gson.toJsonTree(co);
            if (extraContext != null) context.putAll(extraContext);
            context.put("objectId", objectId);
            context.put("userId", authResult.userId);
            context.put("groups", authResult.groupIds);
            context.put("effectiveAcl", getAclEnforcer().getEffectiveAcl(co));
            context.put("newPayloads", newPayloads);
            context.put("payloadsToDelete", payloadsToDelete);
            context.put("isNew", false);
            context.put("isDryRun", isDryRun);
            ObjectDelta objectDelta = new ObjectDelta(objectId, type, jsonData, acl, userMetadata, newPayloads, payloadsToDelete);
            if (!bypassAuthorizationAndBeforeSchemaValidation) {
                try {
                    objectDelta = javaScriptHooks.beforeSchemaValidation(type, co, objectDelta, context);
                    type = objectDelta.type;
                    objectDelta = javaScriptHooks.beforeSchemaValidationWithId(type, co, objectDelta, context);
                    // can't change digital object id (beforeSchemaValidation will throw)
                    type = objectDelta.type;
                    jsonData = objectDelta.jsonData;
                    acl = objectDelta.acl;
                    userMetadata = objectDelta.userMetadata;
                    newPayloads = objectDelta.payloads;
                    payloadsToDelete = objectDelta.payloadsToDelete;
                } catch (CordraException | InvalidException e) {
                    throw e;
                } catch (Exception e) {
                    throw new InternalErrorCordraException(e);
                }
            }
            bsvResultJson = gson.toJsonTree(objectDelta.asCordraObjectForUpdate(co));
            JsonNode jsonNode = JacksonUtil.parseJson(jsonData);
            LazySchema<JsonSchema> schema = schemas.get(type);
            if (schema == null) {
                throw new InvalidException("Unknown type " + type);
            }
            Map<String, JsonNode> pointerToSchemaMap = validator.schemaValidateAndReturnKeywordsMap(jsonNode, schema.getSchemaNode(), schema.getSchema());
            validator.postSchemaValidate(jsonNode, pointerToSchemaMap);
            validator.validatePayloads(newPayloads);
            boolean isSchema = "Schema".equals(type);
            boolean isUser = UserProcessor.isUser(pointerToSchemaMap);
            boolean isDesign = DESIGN_OBJECT_ID.equals(objectId);
            if (isSchema) schemaNameLocker.acquire();
            if (isUser) usernameLocker.acquire();
            if (isDesign) designLocker.writeLock().acquire();
            ProcessObjectResult processObjectResult;
            try {
                processObjectResult = processObjectBasedOnJsonAndType(co, type, jsonNode, schema.getSchemaNode(), pointerToSchemaMap, authResult.userId, isDryRun);
                if (processObjectResult.changedJson) {
                    jsonData = JacksonUtil.printJson(jsonNode);
                    objectDelta.jsonData = jsonData;
                }
                context.put("originalObject", originalObjectJson);
                context.put("beforeSchemaValidationResult", bsvResultJson);
                javaScriptHooks.beforeStorage(objectDelta.asCordraObjectForUpdate(co), context);
                updateCordraObject(co, false, type, jsonData, acl, userMetadata, payloadsToDelete, newPayloads, authResult.userId, pointerToSchemaMap, isDryRun, false);
                if (isDesign && !isDryRun) {
                    signalWatcher.sendSignal(SignalWatcher.Signal.DESIGN);
                    loadStatefulData();
                }
            } catch (CordraException | InvalidException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalErrorCordraException(e);
            } finally {
                if (isUser) usernameLocker.release();
                if (isSchema) schemaNameLocker.release();
                if (isDesign) designLocker.writeLock().release();
            }
            if (!isDryRun) {
                if (processObjectResult.isUserOrGroup) {
                    authObjectChangeCount.incrementAndGet();
                    authCache.clearAllGroupsForUserValues();
                    authCache.clearAllUserIdForUsernameValues();
                    signalWatcher.sendSignal(SignalWatcher.Signal.AUTH_CHANGE);
                    if (isUser && !isUserAccountActive(co)) {
                        invalidateSessionsForUser(co.id);
                    }
                    preCache();
                }
                if (validator.hasJavaScriptModules(pointerToSchemaMap)) {
                    signalWatcher.sendSignal(SignalWatcher.Signal.JAVASCRIPT_CLEAR_CACHE);
                    cordraRequireLookup.clearAllObjectIdsForModuleValues();
                    javaScriptEnvironment.clearCache();
                    backgroundSchemaWarmUpExec.submit(() -> warmUpJavaScript(jsonSchemaFactory.getGeneration(), javaScriptWarmUpGeneration.incrementAndGet()));
                }
                if ("Schema".equals(type)) {
                    addToKnownSchemas(co.id);
                }
                if (handleClient != null && !isDesign) {
                    try {
                        Map<String, Object> updateHandleContext = new HashMap<>();
                        updateHandleContext.put("objectId", objectId);
                        updateHandleContext.put("effectiveAcl", getAclEnforcer().getEffectiveAcl(co));
                        handleClient.updateHandleFor(co.id, co, type, jsonNode, updateHandleContext);
                    } catch (Exception e) {
                        alerter.alert("Failure to update handle after updating object " + co.id + ", out of sync");
                        logger.error("Failure to update handle after updating object " + co.id + ", out of sync", e);
                        throw new InternalErrorCordraException(e);
                    }
                }
            }
        } finally {
            objectLocker.release(objectId);
        }
        try {
            javaScriptHooks.afterCreateOrUpdate(co, context);
        } catch (CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
        return co;
    }

    public CordraObject writeJsonAndPayloadsIntoCordraObjectIfValid(String type, String jsonData, CordraObject.AccessControlList acl, JsonObject userMetadata, List<Payload> payloads, String handle, AuthenticationResult authResult, boolean isDryRun) throws CordraException, InvalidException, ReadOnlyCordraException {
        authorizer.authorizeCreate(authResult, type);
        if (isReadOnly && !isDryRun) throw new ReadOnlyCordraException();
        ObjectDelta objectDelta = new ObjectDelta(handle, type, jsonData, acl, userMetadata, payloads, null);
        Map<String, Object> context = new HashMap<>();
        try {
            context.put("objectId", handle);
            context.put("userId", authResult.userId);
            context.put("groups", authResult.groupIds);
            context.put("aclCreate", Collections.unmodifiableList(getAclEnforcer().getAclForObjectType(type).aclCreate));
            context.put("isNew", true);
            context.put("isDryRun", isDryRun);
            objectDelta = javaScriptHooks.beforeSchemaValidation(type, null, objectDelta, context);
            handle = objectDelta.id;
            // Even if beforeSchemaValidation changes the type, we do not re-run authorizeCreate
            type = objectDelta.type;
        } catch (CordraException | InvalidException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
        CordraObject proto = null;
        CordraObject co;
        try {
            try {
                if (javaScriptHooks.typeHasJavaScriptFunction(type, JavaScriptLifeCycleHooks.BEFORE_SCHEMA_VALIDATION_WITH_ID, 400)) {
                    proto = createNewCordraObjectEnsuringHandleLock(null, objectDelta, null, handle, authResult, isDryRun);
                    objectDelta.id = proto.id;
                    objectDelta = javaScriptHooks.beforeSchemaValidationWithId(type, null, objectDelta, context);
                }
            } catch (CordraException | InvalidException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalErrorCordraException(e);
            }
            handle = objectDelta.id;
            type = objectDelta.type;
            jsonData = objectDelta.jsonData;
            acl = objectDelta.acl;
            userMetadata = objectDelta.userMetadata;
            payloads = objectDelta.payloads;
            JsonElement bsvResultJson = gson.toJsonTree(objectDelta.asCordraObjectForCreate());
            if (DESIGN_OBJECT_ID.equals(handle)) {
                throw new InternalErrorCordraException("Object not valid for creation: " + handle);
            }
            LazySchema<JsonSchema> schema = schemas.get(type);
            if (schema == null) {
                throw new InvalidException("Unknown type " + type);
            }
            JsonNode jsonNode = JacksonUtil.parseJson(jsonData);
            Map<String, JsonNode> pointerToSchemaMap = validator.schemaValidateAndReturnKeywordsMap(jsonNode, schema.getSchemaNode(), schema.getSchema());
            validator.postSchemaValidate(jsonNode, pointerToSchemaMap);
            validator.validatePayloads(payloads);
            preprocessObjectBasedOnJsonAndType(null, type, jsonNode, pointerToSchemaMap, isDryRun);
            if (proto == null) {
                proto = createNewCordraObjectEnsuringHandleLock(jsonNode, objectDelta, pointerToSchemaMap, handle, authResult, isDryRun);
            }
            boolean isSchema = "Schema".equals(type);
            boolean isUser = UserProcessor.isUser(pointerToSchemaMap);
            if (isSchema) schemaNameLocker.acquire();
            if (isUser) usernameLocker.acquire();
            ProcessObjectResult processObjectResult;
            try {
                processObjectResult = processObjectBasedOnJsonAndType(proto, type, jsonNode, schema.getSchemaNode(), pointerToSchemaMap, authResult.userId, isDryRun);
                objectDelta.id = proto.id;
                if (processObjectResult.changedJson) {
                    jsonData = JacksonUtil.printJson(jsonNode);
                    objectDelta.jsonData = jsonData;
                }
                context.put("beforeSchemaValidationResult", bsvResultJson);
                javaScriptHooks.beforeStorage(objectDelta.asCordraObjectForCreate(), context);
                co = updateCordraObject(proto, true, type, jsonData, acl, userMetadata, Collections.<String>emptyList(), payloads, authResult.userId, pointerToSchemaMap, isDryRun, false);
            } catch (CordraException | InvalidException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalErrorCordraException(e);
            } finally {
                if (isUser) usernameLocker.release();
                if (isSchema) schemaNameLocker.release();
            }
            if (!isDryRun) {
                if (processObjectResult.isUserOrGroup) {
                    authObjectChangeCount.incrementAndGet();
                    authCache.clearAllGroupsForUserValues();
                    authCache.clearAllUserIdForUsernameValues();
                    signalWatcher.sendSignal(SignalWatcher.Signal.AUTH_CHANGE);
                    if (isUser && !isUserAccountActive(co)) {
                        invalidateSessionsForUser(co.id);
                    }
                    preCache();
                }
                if (validator.hasJavaScriptModules(pointerToSchemaMap)) {
                    signalWatcher.sendSignal(SignalWatcher.Signal.JAVASCRIPT_CLEAR_CACHE);
                    cordraRequireLookup.clearAllObjectIdsForModuleValues();
                    javaScriptEnvironment.clearCache();
                    backgroundSchemaWarmUpExec.submit(() -> warmUpJavaScript(jsonSchemaFactory.getGeneration(), javaScriptWarmUpGeneration.incrementAndGet()));
                }
                if ("Schema".equals(type)) {
                    addToKnownSchemas(co.id);
                }
                if (handleClient != null) {
                    try {
                        Map<String, Object> registerHandleContext = new HashMap<>();
                        registerHandleContext.put("objectId", co.id);
                        registerHandleContext.put("effectiveAcl", getAclEnforcer().getEffectiveAcl(co));
                        handleClient.registerHandle(co.id, co, type, jsonNode, registerHandleContext);
                    } catch (Exception e) {
                        if (e instanceof CordraException) throw (CordraException) e;
                        else throw new InternalErrorCordraException(e);
                    }
                }
            }
        } finally {
            if (proto != null) objectLocker.release(proto.id);
        }
        try {
            javaScriptHooks.afterCreateOrUpdate(co, context);
        } catch (CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
        return co;
    }

    public boolean callAllowsGet(String objectId, String method, boolean isStatic) throws CordraException {
        if (doipServiceId.equals(objectId) || CordraClientDoipProcessor.SERVICE_ALIAS.equals(objectId)) {
            objectId = CordraService.DESIGN_OBJECT_TYPE;
            isStatic = true;
        }
        String type;
        if (isStatic) {
            type = objectId;
        } else {
            CordraObject co = getCordraObject(objectId);
            isStatic = "Schema".equals(co.type);
            if (isStatic) {
                type = getTypeNameForSchemaObject(co);
            } else {
                type = co.type;
            }
        }
        String moduleId = CordraRequireLookup.moduleIdForSchemaTypeForMethods(type);
        if (!cordraRequireLookup.exists(moduleId)) {
            throw new NotFoundCordraException("Schema does not have javascript");
        }
        return javaScriptHooks.callAllowsGet(method, moduleId, isStatic);
    }

    private String getTypeNameForSchemaObject(CordraObject co) {
        return co.content.getAsJsonObject().get("name").getAsString();
    }

    public String call(String objectId, String type, AuthenticationResult authResult, String method, DirectIo directIo, boolean isGet) throws CordraException, InterruptedException, ScriptException, InvalidException, ReadOnlyCordraException {
        long start = System.currentTimeMillis();
        if (doipServiceId.equals(objectId) && getCordraObjectOrNull(objectId) == null) {
            objectId = CordraService.DESIGN_OBJECT_ID;
        }
        // Currently design methods can only be static
        if (DESIGN_OBJECT_ID.equals(objectId)) type = DESIGN_OBJECT_TYPE;
        boolean isStatic = type != null;
        if (!isStatic) objectLocker.lock(objectId);
        try {
            String coJson;
            CordraObject co = null;
            if (isStatic) {
                String schemaId = idFromTypeNoSearch(type);
                if (schemaId == null) {
                    throw new NotFoundCordraException("Type not found: " + type);
                }
                coJson = null;
                authorizer.authorizeCall(authResult, method, null, type);
            } else {
                co = getCordraObjectWithNoInternalMetadata(objectId);
                type = co.type;
                coJson = gson.toJson(co);
                authorizer.authorizeCall(authResult, method, co, null);
            }
            String moduleId = CordraRequireLookup.moduleIdForSchemaTypeForMethods(type);
            if (!cordraRequireLookup.exists(moduleId)) {
                throw new NotFoundCordraException("Schema does not have javascript");
            }
            Map<String, Object> context = new HashMap<>();
            if (!isStatic) {
                context.put("objectId", objectId);
                context.put("isNew", false);
                context.put("effectiveAcl", getAclEnforcer().getEffectiveAcl(co));
            }
            context.put("userId", authResult.userId);
            context.put("groups", authResult.groupIds);
            context.put("grantAuthenticatedAccess", authResult.grantAuthenticatedAccess);
            JavaScriptLifeCycleHooks.CallResult callResult = this.javaScriptHooks.call(method, moduleId, isStatic, coJson, context, directIo, isGet);
            String result = callResult.result;
            String before = callResult.before;
            String after = callResult.after;
            if (!isStatic && before != null) {
                // if changed the digital object, perform an update
                if (!before.equals(after)) {
                    ObjectDelta delta = ObjectDelta.fromStringifiedCordraObjectForUpdate(co, after, null);
                    writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdateWithoutAuthorizationOrBeforeSchemaValidation(objectId, delta.type, delta.jsonData, delta.acl, delta.userMetadata, delta.payloads, authResult, delta.payloadsToDelete, false, Collections.singletonMap("method", method));
                }
            }
            return result;
        } finally {
            if (cordraConfig.traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(method + " call: start " + startTime + ", " + delta + "ms");
            }
            if (!isStatic) objectLocker.release(objectId);
        }
    }

    public List<String> listMethods(String type, String objectId, boolean isStatic) throws InterruptedException, ScriptException, CordraException {
        if (type == null) {
            if (objectId == null) {
                throw new BadRequestCordraException("Request must include 'type' or 'objectId'");
            }
            CordraObject co = getCordraObject(objectId);
            type = co.type;
            isStatic = false;
        }
        List<String> result = listMethods(type, isStatic);
        return result;
    }

    public List<String> listMethods(String type, boolean isStatic) throws InterruptedException, ScriptException, CordraException {
        if (getSchema(type) == null) {
            throw new NotFoundCordraException("Type not found " + type);
        }
        String moduleId = CordraRequireLookup.moduleIdForSchemaTypeForMethods(type);
        if (!cordraRequireLookup.exists(moduleId)) {
            //schema does not have javascript
            return Collections.emptyList();
        }
        // Currently design methods can only be static
        if (DESIGN_OBJECT_TYPE.equals(type)) isStatic = true;
        return javaScriptHooks.listMethods(isStatic, moduleId);
    }

    public List<String> listMethodsForUser(AuthenticationResult authResult, String type, String objectId, boolean isStatic, boolean includeCrud) throws InterruptedException, ScriptException, CordraException {
        CordraObject co = null;
        if (type == null) {
            if (objectId == null) {
                throw new BadRequestCordraException("Request must include 'type' or 'objectId'");
            }
            co = storage.get(objectId);
            if (co == null) throw new NotFoundCordraException("Missing object: " + objectId);
            type = co.type;
        }
        return aclEnforcer.listMethodsForUser(authResult, type, co, isStatic, includeCrud);
    }

    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX").withZone(ZoneOffset.UTC);

    public PublicKey getAdminPublicKey() {
        return design.adminPublicKey;
    }

    public void updatePasswordForUser(String newPassword, AuthenticationResult authResult) throws Exception {
        if ("admin".equals(authResult.userId)) {
            setAdminPassword(newPassword);
            return;
        }
        objectLocker.lock(authResult.userId);
        try {
            CordraObject user = getCordraObject(authResult.userId);
            authorizer.authorizeWrite(authResult, user);
            if (checkUserPassword(user, newPassword)) {
                throw new BadRequestCordraException("New password can not be the same as previous password.");
            }
            JsonNode jsonNode = JacksonUtil.gsonToJackson(user.content);
            String type = user.type;
            Map<String, JsonNode> pointerToSchemaMap = getPointerToSchemaMap(type, jsonNode);
            updatePasswordForUser(newPassword, authResult, jsonNode, pointerToSchemaMap);
        } finally {
            objectLocker.release(authResult.userId);
        }
    }

    public boolean checkUserPassword(CordraObject user, String password) {
        if (user == null) return false;
        JsonElement hash = user.metadata.internalMetadata.get("hash");
        JsonElement salt = user.metadata.internalMetadata.get("salt");
        if (hash == null || salt == null) {
            return false;
        }
        String hashString = hash.getAsString();
        String saltString = salt.getAsString();
        JsonElement iterationsElement = user.metadata.internalMetadata.get("iterations");
        int iterations;
        if (iterationsElement != null) {
            iterations = iterationsElement.getAsInt();
        } else {
            iterations = HashAndSalt.LEGACY_HASH_ITERATION_COUNT_2048;
        }
        JsonElement algorithm = user.metadata.internalMetadata.get("algorithm");
        String algString = algorithm != null ? algorithm.getAsString() : null;
        HashAndSalt hashAndSalt = new HashAndSalt(hashString, saltString, iterations, algString);
        return hashAndSalt.verifySecret(password);
    }

    private void updatePasswordForUser(String newPassword, AuthenticationResult authResult, JsonNode jsonNode, Map<String, JsonNode> pointerToSchemaMap) throws CordraException, ReadOnlyCordraException, InvalidException {
        PasswordProcessor passwordProcessor = new PasswordProcessor();
        passwordProcessor.setPasswordIntoJson(newPassword, jsonNode, pointerToSchemaMap);
        passwordProcessor.setRequirePasswordChangeFlag(false, jsonNode, pointerToSchemaMap);
        String jsonData = jsonNode.toString();
        CordraObject.AccessControlList acl = null;
        List<String> payloadsToDelete = new ArrayList<>();
        List<Payload> payloads = null;
        JsonObject userMetadata = null;
        writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(authResult.userId, null, jsonData, acl, userMetadata, payloads, authResult, payloadsToDelete, false);
    }

    public boolean isRequirePasswordChange(JsonNode jsonNode, Map<String, JsonNode> pointerToSchemaMap) {
        PasswordProcessor passwordProcessor = new PasswordProcessor();
        return passwordProcessor.getRequirePasswordChangeFlag(jsonNode, pointerToSchemaMap);
    }

    public boolean isUserAccountActive(String userId) throws CordraException {
        CordraObject user = getCordraObjectOrNull(userId);
        return isUserAccountActive(user);
    }

    public boolean isUserAccountActive(CordraObject user) {
        if (user == null) return true;
        JsonNode jsonNode = JacksonUtil.gsonToJackson(user.content);
        String type = user.type;
        try {
            Map<String, JsonNode> pointerToSchemaMap = getPointerToSchemaMap(type, jsonNode);
            UserProcessor userProcessor = new UserProcessor(this);
            return userProcessor.isUserAccountActive(jsonNode, pointerToSchemaMap);
        } catch (InvalidException e) {
            // this can happen for example if the User schema has been deleted
            return false;
        }
    }

    private void preprocessObjectBasedOnJsonAndType(String handle, String type, JsonNode jsonNode, Map<String, JsonNode> pointerToSchemaMap, boolean isDryRun) throws CordraException, ReadOnlyCordraException {
        if (isReadOnly && !isDryRun) throw new ReadOnlyCordraException();
        SchemaNameProcessor schemaNameProcessor = new SchemaNameProcessor(this);
        schemaNameProcessor.preprocess(type, handle, jsonNode);
        UserProcessor userProcessor = new UserProcessor(this);
        userProcessor.preprocess(handle, jsonNode, pointerToSchemaMap);
        PasswordProcessor passwordProcessor = new PasswordProcessor();
        passwordProcessor.preprocess(jsonNode, pointerToSchemaMap);
    }

    public boolean verifySecureProperty(CordraObject cordraObject, String jsonPointer, String secretToVerify) {
        if (cordraObject == null) return false;
        SecurePropertiesProcessor securePropertiesProcessor = new SecurePropertiesProcessor();
        return securePropertiesProcessor.verifySecureProperty(cordraObject, jsonPointer, secretToVerify);
    }

    public boolean isPasswordChangeRequired(CordraObject user) throws CordraException {
        if (user == null) return false;
        JsonNode jsonNode = JacksonUtil.gsonToJackson(user.content);
        String type = user.type;
        try {
            Map<String, JsonNode> pointerToSchemaMap = getPointerToSchemaMap(type, jsonNode);
            return isRequirePasswordChange(jsonNode, pointerToSchemaMap);
        } catch (InvalidException e) {
            // This would mean an existing user object failed to validate against its schema
            throw new InternalErrorCordraException(e);
        }
    }

    static class ProcessObjectResult {
        boolean isUserOrGroup;
        boolean changedJson;
    }

    private ProcessObjectResult processObjectBasedOnJsonAndType(CordraObject co, String type, JsonNode jsonNode, JsonNode schemaNode, Map<String, JsonNode> pointerToSchemaMap, String creatorId, boolean isDryRun) throws CordraException, ReadOnlyCordraException {
        if (isReadOnly && !isDryRun) throw new ReadOnlyCordraException();
        SchemaNameProcessor schemaNameProcessor = new SchemaNameProcessor(this);
        schemaNameProcessor.process(type, co, jsonNode);
        UserProcessor userProcessor = new UserProcessor(this);
        boolean usernameChange = userProcessor.process(co, jsonNode, pointerToSchemaMap);
        PasswordProcessor passwordProcessor = new PasswordProcessor();
        boolean changedJsonForPassword = passwordProcessor.process(co, jsonNode, pointerToSchemaMap);
        UsersListProcessor usersListProcessor = new UsersListProcessor();
        boolean usersListChange = usersListProcessor.process(co, jsonNode, pointerToSchemaMap, design.handleMintingConfig.prefix);
        SecurePropertiesProcessor securePropertiesProcessor = new SecurePropertiesProcessor();
        boolean changedJsonForSecureProperties = securePropertiesProcessor.process(co, jsonNode, pointerToSchemaMap);
        ProcessObjectResult result = new ProcessObjectResult();
        result.isUserOrGroup = usernameChange || usersListChange;
        JsonAugmenter jsonAugmenter = new JsonAugmenter();
        boolean changedJsonForJsonAugmenter = jsonAugmenter.augment(co, jsonNode, pointerToSchemaMap, creatorId, design.handleMintingConfig.prefix);
        boolean changedForOrder = SchemaUtil.changePropertyOrder(jsonNode, schemaNode);
        result.changedJson = changedJsonForPassword || changedJsonForJsonAugmenter || changedForOrder || changedJsonForSecureProperties;
        return result;
    }

    // Note: changes to this method should mirror changes made to rebuildSchemasFromListOfObjects
    private void addToKnownSchemas(String handle) throws CordraException, ReadOnlyCordraException {
        designLocker.writeLock().acquire();
        try {
            CordraObject designObject = this.getDesignCordraObject();
            Map<String, String> schemaIds = getSchemaIdsFromDesignObject(designObject);
            if (!schemaIds.equals(design.schemaIds)) {
                List<CordraObject> knownSchemaObjects = objectListFromHandleList(schemaIds.keySet());
                rebuildSchemasFromListOfObjects(knownSchemaObjects);
            }
            String oldType = schemaIds.get(handle);
            CordraObject schemaObject = getCordraObject(handle);
            JsonNode node = JacksonUtil.gsonToJackson(schemaObject.content);
            JsonNode schemaNode = JacksonUtil.getJsonAtPointer("/schema", node);
            String schemaString = JacksonUtil.printJson(schemaNode);
            String type = JacksonUtil.getJsonAtPointer("/name", node).asText();
            String baseUri = net.cnri.cordra.schema.SchemaUtil.getBaseUriFromSchemaCordraObject(schemaObject);
            String js = JacksonUtil.getJsonAtPointer("/javascript", node).asText();
            if (js != null && js.isEmpty()) js = null;
            if (!designLocker.writeLock().isLocked()) {
                addToKnownSchemas(handle);
                return;
            }
            long start = System.currentTimeMillis();
            String oldBaseUri = design.nameToBaseUriMap.get(type);
            design.nameToBaseUriMap.put(type, baseUri);
            if (oldBaseUri != null && design.baseUriToNameMap.containsKey(oldBaseUri)) {
                design.baseUriToNameMap.remove(oldBaseUri);
            }
            design.baseUriToNameMap.put(baseUri, type);
            if (oldType != null && !type.equals(oldType)) {
                schemas.remove(oldType);
                schemaCordraObjects.remove(oldType);
                cordraRequireLookup.removeSchemaJavaScript(oldType);
                cordraRequireLookup.removeSchema(oldType);
            }
            schemas.put(type, new LazySchema<>(schemaNode, baseUri, jsonSchemaFactory));
            long gen = jsonSchemaFactory.incrementAndGetGeneration();
            backgroundSchemaWarmUpExec.submit(() -> parseAllSchemas(gen));
            schemaCordraObjects.put(type, schemaObject);
            design.schemaIds.put(handle, type);
            design.schemas = getSchemaNodes();
            if (!isReadOnly && designObject != null) {
                persistDesignToCordraObject(designObject);
            }
            if (!designLocker.writeLock().isLocked()) {
                long duration = System.currentTimeMillis() - start;
                alerter.alert("Design lock failure adding " + handle + ", critical code took " + duration + "ms");
            }
            boolean needClear = false;
            String oldJs = cordraRequireLookup.getSchemaJavaScript(type);
            if ((js == null && oldJs != null) || (js != null && !js.equals(oldJs))) {
                cordraRequireLookup.putSchemaJavaScript(type, js);
                needClear = true;
            }
            String oldSchema = cordraRequireLookup.getSchema(type);
            if (!schemaString.equals(oldSchema)) {
                cordraRequireLookup.putSchema(type, schemaString);
                needClear = true;
            }
            if (needClear) {
                javaScriptEnvironment.clearCache();
                backgroundSchemaWarmUpExec.submit(() -> warmUpJavaScript(gen, javaScriptWarmUpGeneration.incrementAndGet()));
            }
            signalWatcher.sendSignal(SignalWatcher.Signal.DESIGN);
        } finally {
            designLocker.writeLock().release();
        }
    }

    private void deleteFromKnownSchemas(String handle) throws CordraException, ReadOnlyCordraException {
        designLocker.writeLock().acquire();
        try {
            CordraObject designObject = this.getDesignCordraObject();
            Map<String, String> schemaIds = getSchemaIdsFromDesignObject(designObject);
            if (!schemaIds.equals(design.schemaIds)) {
                List<CordraObject> knownSchemaObjects = objectListFromHandleList(schemaIds.keySet());
                rebuildSchemasFromListOfObjects(knownSchemaObjects);
            }
            String oldType = schemaIds.get(handle);
            if (oldType == null) {
                alerter.alert("Deleting schema " + handle + " not in design");
                return;
            }
            if (!designLocker.writeLock().isLocked()) {
                deleteFromKnownSchemas(handle);
                return;
            }
            long start = System.currentTimeMillis();
            schemas.remove(oldType);
            long gen = jsonSchemaFactory.incrementAndGetGeneration();
            backgroundSchemaWarmUpExec.submit(() -> parseAllSchemas(gen));
            schemaCordraObjects.remove(oldType);
            cordraRequireLookup.removeSchemaJavaScript(oldType);
            cordraRequireLookup.removeSchema(oldType);
            design.schemaIds.remove(handle);
            design.schemas = getSchemaNodes();
            if (!isReadOnly && designObject != null) {
                persistDesignToCordraObject(designObject);
            }
            if (!designLocker.writeLock().isLocked()) {
                long duration = System.currentTimeMillis() - start;
                alerter.alert("Design lock failure deleting " + handle + ", critical code took " + duration + "ms");
            }
            signalWatcher.sendSignal(SignalWatcher.Signal.DESIGN);
        } finally {
            designLocker.writeLock().release();
        }
    }

    private Map<String, JsonNode> getSchemaNodes() {
        Map<String, JsonNode> result = new HashMap<>();
        for (Map.Entry<String, LazySchema<JsonSchema>> entry : schemas.entrySet()) {
            String type = entry.getKey();
            result.put(type, entry.getValue().getSchemaNode());
        }
        return result;
    }

    public String getHandleForSuffix(String suffix) {
        return handleMinter.mintWithSuffix(suffix);
    }

    CordraObject createNewCordraObjectForPublishVersionEnsuringHandleLock(String existingObjectId, String versionId) throws CordraException, ReadOnlyCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        CordraObject result = null;
        if (versionId != null && !versionId.isEmpty()) {
            result = lockAndCreateMemoryObjectIfHandleAvailable(versionId, true, existingObjectId);
        }
        if (result == null) {
            while (result == null) {
                versionId = handleMinter.mintByTimestamp();
                result = lockAndCreateMemoryObjectIfHandleAvailable(versionId, false, existingObjectId);
            }
        }
        return result;
    }

    CordraObject createNewCordraObjectEnsuringHandleLock(JsonNode jsonNode, ObjectDelta objectDelta, Map<String, JsonNode> pointerToSchemaMap, String handle, AuthenticationResult authResult, boolean isDryRun) throws CordraException, ReadOnlyCordraException, InvalidException {
        if (isReadOnly && !isDryRun) throw new ReadOnlyCordraException();
        CordraObject result = null;
        if (handle != null && !handle.isEmpty()) {
            result = lockAndCreateMemoryObjectIfHandleAvailable(handle, true);
        } else {
            result = createNewCordraObjectEnsuringHandleLockUsingGenerateIdJavaScript(objectDelta, authResult);
        }
        if (result == null) {
            if (pointerToSchemaMap != null) {
                String primaryData = getPrimaryData(jsonNode, pointerToSchemaMap);
                if (primaryData != null) {
                    handle = handleMinter.mint(primaryData);
                    result = lockAndCreateMemoryObjectIfHandleAvailable(handle, false);
                }
            }
            while (result == null) {
                handle = handleMinter.mintByTimestamp();
                result = lockAndCreateMemoryObjectIfHandleAvailable(handle, false);
            }
        }
        try {
            String creatorId = authResult.userId;
            if (creatorId == null) {
                creatorId = "anonymous";
            }
            long now = System.currentTimeMillis();
            result.metadata.createdOn = now;
            result.metadata.modifiedOn = now;
            result.metadata.createdBy = creatorId;
            result.metadata.modifiedBy = creatorId;
            return result;
        } catch (Exception e) {
            objectLocker.release(handle);
            throw e;
        }
    }

    public AuthenticationResult authenticateViaHook(RequestAuthenticationInfo requestAuthInfo) throws CordraException {
        Map<String, Object> context = new HashMap<>();
        try {
            AuthenticationResult authResult = javaScriptHooks.authenticate(requestAuthInfo, context);
            return authResult;
        } catch (CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private CordraObject createNewCordraObjectEnsuringHandleLockUsingGenerateIdJavaScript(ObjectDelta objectDelta, AuthenticationResult authResult) throws CordraException, InvalidException {
        JavaScriptRunner runner = javaScriptHooks.getJavaScriptRunner();
        try {
            JavaScriptLifeCycleHooks.GenerateIdJavaScriptStatus generateIdJavaScriptStatus = javaScriptHooks.hasJavaScriptGenerateIdFunction(runner);
            if (!generateIdJavaScriptStatus.hasFunction) return null;
            CordraObject co = objectDelta.asCordraObjectForCreate();
            Map<String, Object> context = createContext(authResult, co);
            String handle = javaScriptHooks.generateIdFromJavaScript(runner, co, context);
            if (handle == null || handle.isEmpty()) return null;
            boolean throwIfNotAvailable = !generateIdJavaScriptStatus.isLoopable;
            CordraObject result = lockAndCreateMemoryObjectIfHandleAvailable(handle, throwIfNotAvailable);
            if (generateIdJavaScriptStatus.isLoopable) {
                while (result == null) {
                    handle = javaScriptHooks.generateIdFromJavaScript(runner, co, context);
                    if (handle == null || handle.isEmpty()) return null;
                    result = lockAndCreateMemoryObjectIfHandleAvailable(handle, false);
                }
            }
            return result;
        } catch (CordraException | InvalidException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            javaScriptHooks.recycleJavaScriptRunner(runner);
        }
    }

    public static boolean isValidHandle(String handle) {
        if (handle == null || handle.isEmpty()) return false;
        if (handle.startsWith("/") || handle.startsWith(".")) return false;
        if (!handle.contains("/")) return false;
        return true;
    }

    private CordraObject lockAndCreateMemoryObjectIfHandleAvailable(String handle, boolean throwIfNotAvailable, String... otherIdsToLock) throws CordraException {
        if (handle == null || handle.isEmpty()) return null;
        if (!isValidHandle(handle)) {
            throw new BadRequestCordraException("Invalid handle: " + handle);
        }
        List<String> idsToLock = null;
        if (otherIdsToLock != null && otherIdsToLock.length > 0) {
            idsToLock = new ArrayList<>();
            idsToLock.addAll(Arrays.asList(otherIdsToLock));
            idsToLock.add(handle);
            objectLocker.lockInOrder(idsToLock);
        } else {
            objectLocker.lock(handle);
        }
        boolean success = false;
        try {
            if (storage.get(handle) == null) {
                CordraObject res = new CordraObject();
                res.id = handle;
                res.metadata = new CordraObject.Metadata();
                res.metadata.internalMetadata = new JsonObject();
                success = true;
                return res;
            }
        } finally {
            if (!success) {
                if (idsToLock != null) {
                    objectLocker.releaseAll(idsToLock);
                } else {
                    objectLocker.release(handle);
                }
            }
        }
        if (throwIfNotAvailable) {
            throw new ConflictCordraException("Object already exists: " + handle);
        } else {
            return null;
        }
    }

    public CordraObject publishVersion(String existingObjectId, String versionId, boolean clonePayloads, AuthenticationResult authResult) throws CordraException, VersionException, ReadOnlyCordraException, ConflictCordraException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        if (existingObjectId == null || existingObjectId.isEmpty()) {
            throw new BadRequestCordraException("No existing object id provided for publishing version");
        }
        CordraObject protoVersion = createNewCordraObjectForPublishVersionEnsuringHandleLock(existingObjectId, versionId);
        CordraObject co;
        try {
            CordraObject existingObject = storage.get(existingObjectId);
            if (existingObject == null) throw new NotFoundCordraException("Missing object: " + existingObjectId);
            authorizer.authorizeWrite(authResult, existingObject);
            versionManager.validateExistingObject(existingObjectId, existingObject);
            ObjectDelta objectDelta = versionManager.buildObjectDeltaForVersion(versionId, existingObjectId, existingObject, clonePayloads);
            versionManager.setProtoObjectMetadataForVersion(protoVersion, existingObjectId, existingObject, authResult.userId);
            // updateCordraObject will generate pointerToSchemaMap as needed
            co = updateCordraObject(protoVersion, true, objectDelta.type, objectDelta.jsonData, objectDelta.acl, objectDelta.userMetadata, Collections.<String>emptyList(), objectDelta.payloads, existingObject.metadata.modifiedBy, null, false, true);
            if (handleClient != null) {
                try {
                    Map<String, Object> context = new HashMap<>();
                    context.put("objectId", co.id);
                    context.put("effectiveAcl", getAclEnforcer().getEffectiveAcl(co));
                    handleClient.registerHandle(co.id, co, co.type, JacksonUtil.gsonToJackson(co.content), context);
                } catch (Exception e) {
                    if (e instanceof CordraException) throw (CordraException) e;
                    else throw new InternalErrorCordraException(e);
                }
            }
        } finally {
            objectLocker.releaseAll(Arrays.asList(existingObjectId, protoVersion.id));
        }
        return co;
    }

    public List<CordraObject> getVersionsFor(String objectId, AuthenticationResult authResult) throws CordraException, InterruptedException, ScriptException {
        CordraObject co = storage.get(objectId);
        if (co == null) {
            throw new NotFoundCordraException("Missing object: " + objectId);
        }
        authorizer.authorizeRead(authResult, co);
        if (co.metadata == null) co.metadata = new CordraObject.Metadata();
        if (co.metadata.internalMetadata == null) co.metadata.internalMetadata = new JsonObject();
        String tipId = co.metadata.versionOf;
        CordraObject tipCo;
        if (tipId != null) {
            tipCo = storage.get(tipId);
            if (tipCo == null) {
                throw new NotFoundCordraException("Versioned object " + objectId + " is missing tip " + tipId);
            }
            authorizer.authorizeRead(authResult, tipCo);
        } else {
            tipCo = co;
        }
        return versionManager.getVersionsFor(tipCo, authResult);
    }

    private CordraObject updateCordraObject(CordraObject co, boolean isCreate, String objectType, String jsonData, CordraObject.AccessControlList acl, JsonObject userMetadata, Collection<String> payloadsToDelete, List<Payload> payloads, String userId, Map<String, JsonNode> pointerToSchemaMap, boolean isDryRun, boolean isPublishVersion) throws CordraException, ReadOnlyCordraException {
        if (isReadOnly && !isDryRun) throw new ReadOnlyCordraException();
        String handle = co.id;
        CordraTransaction txn = null;
        boolean success = false;
        boolean stored = false;
        boolean possiblyStored = false;
        try {
            if (!isDryRun) {
                txn = makeUpdateTransactionFor(handle);
            }
            co.type = objectType;
            co.setContent(jsonData);
            if (co.metadata == null) co.metadata = new CordraObject.Metadata();
            if (co.metadata.internalMetadata == null) co.metadata.internalMetadata = new JsonObject();
            if (!isDryRun) {
                if (txn == null) throw new AssertionError();
                co.metadata.txnId = txn.txnId;
            }

            if (!isPublishVersion) {
                if (userId == null) {
                    userId = "anonymous";
                }
                co.metadata.modifiedBy = userId;
                if (!isCreate) {
                    co.metadata.modifiedOn = System.currentTimeMillis();
                }
            }
            if (acl != null) {
                co.acl = acl;
            }
            if (userMetadata != null) {
                co.userMetadata = userMetadata;
            }
            addAndDeletePayloads(co, payloadsToDelete, payloads);
            if (shouldHashObject(objectType)) {
                CordraObjectHasher hasher = new CordraObjectHasher();
                hasher.generateAllHashesAndSetThemOnTheCordraObject(co, storage);
            }
            if (!isDryRun) {
                possiblyStored = true;
                co = storeCordraObject(co, isCreate);
                stored = true;
                indexCordraObject(co, objectType, pointerToSchemaMap);
            }
            cleanupPayloads(co);
            if (!isDryRun) {
                if (txn == null) throw new AssertionError();
                sendUpdateReplicationMessage(co);
                success = true;
                transactionManager.closeTransaction(txn.txnId, cordraServiceId);
            }
            return co;
        } catch (Exception e) {
            if (!success && isCreate && !isDryRun) {
                // created object
                try {
                    if (stored || (possiblyStored && storage.get(co.id) != null)) {
                        storage.delete(co.id);
                        logger.warn("Deleted object after error " + handle);
                    }
                } catch (Exception ex) {
                    alerter.alert("Failure to delete new object after creation error: " + handle + ", out of sync");
                    logger.error("Failure to delete new object after creation error: " + handle + ", out of sync", ex);
                }
            }
            if (txn != null) {
                try {
                    transactionReprocessingQueue.insert(txn, cordraServiceId);
                    transactionManager.closeTransaction(txn.txnId, cordraServiceId);
                } catch (Exception ex) {
                    logger.error("Error in updateCordraObject; followed by reprocessing error", e);
                    throw ex;
                }
            }
            throw e;
        }
    }

    private void addAndDeletePayloads(CordraObject co, Collection<String> payloadsToDelete, List<Payload> payloads) {
        if (payloadsToDelete != null) {
            for (String payloadName : payloadsToDelete) {
                co.deletePayload(payloadName);
            }
        }
        if (payloads != null) {
            for (Payload payload : payloads) {
                addPayloadToCordraObject(co, payload.name, payload.filename, payload.mediaType, payload.getInputStream());
            }
        }
    }

    private CordraObject storeCordraObject(CordraObject co, boolean isCreate) throws CordraException, IndexerException {
        if (isCreate) {
            co = storage.create(co);
        } else {
            co = storage.update(co);
        }
        return co;
    }

    private void indexCordraObject(CordraObject co, String objectType, Map<String, JsonNode> pointerToSchemaMap) throws CordraException, IndexerException {
        boolean indexPayloads = shouldIndexPayloads(objectType);
        if (pointerToSchemaMap == null) {
            indexObject(co, indexPayloads);
        } else {
            indexer.indexObject(cordraServiceId, co, indexPayloads, pointerToSchemaMap);
        }
    }

    private void cleanupPayloads(CordraObject co) {
        co.clearPayloadsToDelete();
        if (co.payloads != null) {
            for (Payload payload : co.payloads) {
                if (payload.getInputStream() != null) {
                    try {
                        payload.getInputStream().close();
                    } catch (Exception e) {
                        // ignore
                    }
                    payload.setInputStream(null);
                }
            }
        }
    }

    private void addPayloadToCordraObject(CordraObject co, String name, String filename, String mediaType, InputStream inputStream) {
        // defaulting -- used for InternalCordraClientTest
        if (filename == null) filename = "";
        if (mediaType == null) mediaType = "application/octet-stream";
        if (co.payloads == null) {
            co.payloads = new ArrayList<>();
        } else {
            for (Payload p : co.payloads) {
                if (p.name.equals(name)) {
                    p.filename = filename;
                    p.mediaType = mediaType;
                    p.setInputStream(inputStream);
                    return;
                }
            }
        }
        Payload p = new Payload();
        p.name = name;
        p.filename = filename;
        p.mediaType = mediaType;
        p.setInputStream(inputStream);
        co.payloads.add(p);
    }

    public SearchResults<CordraObject> searchRepo(String query) throws CordraException {
        return indexer.search(query);
    }

    public SearchResults<String> searchRepoHandles(String query) throws CordraException {
        return indexer.searchHandles(query);
    }

    private QueryParams adjustQueryParams(QueryParams params) {
        int pageSize = params.getPageSize();
        if (params.getPageSize() == 0 && Boolean.TRUE.equals(design.useLegacySearchPageSizeZeroReturnsAll)) {
            pageSize = -1;
        }
        return new QueryParams(params.getPageNum(), pageSize, params.getSortFields(), params.getFacets(), params.getFilterQueries());
    }

    SearchResults<String> searchHandles(String query, QueryParams params) throws CordraException {
        params = adjustQueryParams(params);
        String q = "valid:true AND (" + query + ")";
        return indexer.searchHandles(q, params);
    }

    SearchResults<IdType> searchIdType(String query, QueryParams params) throws CordraException {
        params = adjustQueryParams(params);
        String q = "valid:true AND (" + query + ")";
        return indexer.searchIdType(q, params);
    }

    SearchResults<CordraObject> search(String query, QueryParams params) throws CordraException {
        params = adjustQueryParams(params);
        String q = "valid:true AND (" + query + ")";
        return indexer.search(q, params);
    }

    @SuppressWarnings("resource")
    public SearchResults<String> searchHandlesWithQueryCustomizationAndRestriction(String query, QueryParams params,
                boolean isPostProcess, AuthenticationResult authResult, boolean excludeVersions) throws CordraException, ScriptException, InterruptedException {
        SearchRequest queryAndParams = customizeAndRestrictQuery(query, params, true, authResult, excludeVersions);
        SearchResults<IdType> results = searchIdType(queryAndParams.query, queryAndParams.getParams());
        Stream<IdType> stream = results.stream();
        if (isPostProcess) {
            stream = stream.filter(result -> filterIdTypeForSearchHandlesPostProcessing(result, authResult));
        }
        Stream<String> idStream = stream.map(result -> result.id);
        return new SearchResultsFromStream<>(results.size(), results.getFacets(), idStream);
    }

    private boolean filterIdTypeForSearchHandlesPostProcessing(IdType result, AuthenticationResult authResult) {
        try {
            if (result.type == null || javaScriptHooks.typeHasOnObjectResolution(result.type)) {
                CordraObject co = storage.get(result.id);
                co = copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
                co = postProcessForSearch(authResult, co);
                return true;
            } else {
                return true;
            }
        } catch (ScriptException | InterruptedException e) {
            throw new UncheckedCordraException(new InternalErrorCordraException(e));
        } catch (InternalErrorCordraException e) {
            throw new UncheckedCordraException(e);
        } catch (CordraException e) {
            return false;
        }
    }

    public SearchResults<CordraObject> searchWithQueryCustomizationAndRestriction(String query, QueryParams params,
                boolean isPostProcess, AuthenticationResult authResult, boolean excludeVersions) throws CordraException, ScriptException, InterruptedException {
        SearchRequest queryAndParams = customizeAndRestrictQuery(query, params, false, authResult, excludeVersions);
        return search(queryAndParams.query, queryAndParams.getParams(), isPostProcess, authResult);
    }

    public SearchRequest customizeAndRestrictQuery(String query, QueryParams params, boolean ids, AuthenticationResult authResult, boolean excludeVersions) throws InterruptedException, CordraException, ScriptException {
        SearchRequest customizedQueryAndParams =  customizeQueryWithJavaScript(new SearchRequest(query, params, ids), authResult);
        customizedQueryAndParams.query = QueryRestrictor.restrict(customizedQueryAndParams.query, authResult, aclEnforcer, excludeVersions);
        return customizedQueryAndParams;
    }

    public SearchRequest customizeQueryWithJavaScript(SearchRequest queryAndParams, AuthenticationResult authResult) throws InterruptedException, CordraException, ScriptException {
        Map<String, Object> context = new HashMap<>();
        context.put("userId", authResult.userId);
        context.put("groups", authResult.groupIds);
        context.put("grantAuthenticatedAccess", authResult.grantAuthenticatedAccess);
        SearchRequest result = javaScriptHooks.customizeQueryAndParams(queryAndParams, context);
        result.query = javaScriptHooks.customizeQuery(result.query, context);
        return result;
    }

    private CordraObject postProcessForSearchNoCheckedExceptions(CordraObject co, AuthenticationResult authResult) {
        try {
            return postProcessForSearch(authResult, co);
        } catch (InterruptedException | ScriptException e) {
            throw new UncheckedCordraException(new InternalErrorCordraException(e));
        } catch (InternalErrorCordraException e) {
            throw new UncheckedCordraException(e);
        } catch (CordraException e) {
            // onObjectResolution enrichment may have forbidden access.
            // Just skip it; don't reveal anything about the skipped object.
            // Note that this means a searching user may receive fewer results than their requested page size.
            return null;
        }
    }

    @SuppressWarnings("resource")
    public SearchResults<CordraObject> search(String query, QueryParams params, boolean isPostProcess, AuthenticationResult authResult) throws CordraException {
        SearchResults<CordraObject> results = search(query, params);
        Stream<CordraObject> stream = results.stream()
                .map(CordraService::copyOfCordraObjectRemovingInternalMetadata);
        if (isPostProcess) {
            stream = stream
                    .map(co -> postProcessForSearchNoCheckedExceptions(co, authResult))
                    .filter(Objects::nonNull);
        }
        if (params.getFilter() != null) {
            stream = stream.map(co -> {
                JsonElement jsonElement = gson.toJsonTree(co, CordraObject.class);
                jsonElement = JsonUtil.pruneToMatchPointers(jsonElement, params.getFilter());
                return gson.fromJson(jsonElement, CordraObject.class);
            });
        }
        return new SearchResultsFromStream<>(results.size(), results.getFacets(), stream);
    }

    private void indexObject(CordraObject co, boolean indexPayloads) throws CordraException {
        Map<String, JsonNode> pointerToSchemaMap = getPointerToSchemaMapForIndexing(co, reindexer.getIsReindexInProcess());
        indexer.indexObject(cordraServiceId, co, indexPayloads, pointerToSchemaMap);
    }

    public Map<String, JsonNode> getPointerToSchemaMapForIndexing(CordraObject co, boolean isReindexInProcess) {
        Map<String, JsonNode> pointerToSchemaMap;
        String type = co.type;
        if (isReindexInProcess && (type == null || schemas.get(type) == null)) {
            // don't log missing types on reindex
            pointerToSchemaMap = Collections.emptyMap();
        } else {
            try {
                JsonNode jsonNode = JacksonUtil.gsonToJackson(co.content);
                if (DESIGN_OBJECT_ID.equals(co.id)) {
                    pointerToSchemaMap = Collections.emptyMap();
                } else {
                    pointerToSchemaMap = getPointerToSchemaMap(type, jsonNode);
                }
            } catch (InvalidException e) {
                logger.warn("Unexpected exception indexing " + co.id, e);
                pointerToSchemaMap = Collections.emptyMap();
            }
        }
        return pointerToSchemaMap;
    }

    public Map<String, JsonNode> getPointerToSchemaMap(String type, JsonNode jsonNode) throws InvalidException {
        LazySchema<JsonSchema> schema;
        schema = schemas.get(type);
        if (schema == null) {
            throw new InvalidException("Unknown type " + type);
        }
        Map<String, JsonNode> keywordsMap = validator.schemaValidateAndReturnKeywordsMap(jsonNode, schema.getSchemaNode(), schema.getSchema());
        return keywordsMap;
    }

    void validateForTesting(String objectType, String jsonData) throws InvalidException, CordraException {
        JsonNode jsonNode = JacksonUtil.parseJson(jsonData);
        Map<String, JsonNode> keywordsMap = getPointerToSchemaMap(objectType, jsonNode);
        validator.postSchemaValidate(jsonNode, keywordsMap);
    }

    static String getPrimaryData(JsonNode jsonNode, Map<String, JsonNode> pointerToSchemaMap) {
        for (Map.Entry<String, JsonNode> entry : pointerToSchemaMap.entrySet()) {
            String jsonPointer = entry.getKey();
            JsonNode subSchema = entry.getValue();
            JsonNode isPrimaryNode = SchemaUtil.getDeepCordraSchemaProperty(subSchema, "preview", "isPrimary");
            if (isPrimaryNode == null) continue;
            if (isPrimaryNode.asBoolean()) {
                JsonNode referenceNode = jsonNode.at(jsonPointer);
                if (referenceNode == null) {
                    logger.warn("Unexpected missing isPrimary node " + jsonPointer);
                } else {
                    return referenceNode.asText();
                }
            }
        }
        return null;
    }

    public String getMediaType(String type, JsonNode jsonNode, String jsonPointer) {
        Map<String, JsonNode> pointerToSchemaMap;
        try {
            pointerToSchemaMap = getPointerToSchemaMap(type, jsonNode);
        } catch (InvalidException e) {
            return null;
        }
        JsonNode subSchema = pointerToSchemaMap.get(jsonPointer);
        if (subSchema == null) return null;
        JsonNode mediaType = SchemaUtil.getDeepCordraSchemaProperty(subSchema, "response", "mediaType");
        if (mediaType == null) return null;
        return mediaType.asText();
    }

    public void ensureIndexUpToDate() throws CordraException {
        if (closed) return; // allow stopping without error during background JavaScriptLifeCycleHooks warmup
        long authObjectChangeCountAtStart = authObjectChangeCount.get();
        indexer.ensureIndexUpToDate();
        authObjectChangeIndexed.getAndAccumulate(authObjectChangeCountAtStart, Math::max);
    }

    public void ensureIndexUpToDateWhenAuthChange() throws CordraException {
        if (authObjectChangeCount.get() > authObjectChangeIndexed.get()) {
            ensureIndexUpToDate();
        }
    }

    public void updateAcls(String objectId, CordraObject.AccessControlList sAcl, AuthenticationResult authResult) throws CordraException, ReadOnlyCordraException, InvalidException {
        if (isReadOnly) throw new ReadOnlyCordraException();
        writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(objectId, null, null, sAcl, null, null, authResult, null, false);
    }

    public boolean shouldIndexPayloads(String type) {
        if (schemas == null) return true; // for creating new Schema objects during bootstrapping
        LazySchema<JsonSchema> schemaAndNode = getSchema(type);
        if (schemaAndNode == null) return true;
        return shouldIndexPayloads(schemaAndNode.getSchemaNode());
    }

    public boolean shouldHashObject(String type) {
        if (schemaCordraObjects == null) return false;
        if (type == null) return false;
        CordraObject schemaObject = schemaCordraObjects.get(type);
        if (schemaObject == null) return false;
        JsonObject content = schemaObject.content.getAsJsonObject();
        if (content.has("hashObject")) {
            return content.get("hashObject").getAsBoolean();
        } else {
            return false;
        }
    }

    public CordraObject getSchemaObject(String type) {
        if (schemaCordraObjects == null) return null;
        if (type == null) return null;
        CordraObject schemaObject = schemaCordraObjects.get(type);
        return schemaObject;
    }

    public Map<String, DefaultAcls> getAllSchemaAclsFromSchemaObjects() {
        Map<String, DefaultAcls> result = new HashMap<>();
        // include CordraDesign and Schema
        for (String type : schemas.keySet()) {
            DefaultAcls defaultAcls = getAclForObjectTypeFromSchemaObject(type);
            if (defaultAcls != null) {
                result.put(type, defaultAcls);
            }
        }
        return result;
    }

    public DefaultAcls getAclForObjectTypeFromSchemaObject(String type) {
        DefaultAcls defaultAcls = null;
        if (DESIGN_OBJECT_TYPE.equals(type)) {
            if (design != null && design.builtInTypes != null && design.builtInTypes.CordraDesign != null) {
                defaultAcls = design.builtInTypes.CordraDesign.authConfig;
            }
        } else if ("Schema".equals(type)) {
            if (design != null && design.builtInTypes != null && design.builtInTypes.Schema != null) {
                defaultAcls = design.builtInTypes.Schema.authConfig;
            }
        } else {
            CordraObject schemaObject = getSchemaObject(type);
            defaultAcls = getSchemaAclsFromObject(schemaObject);
        }
        if (defaultAcls == null) {
            try {
                Map<String, Object> context = new HashMap<>();
                defaultAcls = javaScriptHooks.getAuthConfig(type, context);
            } catch (Exception e) {
                logger.error("getAclForObjectTypeFromSchemaObject",e);
                defaultAcls = new DefaultAcls();
            }
        }
        return defaultAcls;
    }

    private DefaultAcls getSchemaAclsFromObject(CordraObject schemaObject) {
        if (schemaObject == null) {
            return null;
        }
        JsonObject content = schemaObject.content.getAsJsonObject();
        if (!content.has("authConfig")) {
            return null;
        }
        JsonObject authConfig = content.get("authConfig").getAsJsonObject();
        DefaultAcls defaultAcls = GsonUtility.getGson().fromJson(authConfig, DefaultAcls.class);
        return defaultAcls;
    }

    private static boolean shouldIndexPayloads(JsonNode schemaNode) {
        JsonNode indexPayloadsProperty = SchemaUtil.getDeepCordraSchemaProperty(schemaNode, "indexPayloads");
        if (indexPayloadsProperty == null) return true;
        if (!indexPayloadsProperty.isBoolean()) return true;
        return indexPayloadsProperty.asBoolean();
    }

    public List<String> getTypesPermittedToCreate(AuthenticationResult authResult) throws CordraException {
        ensureIndexUpToDate();
        List<String> allTypes = new ArrayList<>(design.schemas.keySet());
        List<String> result = aclEnforcer.filterTypesPermittedToCreate(authResult, allTypes);
        return result;
    }

    public boolean isReadOnly() {
        return isReadOnly;
    }

    public void shutdown() {
        closed = true;
        for (Callable<Void> shutdownHook : shutdownHooks) {
            try { shutdownHook.call(); } catch (Exception e) { logger.error("Shutdown error", e); }
        }
        try { backgroundSchemaWarmUpExec.shutdown(); } catch (Exception e) { logger.error("Shutdown error", e); }
        if (lightWeightHandleServer != null) {
            lightWeightHandleServer.shutdown();
        }
        BatchUploadProcessor.shutdownIfNecessary();
        if (reindexer != null) {
            reindexer.shutdown();
        }
        if (replicationConsumer != null) {
            try { replicationConsumer.shutdown(); } catch (Exception e) { logger.error("Shutdown error", e); }
        }
        if (stripedTaskRunner != null) {
            stripedTaskRunner.shutdown();
        }
        if (replicationProducer != null) {
            try { replicationProducer.shutdown(); } catch (Exception e) { logger.error("Shutdown error", e); }
        }
        try { handlesUpdater.shutdown(); } catch (Exception e) { logger.error("Shutdown error", e); }
        try { javaScriptEnvironment.shutdown(); } catch (Exception e) { logger.error("Shutdown error", e); }
        try { preCacheExecutorService.shutdown(); } catch (Exception e) { logger.error("Shutdown error", e); }
        try { indexer.close(); } catch (Exception e) { logger.error("Shutdown error", e); }
        if (transactionReprocessingQueue != null) {
            try { transactionReprocessingQueue.shutdown(); } catch (Exception e) { logger.error("Shutdown error", e); }
        }
        syncObjects.shutdown();
        try { storage.close(); } catch (Exception e) { logger.error("Shutdown error", e); }
    }

    private void sendUpdateReplicationMessage(CordraObject co) throws CordraException {
        if (replicationProducer == null) {
            return;
        }
        CordraObjectWithPayloadsAsStrings cos;
        try {
            boolean includePayloads = false;
            if (design != null && design.includePayloadsInReplicationMessages != null) {
                includePayloads = design.includePayloadsInReplicationMessages;
            }
            cos = CordraObjectWithPayloadsAsStrings.fromCordraObject(co, storage, includePayloads);
            ReplicationMessage replicationMessage = new ReplicationMessage();
            replicationMessage.cordraClusterId = this.cordraClusterId;
            replicationMessage.type = ReplicationMessage.Type.UPDATE;
            replicationMessage.object = cos;
            replicationMessage.handle = co.id;
            String message = gson.toJson(replicationMessage);
            replicationProducer.send(co.id, message);
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private void sendDeleteReplicationMessage(String id) throws CordraException {
        if (replicationProducer == null) {
            return;
        }
        ReplicationMessage replicationMessage = new ReplicationMessage();
        replicationMessage.type = ReplicationMessage.Type.DELETE;
        replicationMessage.cordraClusterId = this.cordraClusterId;
        replicationMessage.handle = id;
        String message = gson.toJson(replicationMessage);
        replicationProducer.send(id, message);
    }

    Object stripePicker(String message) {
        ReplicationMessage txn = gson.fromJson(message, ReplicationMessage.class);
        return txn.handle;
    }

    void applyReplicationMessage(String message) {
        ReplicationMessage txn = gson.fromJson(message, ReplicationMessage.class);
        if (cordraClusterId.equals(txn.cordraClusterId)) {
            return;
        }
        try {
            if (DESIGN_OBJECT_ID.equals(txn.handle)) {
                CordraObjectWithPayloadsAsStrings cos = txn.object;
                replicateDesignObject(cos);
            } else if (txn.type == ReplicationMessage.Type.DELETE) {
                delete(txn.handle, null, false);
            } else {
                //UPDATE
                CordraObjectWithPayloadsAsStrings cos = txn.object;
                replicateCordraObject(cos);
            }
        } catch (Exception e) {
            alerter.alert("Error replicating " + txn.handle + ": " + e);
            logger.error("Error replicating " + txn.handle, e);
        }
    }

    private void replicateCordraObject(CordraObjectWithPayloadsAsStrings cos) throws CordraException, InvalidException, ReadOnlyCordraException {
        String id = cos.cordraObject.id;
        String type = cos.cordraObject.type;
        objectLocker.lock(id);
        CordraTransaction txn = null;
        try {
            txn = makeUpdateTransactionFor(id, false);
            CordraObject existingCo = storage.get(id);
            CordraObject co = cos.cordraObject;
            List<Payload> payloads = getPayloadsFromReplicatedObject(cos);
            List<String> payloadsToDelete = new ArrayList<>();
            if (existingCo != null && existingCo.payloads != null) {
                for (Payload payload : existingCo.payloads) {
                    if (getCordraObjectPayloadByName(co, payload.name) != null) {
                        payloadsToDelete.add(payload.name);
                    }
                }
            }
            boolean isDryRun = false;
            Map<String, JsonNode> pointerToSchemaMap = getPointerToSchemaMap(type, JacksonUtil.gsonToJackson(co.content));
            boolean isCreate = existingCo == null;
            addAndDeletePayloads(co, payloadsToDelete, payloads);
            if (!isDryRun) {
                co = storeCordraObject(co, isCreate);
                indexCordraObject(co, type, pointerToSchemaMap);
            }
            cleanupPayloads(co);
            if (isUserOrGroup(co)) {
                authObjectChangeCount.incrementAndGet();
                authCache.clearAllGroupsForUserValues();
                authCache.clearAllUserIdForUsernameValues();
                signalWatcher.sendSignal(SignalWatcher.Signal.AUTH_CHANGE);
                if (!isUserAccountActive(co)) {
                    invalidateSessionsForUser(co.id);
                }
                preCache();
            }
            if (validator.hasJavaScriptModules(pointerToSchemaMap)) {
                signalWatcher.sendSignal(SignalWatcher.Signal.JAVASCRIPT_CLEAR_CACHE);
                cordraRequireLookup.clearAllObjectIdsForModuleValues();
                javaScriptEnvironment.clearCache();
                backgroundSchemaWarmUpExec.submit(() -> warmUpJavaScript(jsonSchemaFactory.getGeneration(), javaScriptWarmUpGeneration.incrementAndGet()));
            }
            if ("Schema".equals(type)) {
                addToKnownSchemas(id);
            }
            transactionManager.closeTransaction(txn.txnId, cordraServiceId);
        } catch (Exception e) {
            if (txn != null) {
                try {
                    transactionReprocessingQueue.insert(txn, cordraServiceId);
                    transactionManager.closeTransaction(txn.txnId, cordraServiceId);
                } catch (Exception ex) {
                    logger.error("Error in replicateCordraObject; followed by reprocessing error", e);
                    throw ex;
                }
            }
            throw e;
        } finally {
            objectLocker.release(id);
        }
    }

    private void invalidateSessionsForUser(String userId) {
        if (userId == null || sessionManager == null) return;
        sessionManager.getSessionsByKeyValue("userId", userId)
                .forEach(sessionManager::ensureInvalid);
    }

    public String getToken(AuthenticationResult authResult, Function<Map<String, Object>, String> sessionCreator) {
        // see also ServletAuthUtil.LegacySessionsCallbackOptions.callback
        Map<String, Object> atts = new HashMap<>();
        if (authResult.username != null) atts.put("username", authResult.username);
        if (authResult.userId != null) atts.put("userId", authResult.userId);
        atts.put("grantAuthenticatedAccess", authResult.grantAuthenticatedAccess);
        if (authResult.exp != null) atts.put("exp", authResult.exp);
        if (authResult.hookSpecifiedGroupIds != null) atts.put("hookSpecifiedGroupIds", authResult.hookSpecifiedGroupIds);
        atts.put("bypassCordraGroupObjects", authResult.bypassCordraGroupObjects);
        if (sessionCreator == null) {
            return sessionManager.getSession(null, null, true, atts).getId();
        } else {
            return sessionCreator.apply(atts);
        }
    }

    public AuthenticationResult introspectTokenForAuthenticationResult(String token, boolean full, Function<String, Function<String, Object>> sessionGetter) throws CordraException {
        if (sessionGetter == null) {
            sessionGetter = tokenParam -> {
                if (tokenParam == null) return null;
                HttpSession session = sessionManager.getSession(null, tokenParam, false);
                if (session == null) return null;
                if (session.getAttribute("userId") == null || ServletAuthUtil.isSessionExpired(session)) {
                    ServletAuthUtil.invalidateQuietly(session);
                    return null;
                }
                return session::getAttribute;
            };
        }
        Function<String, Object> getAttribute = sessionGetter.apply(token);
        if (getAttribute == null) {
            // Note: apparently the only instance of active=false on non-anonymous AuthenticationResult (check-credentials throws an error instead)
            return new AuthenticationResult(false, null, null, null, false, null);
        } else {
            // see also ServletAuthUtil.getOptionsFromSessionsUsingRequest
            String userId = (String) getAttribute.apply("userId");
            String username = (String) getAttribute.apply("username");
            Boolean grantAuthenticatedAccessObject = (Boolean) getAttribute.apply("grantAuthenticatedAccess");
            boolean grantAuthenticatedAccess = grantAuthenticatedAccessObject == null ? false : grantAuthenticatedAccessObject.booleanValue();
            Long exp = (Long) getAttribute.apply("exp");
            List<String> groupIds = null;
            if (full) {
                @SuppressWarnings("unchecked")
                List<String> hookSpecifiedGroupIds = (List<String>) getAttribute.apply("hookSpecifiedGroupIds");
                boolean bypassCordraGroupObjects = getAttribute.apply("bypassCordraGroupObjects") == Boolean.TRUE;
                if (bypassCordraGroupObjects) {
                    groupIds = hookSpecifiedGroupIds;
                } else {
                    groupIds = aclEnforcer.getGroupsForUserAndGroups(userId, hookSpecifiedGroupIds);
                }
            }
            return new AuthenticationResult(true, userId, username, groupIds, grantAuthenticatedAccess, exp);
        }
    }

    public AuthResponse introspectToken(String token, boolean full, Function<String, Function<String, Object>> sessionGetter) throws CordraException {
        AuthenticationResult authResult = introspectTokenForAuthenticationResult(token, full, sessionGetter);
        List<String> typesPermittedToCreate = null;
        if (authResult.active && full) {
            typesPermittedToCreate = getTypesPermittedToCreate(authResult);
        }
        return new AuthResponse(authResult.active, authResult.userId, authResult.username, typesPermittedToCreate, authResult.groupIds, authResult.exp);
    }

    @SuppressWarnings("unused")
    public void revokeToken(String token, Consumer<String> sessionRevoker) throws CordraException {
        if (sessionRevoker == null) {
            if (token == null) return;
            HttpSession session = sessionManager.getSession(null, token, false);
            if (session != null) session.invalidate();
        } else {
            sessionRevoker.accept(token);
        }
    }

    private boolean isUserOrGroup(CordraObject co) {
        if (co.metadata == null || co.metadata.internalMetadata == null) return false;
        if (co.metadata.internalMetadata.has("username")) return true;
        if (co.metadata.internalMetadata.has("users")) return true;
        return false;
    }

    private List<Payload> getPayloadsFromReplicatedObject(CordraObjectWithPayloadsAsStrings cos) {
        List<Payload> cordraObjectPayloads = cos.cordraObject.payloads;
        if (cordraObjectPayloads == null) return null;
        for (Payload payload : cordraObjectPayloads) {
            InputStream in;
            if (cos.payloads == null) {
                in = new ByteArrayInputStream(new byte[0]);
            } else {
                String base64OfPayload = cos.payloads.get(payload.name);
                if (base64OfPayload == null) {
                    in = new ByteArrayInputStream(new byte[0]);
                } else {
                    in = new ByteArrayInputStream(Base64.getDecoder().decode(base64OfPayload));
                }
            }
            payload.setInputStream(in);
        }
        return cordraObjectPayloads;
    }

    private void replicateDesignObject(CordraObjectWithPayloadsAsStrings cos) throws CordraException {
        designLocker.writeLock().acquire();
        try {
            cos.writeIntoStorage(storage);
            signalWatcher.sendSignal(SignalWatcher.Signal.DESIGN);
            loadStatefulData();
        } finally {
            designLocker.writeLock().release();
        }
    }

    public CordraConfig getCordraConfig() { return this.cordraConfig; }

    public PublicKey getPublicKey() { return this.publicKey; }

    public PrivateKey getPrivateKey() { return this.privateKey; }

    public AuthCache getAuthCache() { return this.authCache; }

    public KeyPairAuthJtiChecker getKeyPairAuthJtiChecker() { return this.keyPairAuthJtiChecker; }

    public CordraObjectSchemaValidator getSchemaValidator() { return this.validator; }

    public GenerationalFgeSchemaFactory getJsonSchemaFactory() { return this.jsonSchemaFactory; }
}
