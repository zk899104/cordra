package net.cnri.cordra;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.auth.InternalAuthenticator;
import net.cnri.cordra.auth.InternalRequestOptions;
import net.cnri.cordra.auth.PreAuthenticatedOptions;
import net.cnri.cordra.javascript.DirectIo;
import net.cnri.cordra.model.FileMetadataResponse;
import net.cnri.cordra.model.Range;
import net.cnri.cordra.model.Version;
import net.cnri.cordra.relationships.Relationships;
import net.cnri.cordra.relationships.RelationshipsService;
import net.cnri.cordra.relationships.RelationshipsServiceFactory;
import net.cnri.cordra.util.HttpUtil;
import net.cnri.cordra.util.JacksonUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.script.ScriptException;

public class InternalCordraClient implements CordraClient, DefaultingFromCallResponseHandlerCordraClient {

    private final CordraService cordra;
    private final InternalAuthenticator authenticator;
    private final RelationshipsService relationshipsService;

    private volatile ExecutorService callResponseHandlerExecServ;

    public InternalCordraClient(CordraService cordra) {
        this.cordra = cordra;
        this.authenticator = new InternalAuthenticator(cordra);
        this.relationshipsService = RelationshipsServiceFactory.getRelationshipsService();
    }

    @Override
    public ExecutorService getCallResponseHandlerExecutorService() {
        if (callResponseHandlerExecServ != null) return callResponseHandlerExecServ;
        synchronized (this) {
            if (callResponseHandlerExecServ != null) return callResponseHandlerExecServ;
            callResponseHandlerExecServ = Executors.newCachedThreadPool();
            return callResponseHandlerExecServ;
        }
    }

    @Override
    public CordraObject get(String id, Options options) throws CordraException {
        //Does Not consider jsonPointer
        //Does consider options.filter, filter must be in full form
        //Does NOT consider options.full, that must be handled at the servlet level

        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject co;
            if (options.filter != null) {
                boolean isFull = true;
                JsonElement jsonElement = cordra.getObjectFilterByJsonPointers(id, authResult, options.filter, isFull);
                co = getGson().fromJson(jsonElement, CordraObject.class);
            } else {
                co = cordra.getContentPlusMetaWithPostProcessing(id, authResult);
            }
            if (options.includeResponseContext) {
                if (co.responseContext == null) co.responseContext = new JsonObject();
                co.responseContext.addProperty("permission", cordra.getAclEnforcer().permittedOperations(authResult, co).toString());
            }
            return co;
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        } catch (NotFoundCordraException nfce) {
            return null;
        }
    }

    public JsonElement getJsonAtPointer(String id, String jsonPointer, Options options) throws CordraException {
        //works on Full CordraObject, servlet responsible for modifying jsonPointer to full form.
        //New method
        AuthenticationResult authResult = authenticator.authenticate(options);
        JsonElement jsonElement = cordra.getAtJsonPointer(id, authResult, jsonPointer);
        return jsonElement;
    }

    public CordraObject updateAtJsonPointer(String objectId, String jsonPointer, JsonElement replacementJsonData, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject co = cordra.modifyObjectAtJsonPointer(objectId, jsonPointer, replacementJsonData.toString(), authResult, options.isDryRun);
            return co;
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        }
    }

    public JsonAndMediaType getJsonAndMediaTypeAtPointer(String id, String jsonPointer, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        JsonElement jsonElement = cordra.getAtJsonPointer(id, authResult, jsonPointer);
        CordraObject co = cordra.getCordraObject(id);
        String mediaType = cordra.getMediaType(co.type, JacksonUtil.gsonToJackson(co.content), jsonPointer);
        return new JsonAndMediaType(jsonElement, mediaType);
    }

    public static class JsonAndMediaType {
        public final JsonElement json;
        public final String mediaType;

        public JsonAndMediaType(JsonElement json, String mediaType) {
            this.json = json;
            this.mediaType = mediaType;
        }
    }

    public FileMetadataResponse getPayloadMetadata(String id, String payloadName, Options options) throws CordraException {
        //New method
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            return cordra.getPayloadMetadata(id, payloadName, authResult);
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public void getPartialPayload(String id, String payloadName, Long start, Long end, CallResponseHandler handler, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject co = cordra.getCordraObjectForPayload(id, payloadName, start, end, authResult);
            Payload payload = CordraService.getCordraObjectPayloadByName(co, payloadName);
            if (payload == null) {
                throw new NotFoundCordraException("No payload " + payloadName + " in object " + id);
            }
            if (setPayloadResponseHeadersReturnRangeOkay(handler, payload, start, end)) {
                DirectIo directIo = new DirectIoForCallResponseHandler(handler);
                // Note that authorization happened at the previous call to CordraService
                cordra.streamPayload(co, payload, new Range(start, end), authResult, directIo);
            }
        } catch (ScriptException | InterruptedException | IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private boolean setPayloadResponseHeadersReturnRangeOkay(CallResponseHandler handler, Payload payload, Long start, Long end) {
        Range range = new Range(start, end);
        String mediaType = payload.mediaType;
        String filename = payload.filename;
        long size = payload.size;
        if (size == 0) {
            // ignore range request for empty payload
            handler.setLength(0);
        } else if (size > 0) {
            if (!range.isPartial()) {
                handler.setLength(size);
            } else {
                if (!range.isSatisfiable(size)) {
                    handler.setRange(true, true, null, null, size);
                    return false;
                }
                range = range.withSize(size);
                if (!range.isPartial()) {
                    handler.setLength(size);
                } else {
                    handler.setRange(true, false, range.getStart(), range.getEnd(), size);
                    handler.setLength(range.getEnd() - range.getStart() + 1);
                }
            }
        } else if (size < 0) {
            // unknown size
            if (range.isPartial()) {
                handler.setRange(true, false, null, null, null);
            }
        }
        if (mediaType == null) {
            mediaType = "application/octet-stream";
        }
        handler.setFilename(filename);
        handler.setMediaType(mediaType);
        return true;
    }

    @Override
    public CordraObject create(CordraObject d, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject co = cordra.writeJsonAndPayloadsIntoCordraObjectIfValid(d.type, d.getContentAsString(), d.acl, d.userMetadata, d.payloads, d.id, authResult, options.isDryRun);
            co = CordraService.copyOfCordraObjectRemovingInternalMetadata(co);
            if (options.includeResponseContext) {
                if (co.responseContext == null) co.responseContext = new JsonObject();
                co.responseContext.addProperty("permission", cordra.getAclEnforcer().permittedOperations(authResult, co).toString());
            }
            return co;
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        }
    }

    @Override
    public CordraObject update(CordraObject d, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject co = cordra.writeJsonAndPayloadsIntoCordraObjectIfValidAsUpdate(d.id, d.type, d.getContentAsString(), d.acl, d.userMetadata, d.payloads, authResult, d.getPayloadsToDelete(), options.isDryRun);
            co = CordraService.copyOfCordraObjectRemovingInternalMetadata(co);
            if (options.includeResponseContext) {
                if (co.responseContext == null) co.responseContext = new JsonObject();
                co.responseContext.addProperty("permission", cordra.getAclEnforcer().permittedOperations(authResult, co).toString());
            }
            return co;
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        }
    }

    @Override
    public List<String> listMethods(String objectId, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            return cordra.listMethodsForUser(authResult, null, objectId, false, options.includeCrud);
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public List<String> listMethodsForType(String type, boolean isStatic, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            return cordra.listMethodsForUser(authResult, type, null, isStatic, options.includeCrud);
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public void call(String objectId, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            String result = cordra.call(objectId, null, authResult, methodName, new DirectIoForCall(handler, input, options), isGet(options));
            if (result != null) {
                // don't set content-type for empty response
                handler.setMediaType("application/json");
                handler.getWriter().write(result);
            }
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public void callForType(String type, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            String result = cordra.call(null, type, authResult, methodName, new DirectIoForCall(handler, input, options), isGet(options));
            if (result != null) {
                // don't set content-type for empty response
                handler.setMediaType("application/json");
                handler.getWriter().write(result);
            }
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private boolean isGet(Options options) {
        if (options instanceof InternalRequestOptions) {
            return ((InternalRequestOptions) options).isGet;
        } else {
            return false;
        }
    }

    @Override
    public VersionInfo publishVersion(String objectId, String versionId, boolean clonePayloads, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject versionObject = cordra.publishVersion(objectId, versionId, clonePayloads, authResult);
            VersionInfo versionInfo = getVersionInfoFor(versionObject);
            return versionInfo;
        } catch (VersionException e) {
            throw new BadRequestCordraException(e);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        }
    }

    @Override
    public List<VersionInfo> getVersionsFor(String id, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            List<CordraObject> versions = cordra.getVersionsFor(id, authResult);
            List<VersionInfo> versionInfos = getVersionInfoListFor(versions);
            return versionInfos;
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private List<VersionInfo> getVersionInfoListFor(List<CordraObject> versions) {
        List<VersionInfo> result = new ArrayList<>();
        for (CordraObject version : versions) {
            VersionInfo versionInfo = getVersionInfoFor(version);
            result.add(versionInfo);
        }
        return result;
    }

    private VersionInfo getVersionInfoFor(CordraObject co) {
        VersionInfo versionInfo = new VersionInfo();
        versionInfo.id = co.id;
        versionInfo.versionOf = co.metadata.versionOf;
        versionInfo.type = co.type;
        if (versionInfo.versionOf == null) {
            versionInfo.isTip = true;
            versionInfo.modifiedOn = co.metadata.modifiedOn;
        } else {
            versionInfo.publishedBy = co.metadata.publishedBy;
            versionInfo.publishedOn = co.metadata.publishedOn;
        }
        return versionInfo;
    }

    @Override
    public void delete(String id, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            cordra.delete(id, authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        }
    }

    public void deletePayload(String id, String payloadName, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            cordra.deletePayload(id, payloadName, authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public void deleteJsonAtPointer(String id, String jsonPointer, Options options) throws CordraException {
        //New method
        //works on Full CordraObject, servlet responsible for modifying jsonPointer to full form.
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            cordra.deleteJsonPointer(id, jsonPointer, authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public SearchResults<CordraObject> list(Options options) throws CordraException {
        throw new UnsupportedOperationException();
    }

    @Override
    public SearchResults<String> listHandles(Options options) throws CordraException {
        throw new UnsupportedOperationException();
    }

    @Override
    public SearchResults<CordraObject> search(String query, QueryParams params, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        cordra.ensureIndexUpToDate();
        boolean excludeVersions = true;
        if (options instanceof InternalRequestOptions) excludeVersions = ((InternalRequestOptions) options).excludeVersions;
        boolean isPostProcess = true;
        try {
            SearchResults<CordraObject> results = cordra.searchWithQueryCustomizationAndRestriction(query, params,
                    isPostProcess, authResult, excludeVersions);
            return results;
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public SearchResults<String> searchHandles(String query, QueryParams params, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        cordra.ensureIndexUpToDate();
        boolean excludeVersions = true;
        if (options instanceof InternalRequestOptions) excludeVersions = ((InternalRequestOptions) options).excludeVersions;
        boolean isPostProcess = true;
        try {
            SearchResults<String> results = cordra.searchHandlesWithQueryCustomizationAndRestriction(query, params,
                    isPostProcess, authResult, excludeVersions);
            return results;
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public AuthenticationResult internalAuthenticate(Options options) throws CordraException {
        return authenticator.authenticate(options);
    }

    @Override
    public AuthResponse authenticateAndGetResponse(Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        List<String> typesPermittedToCreate = null;
        if (options.full) {
            typesPermittedToCreate = cordra.getTypesPermittedToCreate(authResult);
        }
        return new AuthResponse(authResult.active, authResult.userId, authResult.username, typesPermittedToCreate, authResult.groupIds, authResult.exp);
    }

    @Override
    public AuthTokenResponse getAuthToken(Options options) throws CordraException {
        return getAuthToken(options, null);
    }

    public AuthTokenResponse getAuthToken(Options options, Function<Map<String, Object>, String> sessionCreator)  throws CordraException {
        AuthenticationResult authResult;
        try {
            authResult = authenticator.authenticate(options);
        } catch (UnauthorizedCordraException e) {
            JsonObject errorObject = new JsonObject();
            errorObject.addProperty("error", "invalid_grant");
            errorObject.addProperty("error_description", "Authentication failed");
            errorObject.addProperty("message", "Authentication failed");
            if (e.isPasswordChangeRequired()) {
                errorObject.addProperty("passwordChangeRequired", true);
            }
            throw new BadRequestCordraException(errorObject);

        }
        String token = cordra.getToken(authResult, sessionCreator);
        List<String> typesPermittedToCreate = null;
        if (options.full) {
            typesPermittedToCreate = cordra.getTypesPermittedToCreate(authResult);
        }
        return new AuthTokenResponse(authResult.active, token, authResult.userId, authResult.username, typesPermittedToCreate, authResult.groupIds, authResult.exp);
    }

    @Override
    public AuthResponse introspectToken(Options options) throws CordraException {
        return introspectToken(options, null);
    }

    public AuthResponse introspectToken(Options options, Function<String, Function<String, Object>> sessionGetter) throws CordraException {
        return cordra.introspectToken(options.token, options.full, sessionGetter);
    }

    @Override
    public void revokeToken(Options options) throws CordraException {
        revokeToken(options, null);
    }

    public void revokeToken(Options options, Consumer<String> sessionRevoker) throws CordraException {
        cordra.revokeToken(options.token, sessionRevoker);
    }

    public void ensureAdmin(Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        if (!"admin".equals(authResult.userId)) {
            throw new ForbiddenCordraException("Forbidden");
        }
    }

    @Override
    public void changePassword(String newPassword, Options options) throws CordraException {
        if (!(options instanceof InternalRequestOptions)) {
            // this is for testing
            InternalRequestOptions fixedOptions = GsonUtility.getGson().fromJson(GsonUtility.getGson().toJson(options), InternalRequestOptions.class);
            fixedOptions.isChangePassword = true;
            options = fixedOptions;
        }
        AuthenticationResult authResult = authenticator.authenticate(options);
        if (!authResult.active || authResult.userId == null) {
            throw new ForbiddenCordraException("No user");
        }
        if (!"admin".equals(authResult.userId)) {
            // we require an actual Authorization: header, which is sure to have taken the place of any other session
            if (options instanceof PreAuthenticatedOptions) {
                throw new ForbiddenCordraException("Authorization: header required, either Basic or Bearer with JWT for key-based authentication");
            }
        }
        try {
            cordra.updatePasswordForUser(newPassword, authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (CordraException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        }
    }

    @Override
    public void reindexBatch(List<String> batch, Options options) throws CordraException  {
        AuthenticationResult authResult = authenticator.authenticate(options);
        cordra.reindexBatchIds(batch, options.reindexBatchLockObjects, authResult);
    }

    public InitDataResponse getInitData(Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            InitDataResponse initDataResponse = new InitDataResponse();
            initDataResponse.version = Version.getInstance();
            initDataResponse.isActiveSession = authResult.active;
            initDataResponse.username = authResult.username;
            initDataResponse.userId = authResult.userId;
            initDataResponse.typesPermittedToCreate = cordra.getTypesPermittedToCreate(authResult);
            initDataResponse.design = cordra.getDesignAsUser(authResult);
            return initDataResponse;
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public Relationships getRelationshipsFor(String objectId, boolean outboundOnly, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            Relationships relationships = relationshipsService.getRelationshipsFor(objectId, outboundOnly, authResult);
            return relationships;
        } catch (InvalidException | ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public CordraObject.AccessControlList getAclFor(String objectId, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            CordraObject.AccessControlList acl = cordra.getAclFor(objectId, authResult);
            return acl;
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public void updateAcls(String objectId, CordraObject.AccessControlList sAcl, Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            cordra.updateAcls(objectId, sAcl, authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        } catch (InvalidException e) {
            throw new BadRequestCordraException(e.getMessage(), e);
        }
    }

    public void updateAllHandleRecords(Options options) throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        try {
            cordra.updateAllHandleRecords(authResult);
        } catch (ReadOnlyCordraException e) {
            throw new BadRequestCordraException("Cordra is read-only", e);
        }
    }

    public AllHandlesUpdater.UpdateStatus getHandleUpdateStatus(Options options)  throws CordraException {
        AuthenticationResult authResult = authenticator.authenticate(options);
        AllHandlesUpdater.UpdateStatus status = cordra.getHandleUpdateStatus(authResult);
        return status;
    }

    public String getHandleForSuffix(String suffix) {
        return cordra.getHandleForSuffix(suffix);
    }

    public void throwIfLegacySessionsDisallowed() throws CordraException {
        if (!cordra.getDesign().useLegacySessionsApi) {
            throw new UnauthorizedCordraException("Legacy sessions disabled on this Cordra");
        }
    }

    public boolean isDisableBackOffRequestParking() {
        return cordra.isDisableBackOffRequestParking();
    }

    @Override
    public void close() throws IOException, CordraException {
        if (callResponseHandlerExecServ != null) {
            callResponseHandlerExecServ.shutdown();
        }
    }

    private static class DirectIoForCallResponseHandler implements DirectIo {

        private final CallResponseHandler handler;

        public DirectIoForCallResponseHandler(CallResponseHandler handler) {
            this.handler = handler;
        }

        @Override
        public InputStream getInputAsInputStream() throws IOException {
            throw new UnsupportedOperationException();
        }

        @Override
        public Reader getInputAsReader() throws IOException {
            throw new UnsupportedOperationException();
        }

        @Override
        public OutputStream getOutputAsOutputStream() throws IOException {
            return handler.getOutputStream();
        }

        @Override
        public Writer getOutputAsWriter() throws IOException {
            return handler.getWriter();
        }

        @Override
        public String getInputMediaType() {
            throw new UnsupportedOperationException();
        }

        @Override
        public String getInputFilename() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setOutputMediaType(String mediaType) {
            handler.setMediaType(mediaType);
        }

        @Override
        public void setOutputFilename(String filename) {
            handler.setFilename(filename);
        }
    }

    private static class DirectIoForCall extends DirectIoForCallResponseHandler {

        private final InputStream in;
        private final Options options;

        private boolean gotInputStream;
        private Reader reader;

        public DirectIoForCall(CallResponseHandler handler, InputStream in, Options options) {
            super(handler);
            this.in = in;
            this.options = options;
        }

        @Override
        public InputStream getInputAsInputStream() throws IOException {
            if (reader != null) throw new IllegalStateException("Cannot use both getInputAsInputStream and getInputAsReader");
            gotInputStream = true;
            return in;
        }

        @Override
        public Reader getInputAsReader() throws IOException {
            if (reader != null) return reader;
            if (gotInputStream) throw new IllegalStateException("Cannot use both getInputAsInputStream and getInputAsReader");
            if (in == null) return null;
            String charset = HttpUtil.getCharset(getInputMediaType());
            reader = new InputStreamReader(in, charset);
            return reader;
        }

        @Override
        public String getInputMediaType() {
            if (options.callHeaders == null) return null;
            return options.callHeaders.mediaType;
        }

        @Override
        public String getInputFilename() {
            if (options.callHeaders == null) return null;
            return options.callHeaders.filename;
        }
    }

}
