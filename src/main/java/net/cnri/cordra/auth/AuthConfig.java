package net.cnri.cordra.auth;

import java.util.HashMap;
import java.util.Map;

public class AuthConfig implements AuthConfigInterface {
    public Map<String, DefaultAcls> schemaAcls;
    public DefaultAcls defaultAcls;

    public AuthConfig() {
        schemaAcls = new HashMap<>();
        defaultAcls = new DefaultAcls();
    }

    @Override
    public Map<String, DefaultAcls> getSchemaAcls() {
        return schemaAcls;
    }

    @Override
    public DefaultAcls getDefaultAcls() {
        return defaultAcls;
    }

    @Override
    public DefaultAcls getAclForObjectType(String objectType) {
        DefaultAcls res = schemaAcls.get(objectType);
        if (res == null) {
            res = defaultAcls;
        }
        return res;
    }
}
