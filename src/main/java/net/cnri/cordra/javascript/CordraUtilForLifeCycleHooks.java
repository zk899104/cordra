package net.cnri.cordra.javascript;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.google.gson.*;

import net.cnri.cordra.*;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.storage.CordraStorage;
import net.cnri.cordra.util.JacksonUtil;
import net.cnri.cordra.util.SearchUtil;
import net.handle.hdllib.trust.JsonWebSignature;
import net.handle.hdllib.trust.JsonWebSignatureFactory;
import net.handle.hdllib.trust.TrustException;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;

public class CordraUtilForLifeCycleHooks {

    private CordraService cordraService;
    private Gson gson;

    public void init(@SuppressWarnings("hiding") CordraService cordraService) {
        this.cordraService = cordraService;
        this.gson = GsonUtility.getGson();
    }

    public String hashJson(String jsonString, String algorithm) {
        if (jsonString == null || jsonString.isEmpty()) return null;
        JsonElement el = JsonParser.parseString(jsonString);
        return CordraObjectHasher.hashJson(el, algorithm);
    }

    public String escapeForQuery(String s) {
        return SearchUtil.escape(s);
    }

    public String verifySecret(String objectJsonOrId, String jsonPointer, String secret) throws CordraException {
        if (objectJsonOrId == null) return "false";
        CordraObject cordraObject;
        if (objectJsonOrId.trim().startsWith("{")) {
            CordraObject passedInCordraObject = gson.fromJson(objectJsonOrId, CordraObject.class);
            if (passedInCordraObject.id == null) return "false";
            cordraObject = cordraService.getCordraObjectOrNull(passedInCordraObject.id);
        } else {
            cordraObject = cordraService.getCordraObjectOrNull(objectJsonOrId);
        }
        return String.valueOf(cordraService.verifySecureProperty(cordraObject, jsonPointer, secret));
    }

    public String verifyHashes(String coJson) throws CordraException {
        CordraObject co = gson.fromJson(coJson, CordraObject.class);
        CordraStorage storage = cordraService.getStorage();
        CordraObjectHasher hasher = new CordraObjectHasher();
        CordraObjectHasher.VerificationReport report = hasher.verify(co, storage);
        String result = gson.toJson(report);
        return result;
    }

    public String signWithKey(String payload, String privateKey, boolean useJsonSerialization) throws TrustException, CordraException {
        if (privateKey == null) {
            throw new InternalErrorCordraException("No key provided for signing");
        }
        if (payload == null) {
            throw new InternalErrorCordraException("No payload provided for signing");
        }
        PrivateKey key = gson.fromJson(privateKey, PrivateKey.class);
        JsonWebSignature jws = JsonWebSignatureFactory.getInstance().create(payload, key);
        if (useJsonSerialization) {
            return jws.serializeToJson();
        } else {
            return jws.serialize();
        }
    }

    public String signWithCordraKey(String payload, boolean useJsonSerialization) throws TrustException, CordraException {
        PrivateKey privateKey = cordraService.getPrivateKey();
        if (privateKey == null) {
            throw new InternalErrorCordraException("No key available for signing");
        }
        if (payload == null) {
            throw new InternalErrorCordraException("No payload provided for signing");
        }
        JsonWebSignature jws = JsonWebSignatureFactory.getInstance().create(payload, privateKey);
        if (useJsonSerialization) {
            return jws.serializeToJson();
        } else {
            return jws.serialize();
        }
    }

    public boolean verifyWithCordraKey(String jwt) throws TrustException, InternalErrorCordraException {
        PublicKey publicKey = cordraService.getPublicKey();
        if (publicKey == null) {
            throw new InternalErrorCordraException("No key available for verifying");
        }
        JsonWebSignatureFactory signatureFactory = JsonWebSignatureFactory.getInstance();
        JsonWebSignature jws = signatureFactory.deserialize(jwt);
        // Note: iss/exp/nbf not checked but only the signature
        boolean isValid = jws.validates(publicKey);
        return isValid;
    }

    public boolean verifyWithKey(String jwt, String publicKeyString)  throws TrustException {
        PublicKey publicKey = gson.fromJson(publicKeyString, PublicKey.class);
        JsonWebSignatureFactory signatureFactory = JsonWebSignatureFactory.getInstance();
        JsonWebSignature jws = signatureFactory.deserialize(jwt);
        boolean isValid = jws.validates(publicKey);
        return isValid;
    }

    public String extractJwtPayload(String jwt) throws TrustException {
        JsonWebSignatureFactory signatureFactory = JsonWebSignatureFactory.getInstance();
        JsonWebSignature jws = signatureFactory.deserialize(jwt);
        String payloadJson = jws.getPayloadAsString();
        return payloadJson;
    }

    public String getCordraPublicKey() {
        PublicKey key = cordraService.getPublicKey();
        if (key == null) return null;
        return gson.toJson(cordraService.getPublicKey());
    }

    public String getDoipProcessorConfig() {
        JsonObject doipConfig = cordraService.getDoipProcessorConfig();
        String result = gson.toJson(doipConfig);
        return result;
    }

    public String getGroupsForUser(String userId) throws CordraException {
        List<String> groupIds = cordraService.getAclEnforcer().getGroupsForUser(userId);
        String result = gson.toJson(groupIds);
        return result;
    }

    public String validateWithSchema(String json, String jsonSchema) throws InvalidException, ProcessingException {
        JsonNode jsonNode = JacksonUtil.parseJson(json);
        JsonNode schemaNode = JacksonUtil.parseJson(jsonSchema);
        JsonSchema schema = cordraService.getJsonSchemaFactory().getSchema(schemaNode, "file:/cordra/schemas/").schema;
        ProcessingReport report = schema.validate(jsonNode);
        SchemaValidationReport validationReport = SchemaValidationReport.from(report);
        String resultString = gson.toJson(validationReport);
        return resultString;
    }
}
