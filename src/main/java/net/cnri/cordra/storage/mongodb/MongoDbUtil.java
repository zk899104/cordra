package net.cnri.cordra.storage.mongodb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.cnri.util.StringUtils;

public class MongoDbUtil {

    public static String CORDRA_NUMBER_KEY = "%cordra-number";

    public static Object jsonElementToMongoObject(JsonElement value) {
        if (value.isJsonObject()) {
            return jsonObjectToDocument(value.getAsJsonObject());
        } else if (value.isJsonArray()) {
            return jsonArrayToList(value.getAsJsonArray());
        } else if (value.isJsonNull()) {
            return null;
        } else {
            return convertPrimitiveForMongo(value.getAsJsonPrimitive());
        }
    }

    private static Document jsonObjectToDocument(JsonObject obj) {
        Document doc = new Document();
        for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
            String key = entry.getKey();
            JsonElement value = entry.getValue();
            doc.put(encodeKey(key), jsonElementToMongoObject(value));
        }
        return doc;
    }

    private static List<Object> jsonArrayToList(JsonArray arr) {
        List<Object> res = new ArrayList<>(arr.size());
        for (JsonElement value : arr) {
            res.add(jsonElementToMongoObject(value));
        }
        return res;
    }

    private static Object convertPrimitiveForMongo(JsonPrimitive prim) {
        if (prim.isBoolean()) {
            return prim.getAsBoolean();
        } else if (prim.isString()) {
            return prim.getAsString();
        } else {
            String numberString = prim.getAsString();
            BigDecimal bigDecimalValue = prim.getAsBigDecimal();
            if (!numberString.contains(".") && !numberString.contains("e-") && !numberString.contains("E-")) {
                try {
                    long longValue = bigDecimalValue.longValueExact();
                    if (longValue >= Integer.MIN_VALUE && longValue <= Integer.MAX_VALUE) {
                        return (int)longValue;
                    }
                    return longValue;
                } catch (ArithmeticException e) {
                    // fall-through
                }
            }
            double doubleValue = bigDecimalValue.doubleValue();
            if (doubleValue != Double.POSITIVE_INFINITY && doubleValue != Double.NEGATIVE_INFINITY && bigDecimalValue.compareTo(new BigDecimal(String.valueOf(doubleValue))) == 0) {
                return doubleValue;
            }
            return nonNumericNumberRepresentation(numberString);
        }
    }

    private static Document nonNumericNumberRepresentation(String numberString) {
        Document doc = new Document();
        doc.put(CORDRA_NUMBER_KEY, numberString);
        return doc;
    }

    public static JsonElement mongoObjectToJsonElement(Object obj) {
        if (obj == null) return JsonNull.INSTANCE;
        if (obj instanceof List<?>) {
            List<?> list = (List<?>)obj;
            JsonArray res = new JsonArray(list.size());
            for (Object item : list) {
                res.add(mongoObjectToJsonElement(item));
            }
            return res;
        }
        if (obj instanceof Map<?,?>) {
            @SuppressWarnings("unchecked")
            Map<String, Object> map = (Map<String, Object>)obj;
            Object numberVal = map.get(CORDRA_NUMBER_KEY);
            if (numberVal != null) {
                return new JsonPrimitive(new BigDecimal((String)numberVal));
            }
            JsonObject res = new JsonObject();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if ("_id".equals(entry.getKey())) continue;
                res.add(decodeKey(entry.getKey()), mongoObjectToJsonElement(entry.getValue()));
            }
            return res;
        }
        if (obj instanceof Boolean) {
            return new JsonPrimitive((Boolean)obj);
        }
        if (obj instanceof Number) {
            return new JsonPrimitive((Number)obj);
        }
        if (obj instanceof String) {
            return new JsonPrimitive((String)obj);
        }
        throw new IllegalArgumentException("Not an expected MongoDB object " + obj.getClass());
    }

    static boolean isKeyNeedsEncoding(String key) {
        for (int i = 0; i < key.length(); i++) {
            char c = key.charAt(i);
            if (i == 0 && '$' == c) return true;
            if ('%' == c) return true;
            if ('.' == c) return true;
            if ('\u0000' == c) return true;
        }
        return false;
    }

    static String encodeKey(String key) {
        if (key.equals("_id")) {
            return "%5Fid";
        }
        if (!isKeyNeedsEncoding(key)) {
            return key;
        }
        int firstNormalCharIndex = -1;
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < key.length(); i++) {
            char c = key.charAt(i);
            String replacement = null;
            if (i == 0 && '$' == c) {
                replacement = "%24";
            } else if ('%' == c) {
                replacement = "%25";
            } else if ('.' == c) {
                replacement = "%2E";
            } else if ('\u0000' == c) {
                replacement = "%00";
            }
            if (replacement != null) {
                if (firstNormalCharIndex >= 0) {
                    buffer.append(key, firstNormalCharIndex, i);
                    firstNormalCharIndex = -1;
                }
                buffer.append(replacement);
            } else if (firstNormalCharIndex < 0) {
                firstNormalCharIndex = i;
            }
        }
        if (firstNormalCharIndex >= 0) {
            buffer.append(key, firstNormalCharIndex, key.length());
        }
        return buffer.toString();
    }

    static String decodeKey(String key) {
        if (!key.contains("%")) return key;
        return StringUtils.decodeURLIgnorePlus(key);
    }
}
