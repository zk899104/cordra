package net.cnri.cordra.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.cnri.cordra.*;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.auth.InternalRequestOptions;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.util.StreamUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

@WebServlet(ChangePasswordServlet.SERVLET_PATH)
public class ChangePasswordServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(ChangePasswordServlet.class);

    public static final String SERVLET_PATH = "/users/this/password";

    private static Gson gson = GsonUtility.getGson();
    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        try {
            String password = StreamUtil.readFully(req.getReader());
            InternalRequestOptions options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            options.isChangePassword = true;
            internalCordra.changePassword(password, options);
            gson.toJson(new UpdateResponse(true), resp.getWriter());
        } catch (InternalErrorCordraException e) {
            logger.error("Error changing password", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Error changing password", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private static class UpdateResponse {
        @SuppressWarnings("unused")
        boolean success = false;

        public UpdateResponse(boolean success) {
            this.success = success;
        }
    }
}
