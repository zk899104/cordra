package net.cnri.cordra.web;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class StreamErrorFixingHttpServletResponseWrapper extends HttpServletResponseWrapper {

    private LazyServletOutputStream outStream;
    private PrintWriter printWriter;

    public StreamErrorFixingHttpServletResponseWrapper(HttpServletResponse request) {
        super(request);
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        if (printWriter != null) throw new IllegalStateException("WRITER");
        if (outStream == null) {
            outStream = new LazyServletOutputStream(super::getOutputStream);
        }
        return outStream;
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        if (printWriter == null) {
            if (outStream == null) {
                printWriter = super.getWriter();
            } else {
                outStream.notifyWriter();
                if (!outStream.isUsed()) {
                    printWriter = super.getWriter();
                } else {
                    printWriter = new PrintWriter(new OutputStreamWriter(outStream, super.getCharacterEncoding()));
                }
            }
        }
        return printWriter;
    }

}
