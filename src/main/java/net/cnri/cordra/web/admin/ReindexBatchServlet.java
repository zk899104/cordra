package net.cnri.cordra.web.admin;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.*;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.web.ServletErrorUtil;
import net.cnri.cordra.web.ServletUtil;
import net.cnri.util.StreamUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet({ "/reindexBatch", "/reindexBatch/" })
public class ReindexBatchServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(ReindexBatchServlet.class);

    private InternalCordraClient internalCordra;
    private Gson prettyGson;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            prettyGson = GsonUtility.getPrettyGson();
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String json = StreamUtil.readFully(req.getReader());
        List<String> batch = prettyGson.fromJson(json, new TypeToken<List<String>>(){}.getType());
        boolean lockObjects = ServletUtil.getBooleanParameter(req, "lockObjects", true);
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            options.reindexBatchLockObjects = lockObjects;
            internalCordra.reindexBatch(batch, options);
            resp.getWriter().println("{\"msg\": \"success\"}");
        } catch (CordraException e) {
            ServletErrorUtil.internalServerError(resp);
            logger.error("Error reindexing batch", e);
        }
    }
}
