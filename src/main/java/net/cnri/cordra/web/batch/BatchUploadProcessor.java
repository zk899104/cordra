package net.cnri.cordra.web.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

public class BatchUploadProcessor {

    private static Logger logger = LoggerFactory.getLogger(BatchUploadProcessor.class);

    private static BatchUploadProcessor instance = null;
    private final ExecutorService exec;
    private static volatile boolean running = false;
    private static final int NUM_THREADS = 5;

    private BatchUploadProcessor() {
        exec = Executors.newFixedThreadPool(NUM_THREADS);
        running = true;
    }

    public synchronized static BatchUploadProcessor instance() {
        if (instance == null) {
            instance = new BatchUploadProcessor();
        }
        return instance;
    }

    public void process(BatchUpload batchUpload) throws Exception {
        Future<Void> f = exec.submit(() -> {
            batchUpload.run();
            return null;
        });
        f.get();
    }

    public synchronized static void shutdownIfNecessary() {
        if (instance != null) {
            instance.shutdown();
        }
    }

    public synchronized void shutdown() {
        if (running) {
            exec.shutdown();
            try {
                exec.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                logger.error("Failure to shutdown BatchUploadProcessor ", e);
            }
            running = false;
        }
    }
}
