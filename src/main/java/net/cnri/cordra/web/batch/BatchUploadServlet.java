package net.cnri.cordra.web.batch;

import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.auth.PreAuthenticatedOptions;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.web.ServletErrorUtil;
import net.cnri.cordra.web.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet({ "/batchUpload/*"})
public class BatchUploadServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(BatchUploadServlet.class);

    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private void doBatchUpload(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            AuthenticationResult authResult = internalCordra.internalAuthenticate(options);
            options = buildPreAuthenticatedOptions(authResult);
            String formatParam = req.getParameter("format");
            boolean isFailFast = ServletUtil.getBooleanParameter(req, "failFast", false);
            boolean isParallel = ServletUtil.getBooleanParameter(req, "parallel", true);
            BatchUpload.Format format = BatchUpload.Format.JSON_ARRAY_OR_SEARCH_RESULTS;
            if ("ndjson".equals(formatParam)) {
                format = BatchUpload.Format.NEWLINE_SEPARATED_JSON;
            }
            BatchUpload batch = new BatchUpload(req.getReader(), resp.getWriter(), internalCordra, options, format, isFailFast, isParallel);
            BatchUploadProcessor.instance().process(batch);
        } catch (InternalErrorCordraException e) {
            logger.error("Error loading objects", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Error loading objects", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private Options buildPreAuthenticatedOptions(AuthenticationResult authResult) {
        PreAuthenticatedOptions options = new PreAuthenticatedOptions();
        options.username = authResult.username;
        options.userId = authResult.userId;
        options.grantAuthenticatedAccess = authResult.grantAuthenticatedAccess;
        // this is setting up the PreAuthenticatedOptions so that it will use the groupIds as-is each time
        options.hookSpecifiedGroupIds = authResult.groupIds;
        options.bypassCordraGroupObjects = true;
        options.exp = authResult.exp;
        return options;
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doBatchUpload(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doBatchUpload(req, resp);
    }
}
