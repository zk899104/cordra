package net.cnri.cordra.web.doip;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Arrays;
import java.util.Map;

public class DoipRequestUtil {

    public static JsonObject buildAttributesFromQueryParams(Map<String, String[]> params, JsonObject attributes) {
        if (attributes == null) {
            attributes = new JsonObject();
        }
        {
            String[] values = params.get("attributes");
            if (values != null) {
                String value = values[0];
                if (value != null && !value.trim().equals("null") && !value.trim().equals("undefined")) {
                    attributes = JsonParser.parseString(value).getAsJsonObject();
                }

            }
        }
        for (Map.Entry<String, String[]> param : params.entrySet()) {
            String paramName = param.getKey();
            String value = param.getValue()[0];
            if (paramName.startsWith("attributes.")) {
                String[] nameParts = paramName.split("\\.");
                nameParts = tail(nameParts);
                setIntoObject(attributes, nameParts, value);
            }
        }
        return attributes;
    }

    private static String[] tail(String[] arr) {
        return Arrays.copyOfRange(arr, 1, arr.length);
    }

    private static void setIntoObject(JsonObject obj, String[] nameParts, String value) {
        String name = nameParts[0];
        if (nameParts.length == 1) {
            obj.addProperty(name, value);
            return;
        } else {
            JsonObject sub = null;
            if (obj.has(name)) {
                JsonElement existingSub = obj.get(name);
                if (existingSub.isJsonObject()) {
                    sub = existingSub.getAsJsonObject();
                }
            }
            if (sub == null) {
                sub = new JsonObject();
                obj.add(name, sub);
            }
            String[] tail = tail(nameParts);
            setIntoObject(sub, tail, value);
        }
    }
}
