function SearchResult(containerDiv, result, uiConfig, onHandleClick) {
    var self = this;

    function constructor() {
        var resultCard = $(`
        <div class="search-result">
            <div class="row">
                <div id="preview" class="col-md-9">
                    <div id="header">
                        <a id="headerLink" style="display: block;font-size:18px" target="_blank"></a>
                    </div>
                    <div id="details" class="details"></div>
                </div>
                <div id="metadata" class="metadata col-md-3"></div>
            </div>
        </div>
        `);

        containerDiv.append(resultCard);

        var header = resultCard.find('#header');
        var details = resultCard.find('#details');
        var metadata = resultCard.find('#metadata');


        var headerLink = resultCard.find('#headerLink');
        headerLink.text(result.id);
        headerLink.attr("data-handle", result.id);
        headerLink.attr("href", "#objects/" + result.id)
        if (onHandleClick) headerLink.on("click", onHandleClick);

        var searchResultsConfig = {};
        if (uiConfig) {
            if (uiConfig.searchResults) {
                searchResultsConfig = uiConfig.searchResults;
            }
        }

        var objectIdSpan = $('<span style="display:block"></span>');
        objectIdSpan.append('<i class="fa fa-bullseye"></i>');
        var objectIdLink = $('<a class="list-handles-link" target="_blank">');
        objectIdLink.attr("href", "#objects/" + result.id);
        objectIdLink.text(result.id);
        objectIdLink.attr("data-handle", result.id);
        if (onHandleClick) objectIdLink.on("click", onHandleClick);
        objectIdSpan.append(objectIdLink);
        details.append(objectIdSpan);

        if (searchResultsConfig.includeType) {
            var objectTypeSpan = $('<span style="display:block"></span>');
            objectTypeSpan.append('<i class="fa fa-file-alt"></i>');
            objectTypeSpan.append("Type: " + result.type);
            details.append(objectTypeSpan);
        }

        var previewData = getPreviewData(result);

        for (var jsonPointer in previewData) {
            var thisPreviewData = previewData[jsonPointer];

            var prettifiedPreviewData = prettifyPreviewJson(thisPreviewData.previewJson);
            if (!prettifiedPreviewData) continue;

            if (thisPreviewData.isPrimary) {
                headerLink.text(prettifiedPreviewData);
            } else {
                if (thisPreviewData.excludeTitle) {
                    if (thisPreviewData.isUri) {
                        var link = $('<a style="display:block" target="_blank"></a>');
                        link.text(prettifiedPreviewData);
                        link.attr("href", prettifiedPreviewData);
                        details.append(link);
                    } else {
                        details.text(prettifiedPreviewData);
                    }
                } else {
                    if (thisPreviewData.isUri) {
                        var link = $('<a style="display:block" target="_blank"></a>');
                        link.text(prettifiedPreviewData);
                        link.attr("href", prettifiedPreviewData);
                        details.text(thisPreviewData.title + ": ");
                        details.append(link);
                    } else {

                        var infoHeaderSpan = $('<span style="display:block"><span>').text(
                            thisPreviewData.title + ": " + prettifiedPreviewData
                        );
                        // var infoContentSpan = $('<span class="info-content"></span>').text(
                        //     prettifiedPreviewData
                        // );
                        details.append(infoHeaderSpan);//.append(infoContentSpan);
                    }
                }
            }
        }


        if (searchResultsConfig.includeModifiedDate) {
            var modifiedOn = $('<span style="display:block; float:right; padding-right: 10px;"><span>').text("Modified: " + toDateString(result.metadata.modifiedOn));
            metadata.append(modifiedOn);
        }
        if (searchResultsConfig.includeCreatedDate) {
            var createdOn = $('<span style="display:block; float:right; padding-right: 10px;"><span>').text("Created: " + toDateString(result.metadata.createdOn));
            metadata.append(createdOn);
        }
    }

    function getPreviewData(result) {
        return ObjectPreviewUtil.getPreviewData(result);
    }

    function prettifyPreviewJson(previewJson, maxLength) {
        var result = null;
        if (typeof previewJson === "string") {
            result = previewJson;
        } else {
            result = JSON.stringify(previewJson);
        }
        if (maxLength != null && maxLength != undefined) {
            if (result.length > maxLength) {
                result = result.substring(0, maxLength) + "...";
            }
        }
        return result;
    }

    function toDateString(timestamp) {
        var date = new Date(timestamp);
        return date.toISOString();
    }

    constructor();
}